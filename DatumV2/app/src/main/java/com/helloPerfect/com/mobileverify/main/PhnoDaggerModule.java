package com.helloPerfect.com.mobileverify.main;
import com.helloPerfect.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;
/**
 * <h1>PhnoDaggerModule</h1>
 * <p>
 *
 * </p>
 * @author 3Embed
 * @since 08/01/2018
 */
@Module
public abstract class PhnoDaggerModule
{
    @FragmentScoped
    @Binds
    abstract PhnoFragment phnoFragment(PhnoFragment phnoFragment);

    @FragmentScoped
    @Binds
    abstract PhnoContract.Presenter taskPresenter(PhnoPresenter presenter);
}