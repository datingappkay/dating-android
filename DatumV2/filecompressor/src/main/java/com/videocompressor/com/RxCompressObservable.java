package com.videocompressor.com;
import com.videocompressor.com.DataModel.CompressedData;
import io.reactivex.Observable;
import io.reactivex.Observer;
/**
 * @since  3/15/2018.
 * @author Suresh.
 * @version 1.0.
 */
public class RxCompressObservable extends Observable<CompressedData>
{
    private Observer<? super CompressedData> observer;
    private static RxCompressObservable rxJava2Observable = null;
    private RxCompressObservable() {}
    private CompressedData lastData;

    static RxCompressObservable getInstance()
    {
        if (rxJava2Observable == null) {
            rxJava2Observable = new RxCompressObservable();
        }
        return rxJava2Observable;
    }
    @Override
    protected void subscribeActual(Observer<? super CompressedData> observer)
    {
        this.observer = observer;
        if(lastData!=null)
        {
            this.observer.onNext(lastData);
            lastData=null;
        }
    }


   public void publishData(CompressedData data)
    {
        if (observer != null && data != null)
        {
            if (data.isError()) {
                observer.onError(new Throwable(data.getMessage()));
            } else {
                observer.onNext(data);
            }
            observer.onComplete();
        }else
        {
            this.lastData=data;
        }
    }
}

