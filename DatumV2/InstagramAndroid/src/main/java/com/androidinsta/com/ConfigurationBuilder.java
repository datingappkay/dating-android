package com.androidinsta.com;
/**
 * <h2>ConfigurationBuilder</h2>
 * <P>
 *
 * </P>
 * @since  12/12/2017.
 * @author Suresh.
 * @version 1.0.
 */
class ConfigurationBuilder implements Configuration
{
    private String  Client_ID;
    private String ClientSecret;
    @Override
    public void setOAuthConsumerKey(String client_ID)
    {
        this.Client_ID=client_ID;
    }
    @Override
    public void setOAuthConsumerSecret(String clientSecret)
    {
        this.ClientSecret=clientSecret;
    }
    @Override
    public String getOAuthConsumerKey() {

        return this.Client_ID;
    }
    @Override
    public String getOAuthConsumerSecret()
    {
        return this.ClientSecret;
    }

    Configuration build()
    {
        return this;
    }
}
