package com.helloPerfect.com.home.Discover.GridFrg;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.helloPerfect.com.R;
import com.helloPerfect.com.home.Discover.GridFrg.Model.DeckCardItemClicked;
import com.helloPerfect.com.home.Discover.GridFrg.Model.GridModel;
import com.helloPerfect.com.home.Discover.Model.UserItemPojo;
import com.helloPerfect.com.home.HomeModel.LoadMoreStatus;
import com.helloPerfect.com.home.HomeUtil;
import com.helloPerfect.com.util.AppConfig;

import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * <h2>GridDataViewPresenter</h2>
 * <P>
 *
 * </P>
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class GridDataViewPresenter implements GridDataViewContract.Presenter,DeckCardItemClicked
{
    private GridDataViewContract.View view;

    @Named(HomeUtil.LOAD_MORE_STATUS)
    @Inject
    LoadMoreStatus loadMoreStatus;
    @Inject
    Activity activity;
    @Inject
    GridModel model;

    private boolean superLikeBySwipe;
    private UserItemPojo userItemPojo;

    @Inject
    GridDataViewPresenter()
    {}

    @Override
    public void takeView(GridDataViewContract.View view) {
        this.view=view;
    }

    @Override
    public void dropView()
    {
        view=null;
    }

    @Override
    public void initListener()
    {
        if(view!=null)
            view.adapterClickListener(this);
    }


    @Override
    public void loadMore()
    {
        if(view!=null&&!loadMoreStatus.isNo_more_data())
            view.doLoadMore();
    }

    @Override
    public void noData()
    {
        if(view!=null)
            view.showLoadingView();
    }


    @Override
    public void updateDataChanged()
    {
        if(view!=null)
            view.notifyDataChanged();
    }

    @Override
    public void showBoostDialog()
    {
        if(view!=null)
            view.openBoostDialog();
    }

    @Override
    public void openUserProfile(String data,View data_view)
    {
        if(view!=null)
            view.openUerProfile(data,data_view);
    }

    @Override
    public void openChatScreen(UserItemPojo userItemPojo, View view) {
        if(this.view != null)
            this.view.openChat(userItemPojo);
    }



    @Override
    public boolean isNoMoreData() {
        return loadMoreStatus.isNo_more_data();
    }

    @Override
    public void revertAction(UserItemPojo item)
    {
        if(view!=null)
            view.onLikeEventError(item);
    }

    /*
     *On Handel result */
    @Override
    public boolean onHandelActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode== AppConfig.PROFILE_REQUEST&&resultCode== Activity.RESULT_OK)
        {
            Bundle result_data=data.getExtras();
            assert result_data != null;
            int result_action=result_data.getInt(AppConfig.RESULT_DATA);
            if(result_action==AppConfig.ON_SUPER_LIKE)
            {
                if(view != null)
                    view.doSuperLike();
            }else if(result_action==AppConfig.ON_LIKE)
            {
                if(view!=null)
                    view.doLike();
            }else if(result_action==AppConfig.ON_DISLIKE)
            {
                if(view!=null)
                    view.doDislike();
            }
            else if(result_action==AppConfig.ON_CHAT)
            {
                if(view!=null)
                    view.setChatListNeedToUpdate(true);
            }
            return true;
        }else
        {
            return false;
        }
    }

    @Override
    public void initiateSuperlike() {
        if(view!=null)
            view.doSuperLike();
    }

    @Override
    public void showBoostViewCounter(boolean show, int viewCount) {
        if(view != null) {
            String boostText = String.format(Locale.ENGLISH,"%d %s",viewCount,activity.getString(R.string.views));
            view.showBoostViewCounter(show, boostText);
        }
    }

    @Override
    public void startCoinAnimation() {
        if(view != null)
            view.startCoinAnimation();
    }

    private void launchWalletEmptyDialogForSuperlike(){
    }

    private void launchSpendCoinDialogForSuperlike(UserItemPojo userItemPojo) {
    }

    @Override
    public void checkWalletForSuperlike(UserItemPojo userItemPojo) {
        if(model.isEnoughWalletBalanceToSuperLike()) {
            if(model.isSuperlikeSpendDialogNeedToShow()){
                launchSpendCoinDialogForSuperlike(userItemPojo);
            }
            else {
                initiateSuperlike();
            }
        }
        else{
            //show wallet empty dialog for superlike.
            launchWalletEmptyDialogForSuperlike();
        }
    }

    @Override
    public void checkWalletForSuperlikeBySwipe(UserItemPojo userItemPojo) {
        superLikeBySwipe = true;
        userItemPojo = userItemPojo;
        if(model.isEnoughWalletBalanceToSuperLike()) {
            if(model.isSuperlikeSpendDialogNeedToShow()){
                launchSpendCoinDialogForSuperlike(userItemPojo);
                revertAction(userItemPojo);
            }
            else {
                superLikeBySwipe = false;
                if(view != null)
                    view.callSuperLikeApi(userItemPojo);
            }
        }
        else{
            superLikeBySwipe = false;
            //show wallet empty dialog for superlike.
            launchWalletEmptyDialogForSuperlike();
            revertAction(userItemPojo);
        }
    }

    @Override
    public void startVideoPlayFirstItem() {
        if(view != null)
            view.notifyFirstItem();
    }

}
