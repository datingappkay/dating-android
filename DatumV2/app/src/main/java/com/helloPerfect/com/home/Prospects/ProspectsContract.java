package com.helloPerfect.com.home.Prospects;

import com.helloPerfect.com.BasePresenter;
import com.helloPerfect.com.BaseView;
/**
 * <h2>ProspectsContract</h2>
 * <P>
 *
 * </P>
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface ProspectsContract
{
    interface View extends BaseView
    {

    }

    interface Presenter extends BasePresenter<View>
    {

    }
}
