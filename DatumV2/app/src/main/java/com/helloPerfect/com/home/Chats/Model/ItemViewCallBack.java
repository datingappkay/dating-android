package com.helloPerfect.com.home.Chats.Model;

import android.view.View;

interface ItemViewCallBack
{
    void onViewItemCallBack(View view, int position);
}
