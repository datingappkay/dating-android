package com.helloPerfect.com.home.Discover.GridFrg;

import android.app.Activity;

import com.helloPerfect.com.dagger.FragmentScoped;
import com.helloPerfect.com.data.model.DiscoverTabPositionHolder;
import com.helloPerfect.com.data.source.PreferenceTaskDataSource;
import com.helloPerfect.com.home.Discover.DiscoveryFragUtil;
import com.helloPerfect.com.home.Discover.GridFrg.Model.CardDeckViewAdapter;
import com.helloPerfect.com.home.Discover.Model.UserItemPojo;
import com.helloPerfect.com.home.HomeModel.HomeAnimation;
import com.helloPerfect.com.util.TypeFaceManager;
import com.helloPerfect.com.util.Utility;
import java.util.ArrayList;
import javax.inject.Named;
import dagger.Module;
import dagger.Provides;
/**
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public class GridDataViewFrgBuilder
{
    @Provides
    @FragmentScoped
    CardDeckViewAdapter getCardAdapter(Activity activity, TypeFaceManager typeFaceManager, Utility utility, @Named(DiscoveryFragUtil.USER_LIST) ArrayList<UserItemPojo> data, PreferenceTaskDataSource dataSource, DiscoverTabPositionHolder tabPostionHolder, HomeAnimation homeAnimation)
    {
        return new CardDeckViewAdapter(activity,typeFaceManager,utility,data,dataSource,tabPostionHolder,homeAnimation);
    }
}
