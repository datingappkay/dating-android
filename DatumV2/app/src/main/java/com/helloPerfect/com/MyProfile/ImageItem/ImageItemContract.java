package com.helloPerfect.com.MyProfile.ImageItem;

import com.helloPerfect.com.BasePresenter;
import com.helloPerfect.com.BaseView;

/**
 * @since  4/4/2018.
 */

public interface ImageItemContract
{
    interface View extends BaseView
    {

    }

    interface Presenter extends BasePresenter<View>
    {

    }
}
