package com.helloPerfect.com.userProfile.AleretBox;
import android.annotation.SuppressLint;
import android.app.Activity;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import com.helloPerfect.com.R;
import com.helloPerfect.com.util.TypeFaceManager;
import com.helloPerfect.com.util.Utility;
import java.util.Locale;
/**
 * <h2>ChatMenuDialog</h2>
 * <P>
 *
 * </P>
 * @since  4/10/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class BottomDialog
{
    private Activity activity;
    private BottomSheetDialog dialog;
    private TypeFaceManager typeFaceManager;
    private Utility utility;
    private AlertBoxCallBack callBack;

    public BottomDialog(Activity activity,TypeFaceManager typeFaceManager,Utility utility)
    {
        this.activity=activity;
        this.typeFaceManager=typeFaceManager;
        this.utility=utility;
        dialog = new BottomSheetDialog(activity,R.style.DatumBottomDialog);
    }


    public void showDialog(String user_name,Boolean isUserBlocked, AlertBoxCallBack callBack)
    {
        this.callBack = callBack;
        if(dialog.isShowing())
        {
            dialog.cancel();
        }
        user_name=utility.formatString( user_name);
        @SuppressLint("InflateParams")
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.user_option_aleret_bg, null);
        Button recomend_button=dialogView.findViewById(R.id.recomend_button);
        recomend_button.setTypeface(typeFaceManager.getCircularAirBook());
        recomend_button.setOnClickListener(view -> {
            //pending
            if(callBack != null)
                callBack.onRecommendFriend();
            if(dialog!=null)
                dialog.cancel();
        });

        Button report_button=dialogView.findViewById(R.id.report_button);
        report_button.setText(String.format(Locale.ENGLISH,"Report %s",user_name));
        report_button.setTypeface(typeFaceManager.getCircularAirBook());
        report_button.setOnClickListener(view -> {
            //pending
            if(callBack != null)
                callBack.onReport();
            if(dialog!=null)
                dialog.cancel();
        });
        Button block_button=dialogView.findViewById(R.id.block_button);
        if(isUserBlocked)
            block_button.setText(String.format(Locale.ENGLISH,"Unblock %s", user_name));
        else
            block_button.setText(String.format(Locale.ENGLISH,"Block %s", user_name));

        block_button.setTypeface(typeFaceManager.getCircularAirBook());
        block_button.setOnClickListener(view -> {
            //pending
            if(isUserBlocked) {
                if (callBack != null)
                    callBack.onUnblock();
            }
            else {
                if (callBack != null)
                    callBack.onBlock();
            }
            if(dialog!=null)
                dialog.cancel();
        });
        Button onCanceled=dialogView.findViewById(R.id.onCanceled);
        onCanceled.setTypeface(typeFaceManager.getCircularAirBook());
        onCanceled.setOnClickListener(view -> {
            if(dialog!=null)
                dialog.cancel();
        });
        dialog.setContentView(dialogView);
        dialog.show();
    }
}
