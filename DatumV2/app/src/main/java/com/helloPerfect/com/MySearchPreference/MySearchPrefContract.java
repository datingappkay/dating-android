package com.helloPerfect.com.MySearchPreference;

import com.helloPerfect.com.BasePresenter;
import com.helloPerfect.com.BaseView;
import java.util.List;

/**
 * @since  3/7/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface MySearchPrefContract
{
    interface View extends BaseView
    {
        void prefUpdated(boolean isRequired);
        void showLoadingProgress();
        void showError(String error);
        void showMessage(String message);
        void updatePrefView(List<android.view.View> view);
        void onConnectionError();
        void launchLocationScreen();

    }

    interface Presenter extends BasePresenter
    {
        void openBoostDialog();
        void updateStoredPref();
        void getMyPreference();
        void updatePreference();
        void observeLocationChange();
    }

}
