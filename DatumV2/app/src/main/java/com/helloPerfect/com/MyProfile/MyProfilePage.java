package com.helloPerfect.com.MyProfile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import com.helloPerfect.com.util.accountKit.AccountKitManager;
import com.helloPerfect.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.appbar.AppBarLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.androidinsta.com.ImageData;
import com.androidinsta.com.InstagramManger;
import com.helloPerfect.com.MyProfile.Model.InstaMediaAdapter;
import com.helloPerfect.com.MyProfile.Model.OpenInstagram;
import com.helloPerfect.com.MyProfile.Model.ProfileMediaAdapter;
import com.helloPerfect.com.MyProfile.editAge.EditDobActivity;
import com.helloPerfect.com.MyProfile.editEmail.EditEmailActivity;
import com.helloPerfect.com.MyProfile.editGender.EditGenderActivity;
import com.helloPerfect.com.MyProfile.editName.EditNameActivity;
import com.helloPerfect.com.MyProfile.editPreference.EditPrefActivity;
import com.helloPerfect.com.R;
import com.helloPerfect.com.data.model.PrefData;
import com.helloPerfect.com.data.source.PreferenceTaskDataSource;
import com.helloPerfect.com.editProfile.EditProfileActivity;
import com.helloPerfect.com.mobileverify.MobileVerifyActivity;
import com.helloPerfect.com.passportLocation.PassportActivity;
import com.helloPerfect.com.util.CustomVideoView.CustomVideoSurfaceView;
import com.helloPerfect.com.util.SegmentProgressBar.SegmentedProgressBar;
import com.helloPerfect.com.util.TypeFaceManager;
import com.suresh.innapp_purches.InnAppSdk;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * <h2>MyProfilePage</h2>
 * <P>
 *  User profile details for the logged in user.
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 17-03-2018.
 * */
public class MyProfilePage extends BaseDaggerActivity implements MyProfilePageContract.View,OpenInstagram
{
    private static final int EDIT_PREF_REQUEST_CODE = 700;
    private static final String EDIT_MOBILE_TAG = "edit_mobile_tag";
    private static final int EDIT_PHOTO_VIDEOS = 900;
    private static final int EDIT_MOBILE_REQ_CODE = 800;
    private static final int EDIT_GENDER_REQ_CODE = 600;
    private static final int EDIT_DOB_REQ_CODE = 700;
    private static final int EDIT_NAME_REQ_CODE = 500;
    private static final int EDIT_EMAIL_REQ_CODE = 400;

    private Unbinder unbinder;
    @Inject
    AccountKitManager accountKitManager;
    @Inject
    Activity activity;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    ProfileMediaAdapter adapter;
    @Inject
    MyProfilePageContract.Presenter presenter;
    @BindView(R.id.toolbar_title_tv)
    TextView tvToolbarTitle;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.itemsCountTv)
    TextView itemsCountTv;
    @BindView(R.id.segmented_progressbar)
    SegmentedProgressBar segmented_progressbar;
    @BindView(R.id.updateProfileLl)
    LinearLayout updateProfileLl;
    @BindView(R.id.other_text)
    TextView other_text;
    @BindView(R.id.not_show_age)
    TextView not_show_age;
    @BindView(R.id.switch_age)
    Switch switch_age;
    @BindView(R.id.not_show_distance)
    TextView not_show_distance;

    @BindView(R.id.switch_distance)
    Switch switch_distance;

    @BindView(R.id.show_insta_pic)
    TextView show_insta_pic;

    @BindView(R.id.connect_instagram)
    TextView connect_instagram;

    @BindView(R.id.datum_plus)
    TextView datum_plus;

    @BindView(R.id.control_text)
    TextView control_text;

    @BindView(R.id.parent_container)
    CoordinatorLayout parent_view;

    @BindView(R.id.bottom_details)
    LinearLayout bottom_details;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tick_mark)
    RelativeLayout rlTickMark;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @Inject
    InstagramManger instagramManger;
    @Inject
    PreferenceTaskDataSource dataSource;

    @BindView(R.id.media_list)
    RecyclerView media_list;
    @BindView(R.id.insta_disconnect_btn)
    TextView btnInstaDisconnect;
    public CustomVideoSurfaceView video_player;
    private Menu menu;
    Drawable drawable,toolbarBg,transparentBg;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile_page);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        drawable = getResources().getDrawable(R.drawable.ic_gallery_icon);
        transparentBg = new ColorDrawable(getResources().getColor(R.color.transparent));
        toolbarBg = getResources().getDrawable(R.drawable.segment_progressbar_profile_bg);
        unbinder=ButterKnife.bind(this);
        video_player=CustomVideoSurfaceView.getNewInstance(this,dataSource);
        initUI();
        presenter.initListener();
        presenter.updateUserStoredDetails();
        presenter.getUserData();
        presenter.observeLocationChange();
        updateInstaDetails();
    }

    /*
     * init user data*/
    private void initUI()
    {
        setSupportActionBar(toolbar);
        ActionBar actionBar=getSupportActionBar();
        if(actionBar!=null)
        {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
        {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }
        tvToolbarTitle.setTypeface(typeFaceManager.getCircularAirBold());
        itemsCountTv.setTypeface(typeFaceManager.getCircularAirBook());
        other_text.setTypeface(typeFaceManager.getCircularAirBold());
        not_show_age.setTypeface(typeFaceManager.getCircularAirLight());
        not_show_distance.setTypeface(typeFaceManager.getCircularAirLight());
        show_insta_pic.setTypeface(typeFaceManager.getCircularAirBold());
        connect_instagram.setTypeface(typeFaceManager.getCircularAirBook());
        datum_plus.setTypeface(typeFaceManager.getCircularAirLight());
        btnInstaDisconnect.setTypeface(typeFaceManager.getCircularAirLight());
        control_text.setTypeface(typeFaceManager.getCircularAirLight());
        segmented_progressbar.setContainerColor(ContextCompat.getColor(activity,R.color.black30));
        segmented_progressbar.setFillColor(ContextCompat.getColor(activity,R.color.white));
        segmented_progressbar.playSegment(50);
        viewPager.setAdapter(adapter);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow;
            int scrollRange = -1;
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if(scrollRange == -1){
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if(scrollRange + verticalOffset == 0){
                    isShow = true;
                    showToolbarTitle(true);
                } else if(isShow){
                    isShow = false;
                    showToolbarTitle(false);
                }
            }
        });
    }

    @OnClick(R.id.parent_container)
    public void parentLayout(){}

    @OnClick(R.id.switch_age)
    public void switchAgeClick()
    {
        presenter.handleAgeSwitchClick();
    }

    @OnCheckedChanged(R.id.switch_age)
    public void switchAge(boolean isChecked)
    {
        presenter.saveAgePref(isChecked);
    }

    @OnClick(R.id.switch_distance)
    public void switchDistanceClick()
    {
        presenter.handleDistanceSwitchClick();
    }


    @OnCheckedChanged(R.id.switch_distance)
    public void switchDistance(boolean isChecked)
    {
        presenter.saveDistancePref(isChecked);
    }

    @OnClick(R.id.datum_plus)
    public void launchBoostDetail()
    {
        presenter.launchBoostDialog();
    }

    private void showToolbarTitle(boolean show) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                if(show){
                    if(itemsCountTv != null && tvToolbarTitle != null && toolbar != null) {
                        itemsCountTv.setVisibility(View.GONE);
                        tvToolbarTitle.setVisibility(View.VISIBLE);
                        toolbar.setNavigationIcon(R.drawable.ic_back_svg_icon);
                        toolbar.setBackground(transparentBg);
                    }
                    if(menu != null){
                        MenuItem menuItem = menu.getItem(0);
                        drawable.setColorFilter(getResources().getColor(R.color.datum), PorterDuff.Mode.SRC_IN);
                        menuItem.setIcon(drawable);

                    }
                }
                else {
                    if(itemsCountTv != null && tvToolbarTitle != null && toolbar != null) {
                        itemsCountTv.setVisibility(View.VISIBLE);
                        tvToolbarTitle.setVisibility(View.GONE);
                        toolbar.setNavigationIcon(R.drawable.ic_white_svg_icon);
                        toolbar.setBackground(toolbarBg);
                    }
                    if(menu != null){
                        MenuItem menuItem = menu.getItem(0);
                        drawable.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
                        menuItem.setIcon(drawable);
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.edit_profile_page_menu,menu);
        return true;
    }


    @Override
    protected void onPause() {
        super.onPause();
        video_player.onActivityStop();
    }

    @Override
    protected void onDestroy()
    {
        if(video_player != null)
            video_player.stopPlayer();
        unbinder.unbind();
        presenter.dispose();
        super.onDestroy();
    }

    @Override
    public void onBackPressed()
    {
        if(presenter.isLocationCahnged())
        {
            Intent resultIntent = new Intent();
          setResult(RESULT_OK,resultIntent);
        }
        presenter.notifySettingsDataChange();
        this.finish();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        accountKitManager.onActivityResult(requestCode, resultCode, data);
        if(!instagramManger.onActivityResult(requestCode,resultCode,data))
        {
            super.onActivityResult(requestCode, resultCode, data);
        }

        if(!InnAppSdk.getVendor().onActivity_result(requestCode,resultCode,data))
        {
            if(requestCode == EDIT_PHOTO_VIDEOS){
                presenter.parseEditedPhotoVideos(requestCode,resultCode,data);
            }
            if(requestCode == EDIT_PREF_REQUEST_CODE){
                presenter.parseEditedPreferenceData(requestCode,resultCode,data);
            }
            if(requestCode == EDIT_MOBILE_REQ_CODE){
                presenter.parseEditedPhone(requestCode,resultCode,data);
            }
            if(requestCode == EDIT_DOB_REQ_CODE){
                presenter.parseEditedDob(requestCode,resultCode,data);
            }
            if(requestCode == EDIT_EMAIL_REQ_CODE){
                presenter.parseEditedEmail(requestCode,resultCode,data);
            }
            if(requestCode == EDIT_NAME_REQ_CODE){
                presenter.parseEditedName(requestCode,resultCode,data);
            }
            if(requestCode == EDIT_GENDER_REQ_CODE){
                presenter.parseEditedGender(requestCode,resultCode,data);
            }
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        switch (menuItem.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_edit_profile:
                Intent intent = new Intent(MyProfilePage.this, EditProfileActivity.class);
                startActivityForResult(intent,EDIT_PHOTO_VIDEOS);
                activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
                return true;
            default:

        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick(R.id.connect_instagram)
    void setConnect_instagram()
    {
        presenter.doInstagramLogin();
    }

    @Override
    public void intPageChangeListener(ViewPager.OnPageChangeListener listener)
    {
        viewPager.addOnPageChangeListener(listener);
    }

    @Override
    public void addChildView(ArrayList<View> views)
    {
        bottom_details.setVisibility(View.VISIBLE);
        if(updateProfileLl.getChildCount()>0)
        {
            updateProfileLl.removeAllViews();
        }
        for(View view:views)
        {
            updateProfileLl.addView(view);
        }
    }

    @Override
    public void updateSegmentBar(int total, int selected)
    {
        video_player.stopPlayerFrg();
        segmented_progressbar.setSegmentCount(total);
        segmented_progressbar.setCompletedSegments(selected+1);
        itemsCountTv.setText(String.format(Locale.ENGLISH, "%d/%d", selected+1, total));
    }

    @Override
    public void showError(String message)
    {
        if(parent_view != null) {
            Snackbar snackbar = Snackbar
                    .make(parent_view, "" + message, Snackbar.LENGTH_LONG);
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorAccent));
            TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(ContextCompat.getColor(activity, R.color.white));
            snackbar.show();
        }
    }


    @Override
    public void showMessage(String message)
    {
        Snackbar snackbar = Snackbar
                .make(parent_view,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void addLoadingView(View view)
    {
        bottom_details.setVisibility(View.GONE);
        if(updateProfileLl.getChildCount()>0)
        {
            updateProfileLl.removeAllViews();
        }
        updateProfileLl.addView(view);
    }

    @Override
    public void launchEditPreference(PrefData prefData) {
        Intent intent = new Intent(MyProfilePage.this, EditPrefActivity.class);
        intent.putExtra("pref_data",prefData);
        startActivityForResult(intent,EDIT_PREF_REQUEST_CODE);
        applyTransition();
    }

    @Override
    public void launchMobileVerifyActivity(Bundle phoneBundle) {
        Intent intent = new Intent(MyProfilePage.this, MobileVerifyActivity.class);
        intent.putExtra("edit_mobile",EDIT_MOBILE_TAG);
        intent.putExtra(MyProfilePagePresenter.PHONE_DATA_BUNDLE, phoneBundle);
        startActivityForResult(intent,EDIT_MOBILE_REQ_CODE);
        applyTransition();
    }

    @Override
    public void launchGenderScreen(String date) {
        Intent intent = new Intent(MyProfilePage.this, EditGenderActivity.class);
        intent.putExtra(MyProfilePagePresenter.GENDER_DATA,date);
        startActivityForResult(intent,EDIT_GENDER_REQ_CODE);
        applyTransition();
    }

    @Override
    public void launchDobScreen(String dob) {
        Intent intent = new Intent(MyProfilePage.this, EditDobActivity.class);
        intent.putExtra(MyProfilePagePresenter.DOB_DATA,dob);
        startActivityForResult(intent,EDIT_DOB_REQ_CODE);
        applyTransition();
    }

    private void applyTransition() {
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }

    @Override
    public void launchNameScreen(String name) {
        Intent intent = new Intent(MyProfilePage.this, EditNameActivity.class);
        intent.putExtra(MyProfilePagePresenter.NAME_DATA,name);
        startActivityForResult(intent,EDIT_NAME_REQ_CODE);
        applyTransition();
    }

    @Override
    public void launchEmailScreen(String email) {
        Intent intent = new Intent(MyProfilePage.this, EditEmailActivity.class);
        intent.putExtra(MyProfilePagePresenter.EMAIL_DATA,email);
        startActivityForResult(intent,EDIT_EMAIL_REQ_CODE);
        applyTransition();
    }

    @Override
    public void showTickMark(boolean show) {
        if(show){
            rlTickMark.setVisibility(View.VISIBLE);
        }
        else{
            rlTickMark.setVisibility(View.GONE);
        }
    }

    @Override
    public void showToolbarTitle(String name) {
        tvToolbarTitle.setText(""+name);
    }

    @Override
    public void finishActivity()
    {
        if(presenter.isLocationCahnged())
        {
            Intent resultIntent = new Intent();
            setResult(RESULT_OK,resultIntent);
        }
        presenter.notifySettingsDataChange();
        this.finish();
    }

    @Override
    public void invalidateAgeSwitch(boolean isAgeHidden) {
        switch_age.setChecked(isAgeHidden);
    }

    @Override
    public void invalidateDistSwitch(boolean isDistHidden) {
        switch_distance.setChecked(isDistHidden);
    }


    @Override
    public void launchLocationScreen()
    {
        Intent intent = new Intent(activity,PassportActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }

    @OnClick(R.id.tick_mark)
    public void tickMarkClick()
    {
        presenter.callEditProfileApi();
    }

    @OnClick(R.id.insta_disconnect_btn)
    public void disconnectInsta(){
        showDisconnectInstaDialog();
    }

    @Override
    public void showDisconnectInstaDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.UpdateAlertDialog)
                .setTitle(R.string.instagram_discoonect_title)
                .setMessage(R.string.instagram_disconnect_msg)
                .setPositiveButton(R.string.Ok_text, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        presenter.disconnectInstagram();
                    }
                })
                .setNegativeButton(R.string.cancel_text, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    @Override
    public void hideInstaMediaUi() {
        if(connect_instagram != null) {
            media_list.setVisibility(View.GONE);
            btnInstaDisconnect.setVisibility(View.GONE);
            connect_instagram.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void updateInstaData(List<List<ImageData>> lists)
    {
        media_list.setAdapter(new InstaMediaAdapter(this,this,lists));
        media_list.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        connect_instagram.setVisibility(View.GONE);
        media_list.setVisibility(View.VISIBLE);
        btnInstaDisconnect.setVisibility(View.VISIBLE);
    }


    private void updateInstaDetails()
    {
        if(instagramManger.isUserLoggedIn())
        {
            presenter.getUserDetails();
        }
    }


    @Override
    public void openInstagram()
    {
        instagramManger.openInInstagram(this,instagramManger.getLoggedInUserID(),instagramManger.getLoggedInUserName());
    }
}
