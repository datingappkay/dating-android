package com.androidinsta.com;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
/**
 * <h2>OAuthActivity</h2>
 * <P>
 *     Auth activity for the use to be redirect and login in tweeter in android.
 *     provide Tweeter login and access token manager.
 * </P>
 * @since  12/12/2017.
 * @version 1.0.
 * @author Suresh.
 */
public class OAuthActivity extends AppCompatActivity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        String authenticationUrl = getIntent().getStringExtra(InstagramConfig.STRING_EXTRA_AUTHENCATION_URL);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        OAuthWebViewFragment oAuthWebViewFragment =OAuthWebViewFragment.getInstance(authenticationUrl);
        fragmentTransaction.add(android.R.id.content,oAuthWebViewFragment);
        fragmentTransaction.commit();
    }
}
