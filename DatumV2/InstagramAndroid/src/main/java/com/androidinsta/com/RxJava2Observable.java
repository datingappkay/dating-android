package com.androidinsta.com;
import io.reactivex.Observable;
import io.reactivex.Observer;
/**
 * @author Suresh.
 * @version 1.0.
 * @since  12/13/2017.
 */
public class RxJava2Observable extends Observable<InstaReponseHolder>
{
    private RxJava2Observable(){};
    private static RxJava2Observable rxJava2Observable=null;
   static RxJava2Observable getInstance()
    {
        if(rxJava2Observable==null)
        {
            rxJava2Observable=new RxJava2Observable();
        }
        return rxJava2Observable;
    }
    private Observer<?super InstaReponseHolder> observer;
    @Override
    protected void subscribeActual(Observer<? super InstaReponseHolder> observer)
    {
        this.observer=observer;
    }

    void publishData(InstaReponseHolder data)
    {
        if(observer!=null&&data!=null)
        {
            if(data.getError())
            {
                observer.onError(new Throwable(data.getErrorMessage()));
            }else
            {
                observer.onNext(data);
            }
            observer.onComplete();
        }
    }
}
