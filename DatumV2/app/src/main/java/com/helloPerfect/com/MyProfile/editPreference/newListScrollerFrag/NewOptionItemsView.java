package com.helloPerfect.com.MyProfile.editPreference.newListScrollerFrag;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.helloPerfect.com.R;
import com.helloPerfect.com.util.TypeFaceManager;

/**
 * @since  2/22/2018.
 */
class NewOptionItemsView extends RecyclerView.ViewHolder
{
    TextView option_text;
    ImageView option_tick;
    View view;

    NewOptionItemsView(View itemView, TypeFaceManager typeFaceManager)
    {
        super(itemView);
        this.view =itemView;
        option_text=itemView.findViewById(R.id.option_text);
        option_tick=itemView.findViewById(R.id.option_tick);
        option_text.setTypeface(typeFaceManager.getCircularAirBook());

    }
}
