package com.helloPerfect.com.UserPreference.CanditionChoice;
import com.helloPerfect.com.BasePresenter;
import com.helloPerfect.com.BaseView;

import org.json.JSONArray;

/**
 * @since  2/21/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface ConChoiceContract
{
    interface Presenter extends BasePresenter<View>
    {
        void updatePreference(String pref_id, JSONArray values);
    }
    interface View extends BaseView
    {

        void showError(String error);

        void showMessage(String message);
    }
}
