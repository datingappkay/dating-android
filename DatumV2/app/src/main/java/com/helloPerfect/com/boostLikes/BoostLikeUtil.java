package com.helloPerfect.com.boostLikes;

import android.app.Activity;
import android.content.Context;
import androidx.recyclerview.widget.GridLayoutManager;

import com.helloPerfect.com.boostLikes.Model.BoostLikeAdapter;
import com.helloPerfect.com.boostLikes.Model.BoostLikeData;
import com.helloPerfect.com.dagger.ActivityScoped;

import com.helloPerfect.com.util.TypeFaceManager;
import com.helloPerfect.com.util.Utility;
import com.helloPerfect.com.util.progressbar.LoadingProgress;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ankit on 7/9/18.
 */

@Module
public class BoostLikeUtil {

    @ActivityScoped
    @Provides
    ArrayList<BoostLikeData> provideLikeList(){
        return new ArrayList<>();
    }

    @ActivityScoped
    @Provides
    GridLayoutManager provideGridLayoutManager(Activity activity){
        return new GridLayoutManager(activity,2);
    }

    @ActivityScoped
    @Provides
    BoostLikeAdapter provideBoostLikeAdapter(Context context, ArrayList<BoostLikeData> arrayList,TypeFaceManager typeFaceManager, Utility utility){
        return new BoostLikeAdapter(context,arrayList,typeFaceManager,utility);
    }

    @ActivityScoped
    @Provides
    LoadingProgress provideLoadingProgress(Activity activity){
        return new LoadingProgress(activity);
    }



}
