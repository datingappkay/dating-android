package com.androidinsta.com;
/**
 * <h2>AccessToken</h2>
 * <P>
 *  Contains the access token of currently active user
 *  and its token details.
 * </P>
 * @since  12/12/2017.
 * @author Suresh.
 * @version 1.0.
 */
class AccessToken
{
    private static final String API_URL = "https://api.instagram.com/v1";
    private String ACCESS_TOKEN;
    private String USER_NAME;
    private String FULL_NAME;
    private String USER_ID;
    private String USER_PIC;
    private String USER_BIO;

    public String getWEB_SITE() {
        return WEB_SITE;
    }

    public void setWEB_SITE(String WEB_SITE) {
        this.WEB_SITE = WEB_SITE;
    }

    private String WEB_SITE;

    public String getUSER_PIC() {
        return USER_PIC;
    }

    public void setUSER_PIC(String USER_PIC) {
        this.USER_PIC = USER_PIC;
    }

    public String getUSER_BIO() {
        return USER_BIO;
    }

    public void setUSER_BIO(String USER_BIO) {
        this.USER_BIO = USER_BIO;
    }

    String getFULL_NAME() {
        return FULL_NAME;
    }
    void setFULL_NAME(String FULL_NAME) {
        this.FULL_NAME = FULL_NAME;
    }
    String getUSER_ID() {
        return USER_ID;
    }
    void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }
    String getUSER_NAME()
    {
        return USER_NAME;
    }
    void setUSER_NAME(String USER_ID)
    {
        this.USER_NAME = USER_ID;
    }
    String getACCESS_TOKEN()
    {
        return ACCESS_TOKEN;
    }
    void setACCESS_TOKEN(String ACCESS_TOKEN)
    {
        this.ACCESS_TOKEN = ACCESS_TOKEN;
    }

    String getUserRecentPostUrl()
    {
        return "https://api.instagram.com/v1/users/self/media/recent/?access_token="+ACCESS_TOKEN;
    }

    String getOtherRecentPostUrl(String user_id)
    {
        return "https://api.instagram.com/v1/users/"+
                user_id+
                "/media/recent/?access_token="+ACCESS_TOKEN;
    }

    public static String getOtherRecentPostUrl(String user_id,String userInstaToken)
    {
        return "https://api.instagram.com/v1/users/"+
                user_id+
                "/media/recent/?access_token="+userInstaToken;
    }
}