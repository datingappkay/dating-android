package com.facebookmanager.com;

import java.io.Serializable;

/**
 * <h2>FacebookUserDetails</h2>
 * <P>
 *     Getting the user details to collect the user details.
 * </P>
 * @since  12/14/2017.
 * @author Suresh.
 * @version 1.0.
 */
public class FacebookUserDetails implements Serializable
{
    /*
   * For error handling for the app.*/
    private String message;
    private boolean isError;
    /*
    * Collecting the user details*/
    private String id;
    private String name;
    private String firstName;
    private String lastName;
    private String userPic;
    private String birthDate;
    private String bio;
    private String email;
    private String otherPic;
    private String education;
    private String work;
    private String gender;
    //Extra
    private Double lat;
    private Double lon;
    private String profileVideo;
    private String profileVideoThumb;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }
    String getMessage()
    {
        return message;
    }
    void setMessage(String message)
    {
        this.message = message;
    }
    boolean isError() {
        return isError;
    }
    void setError(boolean error) {
        isError = error;
    }
    public String getId() {
        return id;
    }
    void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    void setName(String name) {
        this.name = name;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getUserPic() {
        return userPic;
    }
    public void setUserPic(String userPic) {
        this.userPic = userPic;
    }
    public String getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }
    public String getBio() {
        return bio;
    }
    void setBio(String bio) {
        this.bio = bio;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getOtherPic() {
        return otherPic;
    }
    void setOtherPic(String otherPic) {
        this.otherPic = otherPic;
    }
    public String getEducation() {
        return education;
    }
    public void setEducation(String education) {
        this.education = education;
    }


    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }


    public String getProfileVideo() {
        return profileVideo;
    }

    public void setProfileVideo(String profileVideo) {
        this.profileVideo = profileVideo;
    }

    public String getProfileVideoThumb() {
        return profileVideoThumb;
    }

    public void setProfileVideoThumb(String profileVideoThumb) {
        this.profileVideoThumb = profileVideoThumb;
    }

    @Override
    public String toString() {
        return "FacebookUserDetails{" +
                "message='" + message + '\'' +
                ", isError=" + isError +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userPic='" + userPic + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", bio='" + bio + '\'' +
                ", email='" + email + '\'' +
                ", otherPic='" + otherPic + '\'' +
                ", education='" + education + '\'' +
                ", gender='" + gender + '\'' +
                ", work='" + work + '\'' +
                '}';
    }
}
