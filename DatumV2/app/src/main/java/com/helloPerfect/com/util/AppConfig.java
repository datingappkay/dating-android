package com.helloPerfect.com.util;

import com.helloPerfect.com.BuildConfig;

/**
 * <h2>AppConfig</h2>
 * <P>
 * </P>
 *@since  12/15/2017.
 * @author 3Embed.
 * @version 1.0.
 */
public interface AppConfig
{

    int notification_id = 201;

    long PROFILE_VIDEO_SIZE = 30 * 1024 * 1024;
    String AUTH_KEY="123";
    int CAMERA_CODE=123;
    int GALLERY_CODE=124;
    int LOAD_SCREEN = 2;
    int SPLASH_SCREEN_DURATION = 4;
    int SEARCH_REQUEST=345;
    int PROFILE_REQUEST=142;
    int ON_LIKE=143;
    int ON_DISLIKE =431;
    int ON_SUPER_LIKE=451;
    int ON_CHAT = 541;
    String RESULT_DATA="result";
    String USER_ID="user_id";
    String DEFAULT_MESSAGE = "3embed test";

    String ACTION_START_FOURGROUND = BuildConfig.APPLICATION_ID+".foreground";
    String NOTIFICATION_CHANNEL_ID_FOURGROUND_SERVICE = BuildConfig.APPLICATION_ID+".service_channel_id";
    String NOTIFICATION_CHANNEL_ID_CHAT = BuildConfig.APPLICATION_ID+".chat_channel_id";
    String NOTIFICATION_CHANNEL_ID_DATUM_NOTIFICATION = BuildConfig.APPLICATION_ID+".channel_id";
    String NOTIFICATION_CHANNEL_NAME_CHAT_SERVICE = BuildConfig.APPLICATION_ID+"_CHAT";
    String NOTIFICATION_CHANNEL_NAME_CHAT_NOTIFICATION = BuildConfig.APPLICATION_ID+"_CHAT_NOTIFICATION";
    String NOTIFICATION_CHANNEL_NAME_DATUM_NOTIFICATION = BuildConfig.APPLICATION_ID+"_NOTIFICATION";
    String DEFAULT_PAYMENT_GETWAY = "playStore";
    String DEFAULT_PROFILE_PIC ="https://findamatch.online/datum_2.0-admin/campagin.png";

    int DATE_PICKER_MINUTE_STEP_SIZE =1;
    int CHAT_SCREEN_REQ_CODE = 101;

    String[] APP_LANGUAGES = {};
    String DEFAULT_LANGUAGE="en";

    //search preference constants
    String KM = "km";
    String MI = "mi";
    long UPDATE_CHECK_INTERVAL = 10 * 60 * 1000; //10min

    //for foreground service;
    interface NOTIFICATION_ID {
        int FOREGROUND_SERVICE = 101;
    }

    interface MqttCredendials{
        String USERNAME = "3embed";
        String PASSWORD = "3embed";
    }
    /*google api details*/
    String GOOGLE_LOCATION_KEY="AIzaSyD4VENtsiXGApErZoq-Zcb70ru5UBm4F1U";

    /*Image processor details*/
    String IMAGE_PROCESSOR_KEY="1049832807";
    String IMAGE_PROCESSOR_SECRET_KEY="ybXQww6BEiHAJy2iCo6R";

    /*insatgram details*/
    String INSTA_CLINT_KEY="a50e0940e88e452cae8c544c3153016b";
    String INSTA_SECRET_KEY="5546fa21dca8405b9bc87ba8f567df91";
    String INSTAGRAM_URL="https://www.instagram.com/";

    /*
    * User name Regex String*/
    String USERNAME_REGEX="[A-Z][a-zA-Z][^#&<>\\\"~;$^%{}?]{1,15}$";

    //foursquare keys
    interface Foursquare{
        String CLIENT_ID ="3ZEDYUHV20HLZEXR50ZOSX1FZ3WANPOG4G5VPKXRMGASOMTL";
        String CLIENT_SECRET ="4SKX00BAX55C4JPVJL0NTGK1ZODCPSMTUZIZU5HVEBT45D2T";
    }

    interface DateActivity{
        int LOACTION_REQUEST_CODE = 111;
    }

    interface CloudinaryDetails
    {
        String NAME="diy6vdafb";
        String KEY="699328366683754";
        String SECRET_KEY="mYc4tzzcPPVahHSj_7o83PYEY-w";

        /*
         * list video quality*/
         String VIDEO_QUALITY ="/q_60";
        /*
         * video format*/
        String VIDEO_FORMATE ="mp4";
        /*
         * Image thumb nail size*/
         String  IMAGE_THUMBNAIL_SIZE="/q_60,w_150,h_150,c_thumb";
        /*
         * Video thumb nail size*/
         String  VIDEO_THUMBNAIL_SIZE="/q_60,w_200,h_200,c_thumb";
    }


    interface InappKey
    {
        String Base64Key="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwR6s7BIOgrJrEODkuqU27lA4wbuxCDTj7Lqu9xIPx4SutYpQSK11eqU4GZvBqcwhw0Jy6imybK+898dHHtcoJGq1U3qKm7Q1GkWSYr/MDbgS/w0p4rHm7sO4G7ZfuD6Lr1yIrz41r0qGQ4Qbn8yzAaVFZuufgR+e3Z/rTHKhr/XMfxdk13fXQLsKFBLlhW/Tu56UDvGAczdLK2+60RRkKWSKkRxaG4lLkk9sX1jrKZNAPV9gVQuqhiq5ntZZFCucF3Hl6lVTAMWAC4d3VEWGuHDLY+irQIx4hDuXKqvwCl583vc+Ww0tY/kKfRCNSsdEw7tT0wyE6Es13PjDjgASXwIDAQAB";
    }
}
