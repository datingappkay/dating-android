package com.helloPerfect.com.MyProfile;
import android.app.Activity;
import androidx.fragment.app.FragmentManager;
import com.helloPerfect.com.MyProfile.Model.ProfileMediaAdapter;
import com.helloPerfect.com.MyProfile.Model.ProfileMediaPojo;
import com.helloPerfect.com.dagger.ActivityScoped;
import com.helloPerfect.com.data.source.PreferenceTaskDataSource;
import com.helloPerfect.com.util.ProgressAleret.DatumProgressDialog;
import com.helloPerfect.com.util.TypeFaceManager;
import com.helloPerfect.com.util.Utility;
import com.helloPerfect.com.util.boostDialog.BoostDialog;
import com.helloPerfect.com.util.progressbar.LoadingProgress;

import java.util.ArrayList;
import javax.inject.Named;
import dagger.Module;
import dagger.Provides;
/**
 * <h2>ProfileUtil</h2>
 * <P>
 *
 * </P>
 * @since  4/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public class ProfileUtil
{
    @Provides
    @ActivityScoped
    ArrayList<ProfileMediaPojo> getMediaList()
    {
        return new ArrayList<>();
    }


    @Provides
    @ActivityScoped
    ProfileMediaAdapter getMediaAdapter(@Named(MyProfileBuilder.PROFILE_FRAGMENT_MANAGER)FragmentManager fm,ArrayList<ProfileMediaPojo> list)
    {
        return new ProfileMediaAdapter(fm,list);
    }

    @Provides
    @ActivityScoped
    DatumProgressDialog getDatumProgressDialog(Activity activity, TypeFaceManager typeFaceManager)
    {
        return new DatumProgressDialog(activity,typeFaceManager);
    }

    @ActivityScoped
    @Provides
    LoadingProgress getLoadingProgress(Activity activity)
    {
        return new LoadingProgress(activity);
    }

    @Provides
    @ActivityScoped
    BoostDialog getBootsDialog(Activity activity, TypeFaceManager typeFaceManager, PreferenceTaskDataSource dataSource, Utility utility)
    {
        return new BoostDialog(activity,typeFaceManager,dataSource,utility);
    }
}
