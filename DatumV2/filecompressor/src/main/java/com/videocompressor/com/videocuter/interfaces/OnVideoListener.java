package com.videocompressor.com.videocuter.interfaces;
/**
 * @since 1-01-2018
 * @author Suresh.
 * @version 1.0.*/
public interface OnVideoListener
{
    void onVideoPrepared();
}
