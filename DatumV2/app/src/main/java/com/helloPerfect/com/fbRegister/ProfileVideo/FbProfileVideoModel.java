package com.helloPerfect.com.fbRegister.ProfileVideo;

import android.graphics.Bitmap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.inject.Inject;

/**
 * <h2>FbProfileVideoModel</h2>
 * <P>
 *
 * </P>
 * @since  2/20/2018.
 * @author 3Embed.
 * @version 1.0.
 */
class FbProfileVideoModel
{
    @Inject
    FbProfileVideoModel()
    {}

    /*
     * Saving the file d*/
    private File saveBitmap(Bitmap bmp,String destination)
    {
        OutputStream outStream = null;
        File file = new File(destination);
        try {
            outStream = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG,100, outStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally
        {
            if(outStream!=null)
            {
                try
                {
                    outStream.flush();
                    outStream.close();
                }catch (Exception e) {}
            }
        }
        return file;
    }

    /*
     *Deleting the file */
    public void deleteFile(File file)
    {
        try
        {
            if(file.exists())
            {
                file.delete();
            }
        }catch (Exception e){}
    }
}
