package com.facebookmanager.com;

import java.util.ArrayList;

/**
 * <h2>FacebookQueryManager</h2>
 * <p>
 * Facebook query manager for the facebook query.
 * </P>
 *
 * @author Suresh.
 * @version 1.0.
 * @since 12/14/2017.
 */
public interface FacebookQueryManager {
    void getUserDetails(ArrayList<String> permission);
}
