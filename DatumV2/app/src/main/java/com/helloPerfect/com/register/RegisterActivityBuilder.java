package com.helloPerfect.com.register;

import android.app.Activity;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import com.helloPerfect.com.dagger.ActivityScoped;
import com.helloPerfect.com.dagger.FragmentScoped;
import com.helloPerfect.com.register.Gender.GenderFragment;
import com.helloPerfect.com.register.Gender.GenderFrgBuilder;
import com.helloPerfect.com.register.Name.NameFragBuilder;
import com.helloPerfect.com.register.Name.NameFragment;
import com.helloPerfect.com.register.ProfilePic.ProfilePicBuilder;
import com.helloPerfect.com.register.ProfilePic.ProfilePicFrg;
import com.helloPerfect.com.register.ProfileVideo.ProfileVideoBuilder;
import com.helloPerfect.com.register.ProfileVideo.ProfileVideoFrg;
import com.helloPerfect.com.register.Email.EmailFragment;
import com.helloPerfect.com.register.Email.EmailFrgBuilder;
import com.helloPerfect.com.register.Userdob.DobFragment;
import com.helloPerfect.com.register.Userdob.DobFrgBuilder;
import javax.inject.Named;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
/**
 * <h1>RegisterActivityBuilder</h1>
 * <p>
 *
 * </p>
 * @author 3Embed
 * @since 13/08/17
 * @version 1.0.
 */
@Module
public abstract class RegisterActivityBuilder
{
 static final String ACTIVITY_FRAGMENT_MANAGER = "Register.FragmentManager";

 @ActivityScoped
 @Binds
 abstract Activity provideActivity(RegisterPage register_page);

 @ActivityScoped
 @Binds
 abstract RegisterContact.View provideView(RegisterPage register_page);

 @ActivityScoped
 @Binds
 abstract RegisterContact.Presenter taskPresenter(RegisterPagePresenter presenter);

 @Provides
 @Named(ACTIVITY_FRAGMENT_MANAGER)
 @ActivityScoped
 static FragmentManager activityFragmentManager(Activity activity)
 {
  return ((AppCompatActivity)activity).getSupportFragmentManager();
 }

 @FragmentScoped
 @ContributesAndroidInjector(modules = {EmailFrgBuilder.class})
 abstract EmailFragment getEmailFragment();

 @FragmentScoped
 @ContributesAndroidInjector(modules ={NameFragBuilder.class})
 abstract NameFragment getNameFragment();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {DobFrgBuilder.class})
 abstract DobFragment getDobFragment();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {GenderFrgBuilder.class})
 abstract GenderFragment getGenderFragment();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {ProfilePicBuilder.class})
 abstract ProfilePicFrg getProfilePicFragment();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {ProfileVideoBuilder.class})
 abstract ProfileVideoFrg getProfileVideoFragment();
}
