package com.androidinsta.com;

/**
 * <h2>Configuration</h2>
 * <P>
 *  configuration structure for the configuration call for the
 *  user.
 * </P>
 * @since  12/12/2017.
 * @author Suresh.
 * @version 1.0.
 */
interface Configuration
{
    void setOAuthConsumerKey(String client_ID);
    void setOAuthConsumerSecret(String clientSecret);
    String getOAuthConsumerKey();
    String getOAuthConsumerSecret();
}
