package com.helloPerfect.com.ChatMessageScreen.Model;

import com.helloPerfect.com.BaseModel;
import com.helloPerfect.com.MatchedView.MatchResponseData;
import com.helloPerfect.com.data.model.CoinBalanceHolder;
import com.helloPerfect.com.data.source.PreferenceTaskDataSource;
import com.helloPerfect.com.home.Chats.Model.ChatListData;
import com.helloPerfect.com.home.Chats.Model.MqttUnmatchResponse;
import com.helloPerfect.com.home.Discover.Model.superLike.SuperLikeResponse;
import com.helloPerfect.com.home.HomeModel.LikeResponse;
import com.helloPerfect.com.util.ApiConfig;
import com.helloPerfect.com.util.Exception.DataParsingException;
import com.helloPerfect.com.util.Exception.EmptyData;
import com.helloPerfect.com.util.Exception.SimilarDataException;
import com.helloPerfect.com.util.Utility;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

public class ChatMessageModel  extends BaseModel {

    @Inject
    CoinBalanceHolder coinBalanceHolder;

    @Inject
    Utility utility;
    @Inject
    PreferenceTaskDataSource dataSource;

    @Inject
    public ChatMessageModel() {
    }

    public String getCoinBalance() {
        return coinBalanceHolder.getCoinBalance();
    }



    public void parseCoinBalance(String response) {

    }

    public void parseCoinConfig(String response)
    {

    }

    /*
     *Creating the form data for like service. */
    public Map<String, Object> getUserDetails(String user_id)
    {
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.DoLikeService.USER_ID, user_id);
        return map;
    }

    public void parseSuperLike(String response)
    {
        try
        {
            SuperLikeResponse superLikeResponse = utility.getGson().fromJson(response, SuperLikeResponse.class);
            if(superLikeResponse.getCoinWallet() != null) {
                coinBalanceHolder.setCoinBalance(String.valueOf(superLikeResponse.getCoinWallet().getCoin()));
                coinBalanceHolder.setUpdated(true);
            }
        }catch (Exception e){}

    }

    public int getRemainsLikeCount() {
        try {
            String str = dataSource.getRemainsLikesCount();
            if(str.equalsIgnoreCase("unlimited"))
                return 1;
            return Integer.parseInt(str);
        }catch (Exception e){
            return 0;
        }
    }

    public void parseLikeResponse(String response) {
        try{
            LikeResponse likeResponse = utility.getGson().fromJson(response,LikeResponse.class);
            dataSource.setRemainsLinksCount(likeResponse.getRemainsLikesInString());
            dataSource.setNextLikeTime(likeResponse.getNextLikeTime());
        }catch (Exception e){

        }
    }

    public ChatListData parseMatchedMessage(JSONObject obj) throws Exception, SimilarDataException, EmptyData {
        try
        {
            MatchResponseData data=utility.getGson().fromJson(obj.toString(),MatchResponseData.class);
            if(data==null)
            {
                throw  new EmptyData("ChatListData is empty!");
            }
            ChatListData chatListData = new ChatListData();
            if(data.getFirstLikedBy().equals(dataSource.getUserId()))
            {
                chatListData.setFirstName(data.getSecondLikedByName());
                chatListData.setProfilePic(data.getSecondLikedByPhoto());
                chatListData.setRecipientId(data.getSecondLikedBy());
                chatListData.setSuperlikedMe(data.getIsSecondSuperLiked());
                chatListData.setChatId(data.getChatId());
            }else
            {
                chatListData.setFirstName(data.getFirstLikedByName());
                chatListData.setProfilePic(data.getFirstLikedByPhoto());
                chatListData.setRecipientId(data.getFirstLikedBy());
                chatListData.setSuperlikedMe(data.getIsFirstSuperLiked());
                chatListData.setChatId(data.getChatId());
            }
            return chatListData;
        }catch (Exception e)
        {
            throw new DataParsingException(e.getMessage());
        }
    }


    public String parseUserId(String response) {
        try{
            MqttUnmatchResponse mqttUnmatchResponse = utility.getGson().fromJson(response,MqttUnmatchResponse.class);
            return mqttUnmatchResponse.getTargetId();
        }catch (Exception e){

        }
        return "";
    }
}
