package com.suresh.innapp_purches;
import android.app.Activity;
import android.content.Intent;
import java.util.List;

public interface InnAppVendor
{
    /**
     * For testing purpuse use this key to buy from palystore
     */
    String testing_id="com.test.purchased";

    String getTesting_id();

    SkuDetails getProductDetails(String prductId,InnAppSdk.Type type);

    /**
     * getting data in background
     */
    void getProductDetails(Activity activity,List<String>  prductIds,InnAppSdk.Type type,InAppCallBack.DetailsCallback callback);

    void purchasedItem(Activity activity,String productId,InnAppSdk.Type type,String payload,InAppCallBack.OnPurchased onPurchased);

    void consumeProduct(Activity activity,String productId,InAppCallBack.onConsume onConsume);

    List<String> getOwenProduct();

    List<String> getOwenSubscription();

    boolean onActivity_result(int requestCode, int resultCode, Intent data);
}
