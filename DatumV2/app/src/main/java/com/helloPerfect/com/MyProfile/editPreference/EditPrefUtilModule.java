package com.helloPerfect.com.MyProfile.editPreference;

import android.app.Activity;
import com.helloPerfect.com.dagger.ActivityScoped;
import com.helloPerfect.com.util.ProgressAleret.DatumProgressDialog;
import com.helloPerfect.com.util.TypeFaceManager;

import dagger.Module;
import dagger.Provides;

/**
 * <h>EditPrefUtilModule</h>
 * @author 3Embed.
 * @since 23/4/18.
 */

@Module
public class EditPrefUtilModule {

    @Provides
    @ActivityScoped
    DatumProgressDialog datumProgressDialog(Activity activity, TypeFaceManager typeFaceManager)
    {
        return  new DatumProgressDialog(activity,typeFaceManager);
    }

    @Provides
    @ActivityScoped
    EditAnimatorHandler animatorHandler(Activity activity)
    {
        return new EditAnimatorHandler(activity);
    }

}
