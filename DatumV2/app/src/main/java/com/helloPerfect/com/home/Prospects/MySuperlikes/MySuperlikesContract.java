package com.helloPerfect.com.home.Prospects.MySuperlikes;
import com.helloPerfect.com.BasePresenter;
import com.helloPerfect.com.BaseView;
import com.helloPerfect.com.home.Prospects.OnAdapterItemClicked;

/**
 * @since  3/23/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface MySuperlikesContract
{
    interface View extends BaseView
    {
        void showError(int id);
        void showError(String message);
        void showMessage(int message);
        void onDataUpdate();
        void showProgress();
        void onApiError(String message);
        void emptyData();
        void adapterListener(OnAdapterItemClicked adapterCallabck);
        void openUserProfile(String data);
    }

    interface Presenter extends BasePresenter<View>
    {
        void initAdapterListener();
        void initMatchListener();
        void  getListData(boolean isLoadMore);
        void pee_fetchProfile(int position);
        boolean checkLoadMore(int position);
    }
}
