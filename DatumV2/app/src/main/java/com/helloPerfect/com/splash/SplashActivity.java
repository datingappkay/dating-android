package com.helloPerfect.com.splash;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;

import com.helloPerfect.com.AppController;
import com.helloPerfect.com.MqttChat.Database.CouchDbController;
import com.helloPerfect.com.R;
import com.helloPerfect.com.data.source.PreferenceTaskDataSource;
import com.helloPerfect.com.home.HomeActivity;
import com.helloPerfect.com.login.LoginActivity;
import com.helloPerfect.com.util.AppConfig;
import com.helloPerfect.com.util.TypeFaceManager;
import com.helloPerfect.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * <h2>SplashActivity</h2>
 * <p>
 * Starts the app.
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 04/01/2018.
 **/
public class SplashActivity extends BaseDaggerActivity implements SplashContract.View, MediaPlayer.OnCompletionListener {

    private static final String TAG = SplashActivity.class.getCanonicalName();

    @Inject
    SplashContract.Presenter splashPresenter;
    @Inject
    PreferenceTaskDataSource preferencesHelper;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    CouchDbController couchDbController;
    @Inject
    Activity activity;
    @Inject
    TypeFaceManager typeFaceManager;

    @BindView(R.id.video_view)
    VideoView videoView;
    @BindView(R.id.app_logo_layout)
    FrameLayout appLogoLayout;
    @BindView(R.id.iv_app_logo)
    ImageView ivAppLogo;
    @BindView(R.id.progress_loader)
    ProgressBar progressLoader;
    @BindView(R.id.tv_error_msg)
    TextView tvErrorMsg;
    @BindView(R.id.btn_retry)
    Button btnRetry;
    private Bundle bundle;
    private Unbinder unbinder;
    private MediaPlayer mediaPlayer;


    @SuppressLint("WrongThread")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(!dataSource.isSplashFirstTime())
            setTheme(R.style.SplashTheme);
        else
            setTheme(R.style.SplashThemeWhiteBg);

        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        setContentView(R.layout.activity_splash);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        applyFont();
        if(dataSource.isSplashFirstTime()) {
            appLogoLayout.setVisibility(View.VISIBLE);
            videoView.setVisibility(View.VISIBLE);
            ivAppLogo.setVisibility(View.VISIBLE);
            initView();
        }
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        String token = Objects.requireNonNull(task.getResult()).getToken();

                        // Log and toast
                        String msg = getString(R.string.msg_token_fmt, token);
                        Log.d(TAG, msg);
                        //Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                        dataSource.setPushToken(token);
                    }
                });
        onNewIntent(getIntent());
        splashPresenter.redirect();
        generateHashKey();
    }

    private void generateHashKey() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.helloPerfect.com", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("KeyHash:", e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("KeyHash:", e.toString());
        }
    }

    @Override
    public void invalidateSelelectedAppLanguage() {
        String localLanguage = Locale.getDefault().getLanguage();
        boolean found = false;
        for(String applanguage: AppConfig.APP_LANGUAGES){
            if(applanguage.equals(localLanguage)){
                found = true;
            }
        }
        Log.d(TAG, "invalidateSelelectedAppLanguage: localLanguage: "+localLanguage);
        if(found){
            AppController.getInstance().setCurrentAppLanguage(localLanguage);
        }
        else {
            AppController.getInstance().setCurrentAppLanguage(AppConfig.DEFAULT_LANGUAGE);
        }
    }

    private void applyFont() {
        btnRetry.setTypeface(typeFaceManager.getCircularAirBook());
        tvErrorMsg.setTypeface(typeFaceManager.getCircularAirBook());
    }

    private void initView() {
        Uri video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.splash);
        videoView.setOnCompletionListener(this);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.start();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        appLogoLayout.setAlpha(0);
                    }
                },300);
            }

        });
        videoView.setVideoURI(video);
        //videoView.start();
    }

    @OnClick(R.id.btn_retry)
    public void retry(){
//        splashPresenter.checkAndGetAppData();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        bundle = null;
        bundle = intent.getBundleExtra("data");
        super.onNewIntent(intent);
    }


    @Override
    public void onResume() {
        super.onResume();
        splashPresenter.redirect();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void move()
    {
        if (preferencesHelper.isLoggedIn())
        {
            updateUserDetails(preferencesHelper.getUserId(),preferencesHelper.getName());
            Intent intent1 = new Intent(this,HomeActivity.class);
            //intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent1.putExtra("data",bundle);
            startActivity(intent1);
        }
        else {
            Intent intent = new Intent(this, LoginActivity.class);
            //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        this.finish();
    }

    @Override
    public void showError(String error) {
        if(tvErrorMsg != null){
            tvErrorMsg.setText(error);
            progressLoader.setVisibility(View.GONE);
            tvErrorMsg.setVisibility(View.VISIBLE);
            btnRetry.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showLoading() {
        if(progressLoader != null) {
            progressLoader.setVisibility(View.VISIBLE);
            tvErrorMsg.setText(activity.getString(R.string.loading_text));
            progressLoader.setVisibility(View.VISIBLE);
            tvErrorMsg.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showInternetError(String error) {
        if(tvErrorMsg != null) {
            tvErrorMsg.setText(error);
            progressLoader.setVisibility(View.GONE);
            tvErrorMsg.setVisibility(View.VISIBLE);
        }
    }

    /*creating the user doc id.*/
    private void updateUserDetails(String userId, String userName)
    {
        if (couchDbController.checkUserDocExists(AppController.getInstance().getIndexDocId(),userId)) {
            AppController.getInstance().getUserDocIdsFromDb(userId);
            AppController.getInstance().setSignedIn(true,true,userId,preferencesHelper.getName(),userId);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        videoView.stopPlayback();
        unbinder.unbind();
        splashPresenter.dispose();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        //move();
    }
}

