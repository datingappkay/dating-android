package com.androidinsta.com;

/**
 * <h2>InstaLoginHolder</h2>
 * <P>
 *
 * </P>
 * @since  12/13/2017.
 * @version 1.0.
 * @author Suresh.
 */
public class InstaLoginHolder
{
    private boolean isError;
    private String errorMessage;
    private String full_name;
    private String profile_pic;
    private String user_name;
    private String user_id;
    private String token;
    public String getUser_name() {
        return user_name;
    }
    public String getProfile_pic() {
        return profile_pic;
    }
    public boolean isError() {
        return isError;
    }

    void setError(boolean error) {
        isError = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getFull_name() {
        return full_name;
    }

    void setFull_name(String full_name) {
        this.full_name = full_name;
    }
    void setUser_name(String user_name) {
        this.user_name = user_name;
    }
    void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
