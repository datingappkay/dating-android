package com.helloPerfect.com.fbRegister.Name;
import com.helloPerfect.com.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h2>FbNameFragBuilder</h2>
 * <P>
 *
 * </P>
 * @since  2/16/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface FbNameFragBuilder
{

    @FragmentScoped
    @Binds
    FbNameFragment getEmailFragment(FbNameFragment nameFragment);

    @FragmentScoped
    @Binds
    FbNameContract.Presenter taskPresenter(FbNameFrgPresenter presenter);
}
