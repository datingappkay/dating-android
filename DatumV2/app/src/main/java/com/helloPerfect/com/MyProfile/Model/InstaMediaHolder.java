package com.helloPerfect.com.MyProfile.Model;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import com.helloPerfect.com.R;

public class InstaMediaHolder extends RecyclerView.ViewHolder
{
    RecyclerView item_grid;
    public InstaMediaHolder(View itemView)
    {
        super(itemView);
        item_grid=itemView.findViewById(R.id.item_grid);
    }
}
