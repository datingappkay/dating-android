package com.helloPerfect.com.home.Chats.Model;


public interface ChatAdapterItemCallback
{
    void initiateChat(int position);
    void reportUser(int position);
    void blockUser(int position);
    void unBlockUser(int position);
    void unMatchActiveUser(int position);
    void deleteActiveUserChat(int position);
}
