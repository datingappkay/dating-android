package com.helloPerfect.com.networking;

/**
 * @since  12/20/2017.
 */
public abstract class TimerChecker
{
    public abstract void run();
}
