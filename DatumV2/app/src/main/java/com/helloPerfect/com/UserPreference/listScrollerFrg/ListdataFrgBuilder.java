package com.helloPerfect.com.UserPreference.listScrollerFrg;

import com.helloPerfect.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;
/**
 *@since  2/22/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface ListdataFrgBuilder
{
    @FragmentScoped
    @Binds
    ListdataFrg getConChoiceFragment(ListdataFrg listdataFrg);
    @FragmentScoped
    @Binds
    ListdataFrgContract.Presenter taskPresenter(ListdataFrgPresenter presenter);
}
