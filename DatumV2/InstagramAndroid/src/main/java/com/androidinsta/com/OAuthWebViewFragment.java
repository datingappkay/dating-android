package com.androidinsta.com;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.androidinsta.com.ProgressDialog.CircleProgress;
/**
 * <h2>OAuthWebViewFragment</h2>
 * <P>
 *  Auth verification manager of the user details.
 * </P>
 * @since  12/12/2017.
 * @author Suresh.
 * @version 1.0.
 */
public class OAuthWebViewFragment extends Fragment
{
    private WebView webView;
    private String authenticationUrl;
    private Activity mactivity;
    private InstagramSession instagramSession;
    private CircleProgress circleProgress;

    public static OAuthWebViewFragment getInstance(String authenticationUrl)
    {
        OAuthWebViewFragment temp=new OAuthWebViewFragment();
        temp.authenticationUrl= authenticationUrl;
        return temp;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        mactivity=getActivity();
        circleProgress=new CircleProgress(mactivity);
        instagramSession =new InstagramSession(mactivity);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.oauth_webview,container,false);
        webView = view.findViewById(R.id.webViewOAuth);
        clearCookies(getActivity());
        return view;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        CookieSyncManager.createInstance(getContext());
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();
        webView.loadUrl(authenticationUrl);
        webView.setWebViewClient(new OAuthWebViewClient());
        WebSettings webSettings= webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
    }

    /*
     *providing the api client for the user details. */
    private class OAuthWebViewClient extends WebViewClient
    {
        RequestToken requestToken;
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {

            if(checkUrl(Uri.parse(url)))
            {
                if(url.contains("error=access_denied"))
                {
                    userAccessFailure("access_denied");
                }else if(url.contains("code="))
                {
                    requestToken=InstagramUtil.getInstance().getRequestToken();
                    view.loadUrl(requestToken.getAccessTokenUrl());
                }else if(url.contains("#access_token="))
                {
                    if(requestToken==null&&mactivity!=null)
                    {
                        mactivity.finish();
                    }
                    String data[]=url.split("=");
                    collectUserDetails(requestToken,data[1]);
                }else
                {
                    view.loadUrl(url);
                }
            }else if(url.contains("denied="))
            {
                userAccessFailure("denied error"+url);
            }else
            {
                view.loadUrl(url);
            }
            return true;
        }
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            super.onPageStarted(view, url, favicon);
            circleProgress.show();
        }

        @Override
        public void onPageFinished(WebView view, String url)
        {
            super.onPageFinished(view, url);
            circleProgress.cancel();
        }
    }

    /*
     *Call back url error */
    private boolean checkUrl(Uri uri)
    {
        return uri != null && uri.toString().startsWith(InstagramConfig.INSTAGRAM_CALLBACK_URL);
    }

    /*
     *Handle permission in android. */
    private void collectUserDetails(RequestToken requestToken, String accessToken)
    {
        new GetUserDetails().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,requestToken,accessToken);
    }

    @SuppressLint("StaticFieldLeak")
    private class GetUserDetails extends AsyncTask<Object, String,AccessToken>
    {
        @Override
        protected  AccessToken doInBackground(Object... param)
        {
            RequestToken requestToken= (RequestToken) param[0];
            String accessToken= (String) param[1];
            Instagram instagram=InstagramUtil.getInstance().getInstagram();
            return instagram.getUserDetails(requestToken,accessToken);
        }

        @Override
        protected void onPostExecute(AccessToken accessToken)
        {
            super.onPostExecute(accessToken);
            InstagramUtil.getInstance().getInstagram().setOAuthAccessToken(accessToken);
            if(accessToken!=null)
            {
                instagramSession.setAccessToken(accessToken.getACCESS_TOKEN());
                instagramSession.setId(accessToken.getUSER_ID());
                instagramSession.setUserName(accessToken.getUSER_NAME());
                instagramSession.setUserFullName(accessToken.getFULL_NAME());
                instagramSession.setUserBio(accessToken.getUSER_BIO());
                instagramSession.setUserWeb(accessToken.getWEB_SITE());
                instagramSession.setUserPic(accessToken.getUSER_PIC());
                instagramSession.setInstagramLoginStatus(true);
                instagramSession.setAuthenticatedAppName(accessToken.getFULL_NAME());
                setSuccessResult(true,"Success");
            }else
            {
                userAccessFailure("Error on getting accessToken");
            }
        }
    }
    /*
     *Got some error */
    private void userAccessFailure(String message)
    {
        instagramSession.setInstagramLoginStatus(false);
        Intent intent=new Intent();
        Bundle data=new Bundle();
        data.putBoolean("ISLOGIN",false);
        data.putString("Message",message);
        intent.putExtras(data);
        if(mactivity!=null)
        {
            mactivity.setResult(Activity.RESULT_OK,intent);
            mactivity.finish();
        }
    }
    /*
     * Success logged in result back*/
    private void setSuccessResult(boolean isSuccess,String message)
    {
        Intent intent=new Intent();
        Bundle data=new Bundle();
        data.putBoolean("ISLOGIN",isSuccess);
        data.putString("Message",message);
        intent.putExtras(data);
        if(mactivity!=null)
        {
            mactivity.setResult(Activity.RESULT_OK,intent);
            mactivity.finish();
        }
    }
    public void clearCookies(Context context)
    {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else
        {
            CookieSyncManager cookieSyncMngr=CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager=CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }
}

