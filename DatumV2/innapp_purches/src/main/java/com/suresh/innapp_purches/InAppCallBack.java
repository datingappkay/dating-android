package com.suresh.innapp_purches;
import java.util.List;
public interface InAppCallBack
{
    interface OnPurchased
    {
        void onSuccess(String productId);
        void onError(String id,String error);
    }

    interface onConsume
    {
        void onSuccess();
        void onError(String id,String error);
    }

    interface DetailsCallback
    {
        void onSuccess(List<SkuDetails> list,List<String> errorList);
    }
}

