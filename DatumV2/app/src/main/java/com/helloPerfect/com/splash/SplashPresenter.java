package com.helloPerfect.com.splash;

import android.app.Activity;

import com.helloPerfect.com.data.source.PreferenceTaskDataSource;
import com.helloPerfect.com.networking.NetworkService;
import com.helloPerfect.com.networking.NetworkStateHolder;
import com.helloPerfect.com.splash.Model.SplashModel;
import com.helloPerfect.com.util.AppConfig;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * <h2>SplashPresenter</h2>
 *
 * @author 3Embed.
 * @version 1.0.
 * @since 04/01/2018.
 */

public class SplashPresenter implements SplashContract.Presenter
{
    @Inject
    NetworkService service;
    @Inject
    SplashContract.View view;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    SplashModel model;
    @Inject
    Activity activity;


    private boolean timerCompleted = false;
    private CompositeDisposable compositeDisposable;

    @Inject
    SplashPresenter() {
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void redirect()
    {
        boolean isFirstTime = dataSource.isSplashFirstTime();
        int loadTime = isFirstTime?AppConfig.SPLASH_SCREEN_DURATION:AppConfig.LOAD_SCREEN;
        if(dataSource.isSplashFirstTime()){
            dataSource.setSplashFirstTimeDone();
        }
        Completable.
                timer(loadTime, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onComplete() {
                        timerCompleted = true;
                            if(view != null)
                                view.move();
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                });
    }

    @Override
    public void dispose() {
        compositeDisposable.clear();
    }


}
