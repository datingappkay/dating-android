package com.facebookmanager.com;

/**
 * @since 12/14/2017.
 * @author Suresh.
 * @version 1.0.
 */
class FacebookException extends Exception
{
    private String message;

    FacebookException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
