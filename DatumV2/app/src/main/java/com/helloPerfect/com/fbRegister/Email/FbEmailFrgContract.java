package com.helloPerfect.com.fbRegister.Email;

import com.helloPerfect.com.BasePresenter;
import com.helloPerfect.com.BaseView;

/**
 * @since  2/15/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface FbEmailFrgContract
{
    interface View extends BaseView
    {
        void  showError(String message);

        void moveNextFragment(String email);
        /**
         * <p>Action when email is not available in database</p>
         */
        void emailNotAvailable();
    }

    interface Presenter extends BasePresenter
    {
       boolean validateEmail(String mail);
        /**
         * <p>Checks email address is available in database or not</p>
         *
         * @param email: email address
         */
        void checkEmailIdExist(String email);

    }
}
