package com.helloPerfect.com.ChatMessageScreen;

import android.app.Activity;

import com.helloPerfect.com.dagger.ActivityScoped;
import com.helloPerfect.com.data.source.PreferenceTaskDataSource;

import com.helloPerfect.com.util.TypeFaceManager;
import com.helloPerfect.com.util.Utility;
import com.helloPerfect.com.util.progressbar.LoadingProgress;
import com.helloPerfect.com.util.timerDialog.TimerDialog;

import dagger.Module;
import dagger.Provides;

@Module
public class ChatMessageUtil {

    @ActivityScoped
    @Provides
    LoadingProgress provideLoadingProgress(Activity activity){
        return new LoadingProgress(activity);
    }


    @ActivityScoped
    @Provides
    TimerDialog timerDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility, PreferenceTaskDataSource dataSource){
        return  new TimerDialog(activity,typeFaceManager,utility,dataSource);
    }
}
