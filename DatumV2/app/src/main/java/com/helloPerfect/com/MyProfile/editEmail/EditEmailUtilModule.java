package com.helloPerfect.com.MyProfile.editEmail;

import android.app.Activity;

import com.helloPerfect.com.dagger.ActivityScoped;
import com.helloPerfect.com.util.ProgressAleret.DatumProgressDialog;
import com.helloPerfect.com.util.TypeFaceManager;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ankit on 27/4/18.
 */

@Module
public class EditEmailUtilModule {

    @Provides
    @ActivityScoped
    DatumProgressDialog datumProgressDialog(Activity activity, TypeFaceManager typeFaceManager)
    {
        return  new DatumProgressDialog(activity,typeFaceManager);
    }

}
