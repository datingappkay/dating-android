package com.androidinsta.com;

/**
 * <h2>ImageData</h2>
 * @since  12/13/2017.
 * @author Suresh.
 * @version 1.0.
 */
public class ImageData
{
    private String id;
    private String thumbnail;
    private String standard_resolution;
    private String low_resolution;

    public String getId()
    {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getStandard_resolution() {
        return standard_resolution;
    }

    public void setStandard_resolution(String standard_resolution) {
        this.standard_resolution = standard_resolution;
    }

    public String getLow_resolution() {
        return low_resolution;
    }

    public void setLow_resolution(String low_resolution) {
        this.low_resolution = low_resolution;
    }
}
