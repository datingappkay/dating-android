package com.helloPerfect.com.login;

import android.app.Activity;
import android.content.Context;
import android.location.Geocoder;

import com.helloPerfect.com.dagger.ActivityScoped;
import com.helloPerfect.com.util.TypeFaceManager;
import com.helloPerfect.com.util.progressbar.LoadingProgress;

import java.util.Locale;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginUtil
{
    @ActivityScoped
    @Provides
    SliderAdapter getAdapter(Context context,TypeFaceManager typeFaceManager)
    {
        return  new SliderAdapter(context,typeFaceManager);
    }

    @ActivityScoped
    @Provides
    AnimatorHandler getAnimationHandler(Context context)
    {
        return new AnimatorHandler(context);
    }

    @ActivityScoped
    @Provides
    LoadingProgress getLoadingProgress(Activity activity)
    {
        return new LoadingProgress(activity);
    }

    @ActivityScoped
    @Provides
    Geocoder provideGeocoder(Activity activity)
    {
        return new Geocoder(activity, Locale.getDefault());
    }
}
