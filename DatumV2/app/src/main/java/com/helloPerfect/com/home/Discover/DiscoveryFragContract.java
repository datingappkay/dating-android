package com.helloPerfect.com.home.Discover;
import android.content.Intent;

import com.helloPerfect.com.BasePresenter;
import com.helloPerfect.com.BaseView;
import com.helloPerfect.com.home.Discover.Model.UserItemPojo;

/**
 * <h2>DiscoveryFragContract</h2>
 * Class represent the structure of the work flow of the Discovery page.
 * @since  2/27/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface DiscoveryFragContract
{
    interface View extends BaseView
    {
        void hideUserSearchMessage();
        void showUserSearchMessage(String msg);
        void showError(int id);
        void showError(String error);
        void showMessage(String message);
        void user_found();
        void noUserFound();
        void showNetworkError();
        void onLike(String user_id);
        void onDislike(String user_id);
        void onSuperLike(String user_id);
        int  getPagePosition();
        void openBootsDailoag();
        void launchChatScreen(Intent intent);
        void updateListener(UserActionEventError callback);
        void setNeedToUpdateChat(boolean yes);


        void checkOnlyForBoost();
        void showLoadedProfileAds();
    }

    interface Presenter extends BasePresenter
    {
        void showSearchView();
        void updateCurrentLocation(boolean isReferesh);
        void onUsersReceived();
        void preFetchImage(int position);
        void doLike(UserItemPojo item);
        void doDisLike(UserItemPojo item);
        void doSuperLike(UserItemPojo item);
        void askForLocationPermission();
        void getUsers();
        void getUserLocationFromApi();
        void onRewind();
        void openChat(UserItemPojo userItemPojo);
        void showBoostViewCounter(boolean show);
        void startCoinAnimation();
        void saveCurrentBoostViewCount(int viewCount);
        void initBoostViewCountObserver();
        void initBoostEndObserver();
        void startVideoPlayFirstItem();
        void handleAdShow();
    }
}
