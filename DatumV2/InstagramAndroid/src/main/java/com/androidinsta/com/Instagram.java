package com.androidinsta.com;

/**
 * <h2>Instagram</h2>
 * <P>
 *
 * </P>
 * @since  12/12/2017.
 * @author Suresh.
 * @version 1.0.
 */
interface Instagram
{
    RequestToken getOAuthRequestToken(String callbackUrl);
    void setOAuthAccessToken(AccessToken authAccessToken);
    AccessToken getUserDetails(RequestToken requestToken, String oauthVerifier);
    String getUserRecentPost();
    String getOtherRecentPost(String userID,String userInstaToken) throws InstagramException;
    boolean isAccessTokenCreated();
    AccessToken recreateAccessToke(InstagramSession instagramSession);
}
