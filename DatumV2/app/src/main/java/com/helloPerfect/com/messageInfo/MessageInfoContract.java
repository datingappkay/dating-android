package com.helloPerfect.com.messageInfo;

import com.helloPerfect.com.BasePresenter;
import com.helloPerfect.com.BaseView;

public interface MessageInfoContract {
    interface View extends BaseView {

        void applyFonts();
        void showMessage(String message);
    }
    interface Presenter extends BasePresenter<MessageInfoContract.View> {

    }
}
