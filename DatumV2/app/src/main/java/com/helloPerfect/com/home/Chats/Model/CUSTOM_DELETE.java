package com.helloPerfect.com.home.Chats.Model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by ankit on 2/8/18.
 */

@Target(ElementType.METHOD) @Retention(RetentionPolicy.RUNTIME)

public @interface CUSTOM_DELETE {
}
