package com.helloPerfect.com.selectLanguage.model;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.helloPerfect.com.R;
import com.helloPerfect.com.editProfile.Model.ViewHolderClickCallback;
import com.helloPerfect.com.util.TypeFaceManager;

public class LanguageItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private TypeFaceManager typeFaceManager;
    private ViewHolderClickCallback clickCallback;

    public ImageView ivCheck;
    public TextView tvLanguageName;

    public LanguageItemViewHolder(@NonNull View itemView, TypeFaceManager typeFaceManager, ViewHolderClickCallback clickCallback) {
        super(itemView);
        this.typeFaceManager = typeFaceManager;
        this.clickCallback = clickCallback;

        ivCheck = itemView.findViewById(R.id.iv_language);
        tvLanguageName = itemView.findViewById(R.id.tv_language_title);
        tvLanguageName.setTypeface(typeFaceManager.getCircularAirBook());

        itemView.findViewById(R.id.ll_root).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(clickCallback != null)
            clickCallback.onClick(v,getAdapterPosition());
    }
}
