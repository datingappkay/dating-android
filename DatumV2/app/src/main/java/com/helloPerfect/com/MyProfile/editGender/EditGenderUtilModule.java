package com.helloPerfect.com.MyProfile.editGender;

import android.app.Activity;

import com.helloPerfect.com.dagger.ActivityScoped;
import com.helloPerfect.com.register.AnimatorHandler;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ankit on 27/4/18.
 */

@Module
public class EditGenderUtilModule {

    @Provides
    @ActivityScoped
    AnimatorHandler animatorHandler(Activity activity)
    {
        return new AnimatorHandler(activity);
    }

}
