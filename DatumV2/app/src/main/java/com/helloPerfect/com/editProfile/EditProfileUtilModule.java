package com.helloPerfect.com.editProfile;

import android.app.Activity;
import android.content.Context;
import androidx.recyclerview.widget.GridLayoutManager;

import com.helloPerfect.com.dagger.ActivityScoped;
import com.helloPerfect.com.editProfile.Model.ProfilePhotoAdapter;
import com.helloPerfect.com.editProfile.Model.ProfilePicture;
import com.helloPerfect.com.editProfile.ProfileAlert.ProfileAlertDialog;
import com.helloPerfect.com.util.App_permission;
import com.helloPerfect.com.util.GridSpacingItemDecoration;
import com.helloPerfect.com.util.ImageChecker.ImproperImageAlert;
import com.helloPerfect.com.util.MediaBottomSelector;
import com.helloPerfect.com.util.TypeFaceManager;
import com.helloPerfect.com.util.Utility;
import com.helloPerfect.com.util.progressbar.LoadingProgress;
import com.videocompressor.com.CompressImage;
import com.videocompressor.com.VideoCompressor;

import java.util.ArrayList;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * <h>EditPrefUtilModule class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

@Module
public class EditProfileUtilModule {

    @ActivityScoped
    @Provides
    LoadingProgress getLoadingProgress(Activity activity)
    {
        return new LoadingProgress(activity);
    }

    @Named("PIC_LIST")
    @ActivityScoped
    @Provides
    ArrayList<ProfilePicture> providePictureList()
    {
        return new ArrayList<ProfilePicture>();
    }

    @ActivityScoped
    @Provides
    ProfilePhotoAdapter provideProfilePhotoAdapter(Activity activity,@Named("PIC_LIST") ArrayList<ProfilePicture> profilePictures)
    {
        return new ProfilePhotoAdapter(activity,profilePictures);
    }

    @Provides
    @ActivityScoped
    GridLayoutManager provideGridLayoutManager(Activity activity)
    {
        return new GridLayoutManager(activity,3);
    }

    @Provides
    @ActivityScoped
    GridSpacingItemDecoration getGridSpacingItemDecoration(Utility utility)
    {
        return  new GridSpacingItemDecoration(3,utility.dpToPx(10), false);
    }

    @Provides
    @ActivityScoped
    MediaBottomSelector getMediaBottomSelector(Activity activity, TypeFaceManager typeFaceManager)
    {
        return new MediaBottomSelector(activity,typeFaceManager);
    }

    @Provides
    @ActivityScoped
    App_permission getApp_permission(Activity activity, TypeFaceManager typeFaceManager)
    {
        return new App_permission(activity,typeFaceManager);
    }

    @ActivityScoped
    @Provides
    CompressImage compressImage()
    {
        return new CompressImage();
    }

    @ActivityScoped
    @Provides
    VideoCompressor getVideoCompressor(Context context)
    {
        return new VideoCompressor(context);
    }

    @ActivityScoped
    @Provides
    ImproperImageAlert getImproperImageAlert(Activity activity, TypeFaceManager typeFaceManager)
    {
        return new ImproperImageAlert(activity,typeFaceManager);
    }

    @ActivityScoped
    @Provides
    ProfileAlertDialog provideProfileAlertDialog(Activity activity, TypeFaceManager typeFaceManager)
    {
        return new ProfileAlertDialog(activity,typeFaceManager);
    }

}
