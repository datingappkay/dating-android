package com.helloPerfect.com.userProfile;

import android.app.Activity;

import androidx.fragment.app.FragmentManager;

import com.helloPerfect.com.dagger.ActivityScoped;
import com.helloPerfect.com.data.source.PreferenceTaskDataSource;
import com.helloPerfect.com.userProfile.AleretBox.BottomDialog;
import com.helloPerfect.com.userProfile.Model.MediaPojo;
import com.helloPerfect.com.userProfile.Model.UserMediaAdapter;
import com.helloPerfect.com.util.ProgressAleret.DatumProgressDialog;
import com.helloPerfect.com.util.ReportUser.ReportUserDialog;
import com.helloPerfect.com.util.TypeFaceManager;
import com.helloPerfect.com.util.Utility;
import com.helloPerfect.com.util.boostDialog.BoostDialog;
import com.helloPerfect.com.util.progressbar.LoadingProgress;
import com.helloPerfect.com.util.timerDialog.TimerDialog;

import java.util.ArrayList;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
/**
 *<h2>Profile_util</h2>
 * <P>
 *     Use proile util class to provide required details.
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 17-03-2018.*/
@Module
public class Profile_util
{
    @ActivityScoped
    @Provides
    ArrayList<MediaPojo> getImageList()
    {
        return  new ArrayList<>();
    }

    @ActivityScoped
    @Provides
    UserMediaAdapter getListAdapter(@Named(UserProfileBuilder.USER_FRAGMENT_MANAGER)FragmentManager fm ,ArrayList<MediaPojo> data)
    {
        return new UserMediaAdapter(fm,data);
    }

    @Provides
    @ActivityScoped
    BottomDialog getBottomDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility)
    {
        return new BottomDialog(activity,typeFaceManager,utility);
    }

    @Provides
    @ActivityScoped
    ReportUserDialog provideReportUserDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility)
    {
        return new ReportUserDialog(activity,typeFaceManager,utility);
    }

    @Provides
    @ActivityScoped
    ArrayList<String> reportReasonList(){
        return new ArrayList<>();
    }

    @Provides
    @ActivityScoped
    LoadingProgress provideLoadingProgress(Activity activity){
        return new LoadingProgress(activity);
    }

    @Provides
    @ActivityScoped
    BoostDialog getBootsDialog(Activity activity, TypeFaceManager typeFaceManager, PreferenceTaskDataSource dataSource,Utility utility)
    {
        return new BoostDialog(activity,typeFaceManager,dataSource,utility);
    }

    @Provides
    @ActivityScoped
    DatumProgressDialog getDatumProgressDialog(Activity activity, TypeFaceManager typeFaceManager)
    {
        return new DatumProgressDialog(activity,typeFaceManager);
    }


    @ActivityScoped
    @Provides
    TimerDialog provideTimerDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility, PreferenceTaskDataSource dataSource){
        return new TimerDialog(activity,typeFaceManager,utility,dataSource);
    }
}
