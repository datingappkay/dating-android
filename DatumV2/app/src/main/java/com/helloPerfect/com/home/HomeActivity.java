package com.helloPerfect.com.home;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.helloPerfect.com.AppController;
import com.helloPerfect.com.R;
import com.helloPerfect.com.data.source.PreferenceTaskDataSource;
import com.helloPerfect.com.home.AppSetting.AppSettingFrag;
import com.helloPerfect.com.home.Chats.ChatsFragment;
import com.helloPerfect.com.home.Discover.DiscoveryFrag;
import com.helloPerfect.com.home.Discover.DiscoveryFragUtil;
import com.helloPerfect.com.home.Discover.Model.UserItemPojo;
import com.helloPerfect.com.home.Prospects.ProspectsFrg;
import com.helloPerfect.com.util.AppConfig;
import com.helloPerfect.com.util.CustomVideoView.CustomVideoSurfaceView;
import com.helloPerfect.com.util.CustomVideoView.NoInterNetView;
import com.helloPerfect.com.util.LocationProvider.Location_service;
import com.helloPerfect.com.util.localization.activity.BaseDaggerActivity;
import com.suresh.innapp_purches.InnAppSdk;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
/**
 * <h2>HomeActivity</h2>
 * <p>
 * Home screen
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 24/01/2018.
 **/
public class HomeActivity extends BaseDaggerActivity implements HomeContract.View
{
    private static final String TAG = HomeActivity.class.getSimpleName();

    @Inject
    Activity activity;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    DiscoveryFrag discoveryFrg;
    @Inject
    ProspectsFrg prospectsFrg;
    @Inject
    ChatsFragment chatsFragment;
    @Inject
    AppSettingFrag appSettingFrg;
    @Inject
    @Named(HomeUtil.HOME_FRAGMENT_MANAGER)
    FragmentManager fragmentManager;
    @Inject
    HomeContract.Presenter presenter;
    @Inject
    @Named(DiscoveryFragUtil.USER_LIST)
    ArrayList<UserItemPojo> userList;
    @BindView(R.id.navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.parent_layout)
    CoordinatorLayout parent_layout;
    @BindView(R.id.no_internetView)
    NoInterNetView noInterNetView;
    @BindView(R.id.home_frg_container)
    FrameLayout fragContainer;
    private TextView tvChatCount;
    private Unbinder unbinder;
    public static boolean isHomeActivityPaused;
    public CustomVideoSurfaceView videoSurfaceView;
    private boolean isDiscoverVisible = false;
    private InterstitialAd mInterstitialAd;
    private AlertDialog updateAlertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        MobileAds.initialize(this, getString(R.string.ADMOB_APP_ID));
        setContentView(R.layout.activity_home);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        intUI();
        launchInitialFrg();
        onNewIntent(getIntent());
        presenter.subscribeToFirebaseTopic();
        videoSurfaceView = CustomVideoSurfaceView.getInstance(activity,dataSource);

        loadInterstialAds();
    }

    @Override
    public void loadInterstialAds(){
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.INTERSTITIAL_AD_UNIT_ID)/*"ca-app-pub-3940256099942544/1033173712"*/);
        mInterstitialAd.setAdListener(new AdListener(){
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Log.d(TAG, "onAdFailedToLoad: "+i);
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
//                if (mInterstitialAd.isLoaded()) {
//                    mInterstitialAd.show();
//                }
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                Log.d(TAG, "onAdClosed: ");
                loadInterstialAds();
            }
        });
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }

    @Override
    public void showLoadedProfileAds() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
        else{
            loadInterstialAds();
        }
    }

    @Override
    public void showUpdateDialog(boolean isMandatory) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.UpdateAlertDialog);
        builder.setTitle(R.string.update_app_dialog_title)
                .setMessage(R.string.update_app_dialog_message)
                .setPositiveButton(R.string.update_dialog_positive_button_text, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent viewIntent =
                                new Intent(Intent.ACTION_VIEW,
                                        Uri.parse(getString(R.string.datum_play_store)));
                        startActivity(viewIntent);
                    }
                })
                .setNegativeButton(R.string.update_dialog_nagtive_button_text, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(isMandatory) {
                            dialog.dismiss();
                            finish();
                        }else{
                            dialog.dismiss();
                        }
                    }
                })
                .setCancelable(!isMandatory);
        updateAlertDialog = builder.create();
        updateAlertDialog.show();
    }

    @Override
    public void dismissUpdateDialog() {
        if(updateAlertDialog != null)
            updateAlertDialog.dismiss();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.checkForForceUpdate();
        AppController.getInstance().logIndexDocIDTesting();

        isHomeActivityPaused = false;
        if(userList.isEmpty()){
            if(videoSurfaceView != null)
                videoSurfaceView.stopPlayer();
        }
        else {
            if(isDiscoverVisible && discoveryFrg.getPagePosition() == 0) {  //card visible.
                if(videoSurfaceView != null)
                    videoSurfaceView.playPlayer();
            }
        }
        presenter.checkOnlyForProfileBoost();
    }



    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        presenter.parseIntent(intent);
    }


    @Override
    protected void onPause()
    {
        isHomeActivityPaused = true;
        if(userList.isEmpty()){
            if(videoSurfaceView != null)
                videoSurfaceView.stopPlayer();
        }
        else {
            if(videoSurfaceView != null)
                videoSurfaceView.pausePlayer();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if(videoSurfaceView != null)
            videoSurfaceView.onActivityStop();
        presenter.dropView();
        unbinder.unbind();
    }

    /*
     * Init xml content*/
    private void intUI()
    {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }
        //BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId())
            {
                case R.id.discover:
                    isDiscoverVisible = true;
                    presenter.checkOnlyForProfileBoost();
                    AppController.getInstance().updatePresence(1,false);
                    Bundle data=new Bundle();
                    displayLoadedFrg(0,data);
                    return true;
                case R.id.Prospects:
                    isDiscoverVisible = false;
                    AppController.getInstance().updatePresence(1,false);
                    if(dataSource.getSubscription()==null)
                    {
                        if(presenter!=null)
                            presenter.openBoostDialog();
                        return false;
                    }
                    Bundle prospects_data=new Bundle();
                    displayLoadedFrg(1,prospects_data);
                    return true;

                case R.id.Chats:
                    isDiscoverVisible = false;
                    AppController.getInstance().updatePresence(1,false);
                    Bundle chats_data=new Bundle();
                    displayLoadedFrg(3,chats_data);
                    return true;
                case R.id.settings:
                    isDiscoverVisible = false;
                    AppController.getInstance().updatePresence(1,false);
                    Bundle settings=new Bundle();
                    displayLoadedFrg(4,settings);
                    return true;
            }
            return false;
        });
        addBadgeIcon();
    }

    private void addBadgeIcon() {
        BottomNavigationMenuView bottomNavigationMenuView =
                (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
        View v = bottomNavigationMenuView.getChildAt(3);
        BottomNavigationItemView itemView = (BottomNavigationItemView) v;
        View badge = LayoutInflater.from(this)
                .inflate(R.layout.badge_chat_layout, bottomNavigationMenuView, false);
        tvChatCount = badge.findViewById(R.id.tv_chat_count);
        itemView.addView(badge);
    }

    //from chat frag
    @Override
    public void openProspectScreen() {
        bottomNavigationView.setSelectedItemId(R.id.Prospects);
    }

    @Override
    public void showUnreadChatCount(String unreadChatCount, boolean empty) {
        if(empty){
            tvChatCount.setVisibility(View.GONE);
        }
        else{
            tvChatCount.setText(unreadChatCount);
            tvChatCount.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void selectTab(int pos) {
        switch (pos){
            case 0: //discovery
                bottomNavigationView.setSelectedItemId(R.id.discover);
                break;
            case 1: //prospect
                bottomNavigationView.setSelectedItemId(R.id.Prospects);
                break;

            case 3: //chat
                bottomNavigationView.setSelectedItemId(R.id.Chats);
                break;
            case 4://settings
                bottomNavigationView.setSelectedItemId(R.id.settings);
        }
    }

    /*
     * called from Grid and List fragment.
     */
    @Override
    public void stopPlayer() {
        if(videoSurfaceView != null)
            videoSurfaceView.stopPlayer();
    }

    /*
     * Launching the fragment on the opening of the activity.*/
    private void launchInitialFrg()
    {
        Bundle data=new Bundle();
        displayLoadedFrg(0,data);
    }

    /*
     * Displaying the already loaded fragment.*/
    private void displayLoadedFrg(int number,Bundle data)
    {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        switch (number)
        {
            case 0:
                if(userList.isEmpty()){
                    if(videoSurfaceView != null)
                        videoSurfaceView.stopPlayer();
                }
                else {
                    if(discoveryFrg.getPagePosition() == 0) {  //card visible.
                        if(videoSurfaceView != null)
                            videoSurfaceView.playPlayer();
                    }
                }

                if(prospectsFrg.isAdded())
                {
                    ft.hide(prospectsFrg);
                }
                if(chatsFragment.isAdded()) {
                    ft.hide(chatsFragment);
                }

                if(appSettingFrg.isAdded())
                {
                    ft.hide(appSettingFrg);
                }
                if (discoveryFrg.isAdded())
                {
                    ft.show(discoveryFrg);
                    ft.commitAllowingStateLoss();
                } else
                {
                    discoveryFrg.setArguments(data);
                    presenter.openFragmentPage(ft,discoveryFrg);
                }
                break;
            case 1:
                if(videoSurfaceView != null)
                    videoSurfaceView.pausePlayer();
                if (discoveryFrg.isAdded())
                {
                    ft.hide(discoveryFrg);
                }
                if(chatsFragment.isAdded()) {
                    ft.hide(chatsFragment);
                }
                if(appSettingFrg.isAdded())
                {
                    ft.hide(appSettingFrg);
                }

                if (prospectsFrg.isAdded())
                {
                    ft.show(prospectsFrg);
                    ft.commitAllowingStateLoss();
                } else {
                    prospectsFrg.setArguments(data);
                    presenter.openFragmentPage(ft,prospectsFrg);
                }
                break;

            case 3:
                if(videoSurfaceView != null)
                    videoSurfaceView.pausePlayer();
                if (discoveryFrg.isAdded())
                {
                    ft.hide(discoveryFrg);
                }
                if (prospectsFrg.isAdded()) {
                    ft.hide(prospectsFrg);
                }
                if(appSettingFrg.isAdded())
                {
                    ft.hide(appSettingFrg);
                }
                if (chatsFragment.isAdded())
                {
                    ft.show(chatsFragment);
                    ft.commitAllowingStateLoss();
                } else {
                    chatsFragment.setArguments(data);
                    presenter.openFragmentPage(ft,chatsFragment);
                }
                break;
            case 4:
                if(videoSurfaceView != null)
                    videoSurfaceView.pausePlayer();
                if (discoveryFrg.isAdded())
                {
                    ft.hide(discoveryFrg);
                }
                if (prospectsFrg.isAdded()) {
                    ft.hide(prospectsFrg);
                }

                if(chatsFragment.isAdded())
                {
                    ft.hide(chatsFragment);
                }

                if (appSettingFrg.isAdded())
                {
                    ft.show(appSettingFrg);
                    ft.commitAllowingStateLoss();
                } else
                {
                    appSettingFrg.setArguments(data);
                    presenter.openFragmentPage(ft,appSettingFrg);
                }
                break;
        }
    }

    @Override
    public void moveFragment(FragmentTransaction fragmentTransaction,DaggerFragment fragment)
    {
        String FRAGMENT_TAG = HomeUtil.SETTINGS_FRAG_TAG;
        if(fragment instanceof DiscoveryFrag){
            FRAGMENT_TAG = HomeUtil.DISCOVER_FRAG_TAG;
        }
        else if(fragment instanceof ProspectsFrg){

            FRAGMENT_TAG = HomeUtil.PROSPECT_FRAG_TAG;
        }

        else if(fragment instanceof ChatsFragment){
            FRAGMENT_TAG = HomeUtil.CHAT_FRAG_TAG;
        }
        fragmentTransaction.add(R.id.home_frg_container,fragment,FRAGMENT_TAG);
        fragmentTransaction.commitAllowingStateLoss();
    }


    @Override
    public void showMessage(String message)
    {
        Snackbar snackbar = Snackbar
                .make(fragContainer,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showError(String error)
    {
        Snackbar snackbar = Snackbar
                .make(fragContainer,""+error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }


    @Override
    public void verify_FragmentLoad(Fragment fragment)
    {
        if (fragment instanceof DiscoveryFrag)
        {
            displayLoadedFrg(0,null);
            bottomNavigationView.setSelectedItemId(R.id.discover);
        } else if (fragment instanceof ProspectsFrg)
        {
            displayLoadedFrg(1,null);
            bottomNavigationView.setSelectedItemId(R.id.Prospects);

        } else if (fragment instanceof ChatsFragment)
        {
            displayLoadedFrg(3,null);
            bottomNavigationView.setSelectedItemId(R.id.Chats);
        }else if (fragment instanceof AppSettingFrag)
        {
            displayLoadedFrg(4,null);
            bottomNavigationView.setSelectedItemId(R.id.settings);
        } else
        {
            this.finish();
        }
    }

    @Override
    public void updateInterNetStatus(boolean status)
    {
        if(noInterNetView!=null)
        {
            if(status)
            {
                noInterNetView.internetFound();
            }else
            {
                noInterNetView.showNoInternet();
            }
        }
    }

    @Override
    public void openProfile(Intent intent) {
        activity.startActivityForResult(intent,AppConfig.PROFILE_REQUEST);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }

    @Override
    public void launchCampaignScreen(Intent intent1) {
        startActivity(intent1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(!InnAppSdk.getVendor().onActivity_result(requestCode,resultCode,data))
        {
            if(!presenter.onHandelActivityResult(requestCode,resultCode,data))
            {

                if(AppConfig.SEARCH_REQUEST==requestCode&&resultCode==Activity.RESULT_OK)
                {
                    displayLoadedFrg(0,data.getExtras());
                    bottomNavigationView.setSelectedItemId(R.id.discover);
                    discoveryFrg.onActivityResult(requestCode,resultCode,data);
                }else if(requestCode== Location_service.REQUEST_CHECK_SETTINGS)
                {
                    discoveryFrg.onActivityResult(requestCode,resultCode,data);
                }
                else
                {
                    super.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }



    @Override
    public void showBoostViewCounter(boolean show) {
        if(discoveryFrg != null)
            discoveryFrg.showBoostViewCounter(show);
    }

    @Override
    public void startCoinAnimation() {
        if(discoveryFrg != null)
            discoveryFrg.startCoinAnimation();
    }


    //from match dialog
    public void setNeedToUpdateChat(boolean needToUpdateChat) {
        presenter.setNeedToUpdateChat(needToUpdateChat);
    }
}