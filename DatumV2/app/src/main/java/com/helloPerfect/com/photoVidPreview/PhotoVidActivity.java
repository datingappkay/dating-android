package com.helloPerfect.com.photoVidPreview;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.transition.Explode;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

import com.helloPerfect.com.R;
import com.helloPerfect.com.util.DraweeTransform;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * <h>PhotoVidActivity class</h>
 * @author 3Embed.
 * @since 12/5/18.
 * @version 1.0.
 */

public class PhotoVidActivity extends AppCompatActivity {

    @BindView(R.id.video_view)
    VideoView videoView;

    @BindView(R.id.my_image_view)
    SimpleDraweeView imageView;
    private Unbinder unbinder;
    private MediaController mediaController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setSharedElementEnterTransition(DraweeTransform.createTransitionSet());
            }
        }
//        Drawable drawable = new ColorDrawable(getResources().getColor(R.color.black));
//        getWindow().setBackgroundDrawable(drawable);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            // inside your activity (if you did not enable transitions in your theme)
//            getWindow().requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
//            // set an enter transition
//            getWindow().setEnterTransition(new Explode());
//        }

        setContentView(R.layout.actvity_photo_vid_preview);
        unbinder = ButterKnife.bind(this);
        initData(getIntent());
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }catch (Exception e){}
    }

    @OnClick(R.id.btn_back)
    public void onBack(){
        videoView.stopPlayback();
        supportFinishAfterTransition();
        onBackPressed();
    }

    private void initData(Intent intent) {
        String mediaUrl = intent.getStringExtra("media_url");
        if(mediaUrl.contains(".mp4")){
            String videoThumb = intent.getStringExtra("media_thumb");
            imageView.setImageURI(Uri.parse(videoThumb));
            mediaController = new MediaController(this);
            videoView.setMediaController(mediaController);
            videoView.setVideoURI(Uri.parse(mediaUrl));
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    imageView.setImageURI(Uri.EMPTY);
                    imageView.setVisibility(View.GONE);
                }
            });
            videoView.start();
        }
        else if(!mediaUrl.isEmpty()){
            imageView.setImageURI(Uri.parse(mediaUrl));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // set an exit transition
            getWindow().setExitTransition(new Explode());
        }
        ActivityCompat.finishAfterTransition(this);
    }
}
