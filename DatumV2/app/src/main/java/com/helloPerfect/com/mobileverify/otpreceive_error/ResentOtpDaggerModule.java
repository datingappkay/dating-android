package com.helloPerfect.com.mobileverify.otpreceive_error;
import com.helloPerfect.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;

@Module
public abstract class ResentOtpDaggerModule
{
    @FragmentScoped
    @Binds
    abstract ResentOtpFragment resentOtpFragment(ResentOtpFragment resentOtp);

    @FragmentScoped
    @Binds
    abstract ResentOtpContract.Presenter resendOtpPresenter(ResendOtpPresenter presenter);
}
