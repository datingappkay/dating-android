package com.helloPerfect.com.util.CustomView;

/**
 * @since  3/20/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface SquizeClicked
{
    void onClicked();
}
