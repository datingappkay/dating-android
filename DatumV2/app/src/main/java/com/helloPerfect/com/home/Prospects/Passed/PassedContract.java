package com.helloPerfect.com.home.Prospects.Passed;
import com.helloPerfect.com.BasePresenter;
import com.helloPerfect.com.BaseView;
import com.helloPerfect.com.home.Prospects.OnAdapterItemClicked;

/**
 * @since  3/23/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface PassedContract
{
    interface View extends BaseView
    {
        void onDataUpdate();
        void showProgress();
        void onApiError(String message);
        void emptyData();
        void openUserProfile(String details);
        void setAdapterListener(OnAdapterItemClicked callback);
    }


    interface Presenter extends BasePresenter<View>
    {
        void initAdapterListener();
        void initMatchListener();
        void  getListData(boolean isLoadMore);
        void pee_fetchProfile(int position);
        boolean checkLoadMore(int position);
    }
}
