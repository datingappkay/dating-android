package com.helloPerfect.com.home.Discover;

import com.helloPerfect.com.home.Discover.Model.UserAssetData;

public interface UserActionEventError
{
    void onRevertAction(UserAssetData userAssetData);

    void onError(UserAssetData userAssetData);
}
