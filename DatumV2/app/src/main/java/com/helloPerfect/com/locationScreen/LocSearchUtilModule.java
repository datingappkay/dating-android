package com.helloPerfect.com.locationScreen;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.helloPerfect.com.dagger.ActivityScoped;
import com.helloPerfect.com.data.model.fourSq.Venue;
import com.helloPerfect.com.locationScreen.model.AddressAdapter;
import com.helloPerfect.com.util.App_permission;
import com.helloPerfect.com.util.LocationProvider.Location_service;
import com.helloPerfect.com.util.TypeFaceManager;

import java.util.ArrayList;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * <h>GoogleLocSearchUtilModule class</h>
 * @author 3Embed.
 * @since 3/4/18.
 * @version 1.0.
 */

@Module
public class LocSearchUtilModule {

    @ActivityScoped
    @Provides
    App_permission provideAppPermission(Activity activity, TypeFaceManager typeFaceManager){
        return new App_permission(activity,typeFaceManager);
    }

    @Named("ADDRESS_LIST")
    @ActivityScoped
    @Provides
    ArrayList<Venue> provideListOfAddress(){
        return  new ArrayList<>();
    }

    @ActivityScoped
    @Provides
    AddressAdapter provideAddressAdapter(@Named("ADDRESS_LIST")ArrayList<Venue> arrayList, TypeFaceManager typeFaceManager){
        return new AddressAdapter(arrayList,typeFaceManager);
    }

    @ActivityScoped
    @Provides
    LinearLayoutManager provideLinearLayoutManager(Activity activity){
        return new LinearLayoutManager(activity);
    }

    @ActivityScoped
    @Provides
    Location_service getLocation_service(Activity activity)
    {
        return new Location_service(activity);
    }

}
