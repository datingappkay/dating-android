package com.helloPerfect.com.userProfile.VideoItem;

import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.helloPerfect.com.R;
import com.helloPerfect.com.userProfile.UserProfilePage;
import com.helloPerfect.com.util.AppConfig;
import com.helloPerfect.com.util.CustomVideoView.CustomVideoSurfaceView;
import com.helloPerfect.com.util.Utility;
import com.facebook.drawee.view.SimpleDraweeView;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
/**
 * A simple {@link Fragment} subclass.
 */
public class VideoItemFrg extends DaggerFragment implements VideoItemContract.View
{
    private Unbinder unbinder;
    private String THUMB_NAIL,VIDEO_URL;
    @Inject
    Utility utility;
    @BindView(R.id.video_parent_view)
    FrameLayout video_parent_view;
    @BindView(R.id.thumbnail)
    SimpleDraweeView thumbnail;
    private CustomVideoSurfaceView videoSurfaceView;
    @BindView(R.id.video_play_icon)
    ImageView play_icon;
    @BindView(R.id.mute_btn)
    ImageView muteButton;
    @BindView(R.id.video_container)
    FrameLayout video_container;


    @Inject
    public VideoItemFrg() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        UserProfilePage userProfilePage= (UserProfilePage) getActivity();
        assert userProfilePage != null;
        videoSurfaceView=userProfilePage.video_player;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_videoitem_frg, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder= ButterKnife.bind(this,view);
        initData();
    }

    private void initData()
    {
        thumbnail.setImageURI(THUMB_NAIL);
        play_icon.setOnClickListener(v -> {
            play_icon.setVisibility(View.GONE);
            create_Media_player(video_container,VIDEO_URL);
        });
        muteButton.setOnClickListener(v -> {
            if(videoSurfaceView != null)
                videoSurfaceView.setMute(!videoSurfaceView.isMuted);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
//        if(UserProfilePage.pager_position == 0) {
//            play_icon.setVisibility(View.GONE);
//            create_Media_player(video_container, VIDEO_URL);
//        }
    }

    /*
         *Setting the video url. */
    public void setMediaFile(String thumbnail,String video_url)
    {

        this.THUMB_NAIL=thumbnail;
        this.VIDEO_URL=video_url;
    }


    @Override
    public void onDestroy()
    {
        unbinder.unbind();
        super.onDestroy();
    }

    /*
     * Creating the media player to play for the given url.*/
    private void create_Media_player(final FrameLayout holder, final String path)
    {
        try
        {
            onPausePlayer();
            if (holder == null)
            {
                return;
            }
            removeVideoView(videoSurfaceView);
            holder.addView(videoSurfaceView);
            videoSurfaceView.startPlayer(Uri.parse(create_Handel_video(path)),play_icon,muteButton,holder.getWidth(),holder.getHeight());
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * prepare for video play
     */
    private void removeVideoView(CustomVideoSurfaceView videoView)
    {
        ViewGroup parent = (ViewGroup) videoView.getParent();
        if (parent == null)
        {
            return;
        }
        int index = parent.indexOfChild(videoView);
        if (index >= 0) {
            parent.removeViewAt(index);
        }
    }

    /*
     * pause media player externally.*/
    public void onPausePlayer()
    {
        if (videoSurfaceView != null) {
            removeVideoView(videoSurfaceView);
            videoSurfaceView.stopPlayer();
        }
    }


    /*
     * Handling the video quality.*/
    private static String create_Handel_video(String video_url)
    {
        video_url=change_video_foramte(video_url);
        if(video_url.contains(AppConfig.CloudinaryDetails.VIDEO_QUALITY))
        {
            return video_url;
        }
        String key_word="upload";
        int length_key=key_word.length();
        int index=video_url.indexOf("upload");
        if(index>0)
        {
            String firs_sub_String=video_url.substring(0,index+length_key);
            String last_sub_String=video_url.substring(index+length_key);
            return firs_sub_String+AppConfig.CloudinaryDetails.VIDEO_QUALITY +last_sub_String;

        }else
        {
            return video_url;
        }
    }
    /*
     * changing the video format.*/
    private static String change_video_foramte(String video_url)
    {
        if(video_url.contains(AppConfig.CloudinaryDetails.VIDEO_FORMATE))
        {
            return video_url;
        }else
        {
            int index_dot=video_url.lastIndexOf(".");
            String front_part=video_url.substring(0,index_dot+1);
            return front_part+AppConfig.CloudinaryDetails.VIDEO_FORMATE;
        }
    }
}
