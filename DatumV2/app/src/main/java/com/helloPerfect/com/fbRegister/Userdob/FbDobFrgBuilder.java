package com.helloPerfect.com.fbRegister.Userdob;
import com.helloPerfect.com.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 * @since  2/16/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface FbDobFrgBuilder
{
    @FragmentScoped
    @Binds
    FbDobFragment getEmailFragment(FbDobFragment dobFragment);

    @FragmentScoped
    @Binds
    FbDobContract.Presenter taskPresenter(FbDobFrgPresenter presenter);
}
