package com.helloPerfect.com.data.model;

/**
 * <h2>ItemActionCallBack</h2>
 * <P>
 *     User item call back for the details.
 * </P>
 * @since  3/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface ItemActionCallBack
{
    void onClick(int id, int position);
}

