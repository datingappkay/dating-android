package com.suresh.innapp_purches;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import java.util.ArrayList;
import java.util.List;
/**
 * <h1>InAppManager</h1>
 * <P>
 *     Purchase inApp item to be purchased.
 * </P>
 *@since  19/12/16.
 */
class InAppManager implements BillingProcessor.IBillingHandler,InnAppVendor
{
    private Activity mactivity;
    private static BillingProcessor bp;
    private ArrayList<SkuDetails> ITEM_LIST;
    private ProgressDialog progressDialog;
    private String pay_load="default";
    private boolean isFor_Purchase;
    private final String base64EncodedPublicKey;
    private InAppCallBack.OnPurchased onPurchased;
    private InAppCallBack.onConsume onConsume;
    private ItemDetails itemDetails;
    private DataHolder dataholder;

    InAppManager(Context context,String key)
    {
        base64EncodedPublicKey=key;
        isFor_Purchase=false;
        bp=new BillingProcessor(context,base64EncodedPublicKey,this);
        ITEM_LIST =new ArrayList<>();
    }


    @Override
    public void onProductPurchased(String productId,TransactionDetails details)
    {
        if(itemDetails.type== InnAppSdk.Type.INNAPP)
        {
            if(bp.consumePurchase(productId))
            {
                if(onPurchased!=null)
                    onPurchased.onSuccess(productId);
                bp.release();
            }else
            {
                if(onPurchased!=null)
                    onPurchased.onError(productId,"Unable to purchase item!");
            }
        }else
        {
            if(onPurchased!=null)
                onPurchased.onSuccess(productId);
        }
        itemDetails=null;
    }


    @Override
    public void onPurchaseHistoryRestored()
    {
        bp.release();
    }

    @Override
    public void onBillingError(int errorCode, Throwable error)
    {
        bp.release();
        if(onConsume!=null)
        {
            onConsume.onError(""+error,"Error on Consume");
        }
        if(itemDetails!=null)
        {
            if(onPurchased!=null)
            {
                String message="Unable to purchase item!";
                if(errorCode==1)
                {
                    message="User canceled!";
                }else if(errorCode==2)
                {
                    message="Product id not matched!";
                }else if(errorCode==7)
                {
                    message="Already owened this product!";
                }
                onPurchased.onError(itemDetails.product_id,message);

            }

        }
        onConsume=null;
    }

    @Override
    public void onBillingInitialized()
    {
        if(progressDialog!=null)
        {
            progressDialog.dismiss();
        }
        if(isFor_Purchase&&itemDetails!=null)
        {
            isFor_Purchase=false;
            if(itemDetails.type== InnAppSdk.Type.INNAPP)
            {
                bp.purchase(mactivity,itemDetails.product_id,pay_load);
            }else
            {
                bp.subscribe(mactivity,itemDetails.product_id,pay_load);
            }
        }else if(dataholder !=null)
        {
            new DetailsCollector().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, dataholder);
        }
    }


    @Override
    public String getTesting_id()
    {
        return testing_id;
    }

    @Override
    public SkuDetails getProductDetails(String productId,InnAppSdk.Type type)
    {
        if(type== InnAppSdk.Type.INNAPP)
        {
            return bp.getPurchaseListingDetails(productId);
        }else
        {
            return bp.getSubscriptionListingDetails(productId);
        }
    }

    @Override
    public void getProductDetails(Activity activity,List<String> prductIds, InnAppSdk.Type type,InAppCallBack.DetailsCallback callback)
    {
        dataholder=new DataHolder();
        dataholder.ids=prductIds;
        dataholder.type=type;
        dataholder.callback=callback;
        isFor_Purchase=false;
        bp=new BillingProcessor(activity,base64EncodedPublicKey,this);
    }

    @Override
    public void purchasedItem(Activity activity, String productId,InnAppSdk.Type type,String pay_load ,InAppCallBack.OnPurchased onPurchased)
    {

        if(type==InnAppSdk.Type.INNAPP)
        {
            this.itemDetails=new ItemDetails(productId,pay_load,type);
            buyItem(activity,productId,pay_load,onPurchased);
        }else
        {
            this.itemDetails=new ItemDetails(productId,pay_load,type);
            subscribeItem(activity,productId,pay_load,onPurchased);
        }
    }

    @Override
    public void consumeProduct(Activity activity, String productId, InAppCallBack.onConsume onConsume)
    {
        this.onConsume=onConsume;
        if(bp!=null)
        {
           if(bp.consumePurchase(productId))
           {
               if(onConsume!=null)
                   onConsume.onSuccess();
               this.onConsume=null;
           }else
           {
               if(onConsume!=null)
                   onConsume.onError(productId,"Initialize first");
               this.onConsume=null;
           }
        }else
        {
            if(onConsume!=null)
             onConsume.onError(productId,"Initialize first");
            this.onConsume=null;
        }
    }


    @Override
    public List<String> getOwenProduct()
    {
        return  bp.listOwnedProducts();
    }


    @Override
    public List<String> getOwenSubscription()
    {
        return  bp.listOwnedSubscriptions();
    }


    @Override
    public boolean onActivity_result(int requestCode, int resultCode, Intent data)
    {
        return bp != null && bp.handleActivityResult(requestCode, resultCode, data);
    }

    private void buyItem(Activity activity,String productId,String pay_load_data,InAppCallBack.OnPurchased onPurchased)
    {
        this.mactivity=activity;
        this.pay_load =pay_load_data;
        this.onPurchased=onPurchased;
        try
        {
            progressDialog=new ProgressDialog(activity);
            progressDialog.setMessage("Wait..");
            progressDialog.show();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        isFor_Purchase=true;
        bp=new BillingProcessor(activity,base64EncodedPublicKey,this);
    }


    private void subscribeItem(Activity activity,String productId,String pay_load_data,InAppCallBack.OnPurchased onPurchased)
    {
        this.mactivity=activity;
        this.pay_load =pay_load_data;
        this.onPurchased=onPurchased;
        try
        {
            progressDialog=new ProgressDialog(activity);
            progressDialog.setMessage("Wait..");
            progressDialog.show();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        isFor_Purchase=true;
        bp=new BillingProcessor(activity,base64EncodedPublicKey,this);
    }



    @SuppressLint("StaticFieldLeak")
    private class DetailsCollector extends AsyncTask<DataHolder,Void,ResultlHolder>
    {
        InAppCallBack.DetailsCallback callBack;
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            callBack=null;
        }


        @Override
        protected ResultlHolder doInBackground(DataHolder... params)
        {
            DataHolder holder=params[0];
            List<String> ids=holder.ids;
            InnAppSdk.Type type=holder.type;
            callBack=holder.callback;
            List<String> errorList=new ArrayList<>();
            List<SkuDetails> result=new ArrayList<>();
            for(String id:ids)
            {
                SkuDetails details;
                details=getDetailsFromLocal(id);
                if(details==null)
                {
                    if(type== InnAppSdk.Type.INNAPP)
                    {
                        details=bp.getPurchaseListingDetails(id);
                    }else
                    {
                        details=bp.getSubscriptionListingDetails(id);
                    }
                    if(details!=null)
                        ITEM_LIST.add(details);
                }
                if(details==null)
                {
                    errorList.add(id);
                }else
                {
                    result.add(details);
                }
            }
            return new ResultlHolder(result,errorList);
        }


        @Override
        protected void onPostExecute(ResultlHolder holder)
        {
            super.onPostExecute(holder);
            if(callBack!=null)
            {
                callBack.onSuccess(holder.result,holder.errorList);
            }
            dataholder=null;
            bp.release();
        }
    }

    class DataHolder
    {
        List<String> ids;
        InnAppSdk.Type type;
        InAppCallBack.DetailsCallback callback;
    }


    class ResultlHolder
    {
        List<String> errorList;
        List<SkuDetails> result;
        ResultlHolder(List<SkuDetails> result,List<String> errorList)
        {
            this.result=result;
            this.errorList=errorList;
        }
    }

    private SkuDetails getDetailsFromLocal(String id)
    {
        for(SkuDetails details:ITEM_LIST)
        {
            if(details.getProductId().equals(id))
            {
                return details;
            }
        }
        return null;
    }
}
