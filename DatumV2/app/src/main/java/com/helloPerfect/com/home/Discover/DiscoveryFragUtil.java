package com.helloPerfect.com.home.Discover;

import com.helloPerfect.com.dagger.ActivityScoped;
import com.helloPerfect.com.home.Discover.Model.UserItemPojo;
import com.helloPerfect.com.util.GridSpacingItemDecoration;
import com.helloPerfect.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * <h2>DiscoveryFragUtil</h2>
 * <P>
 *     Dagger builder interface for the Discovery page .
 * </P>
 * @since  2/27/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public class DiscoveryFragUtil
{
    public static final String DISCOVERY_FRAGMENT_MANAGER = "discovery_fragment_manager";
    public static final String USER_LIST="user_list";
    public static final String DISCOVERY_FRAG_LIST = "discovery_frag_list";

    @Provides
    @ActivityScoped
    GridSpacingItemDecoration provideGridSpacingItemDecoration(Utility utility)
    {
        return  new GridSpacingItemDecoration(2,utility.dpToPx(10), true);
    }

    @Named(USER_LIST)
    @Provides
    @ActivityScoped
    ArrayList<UserItemPojo> provideUserList()
    {
        return new ArrayList<>();
    }

}
