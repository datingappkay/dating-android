package com.helloPerfect.com.editProfile;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;

import com.helloPerfect.com.R;
import com.helloPerfect.com.data.source.PreferenceTaskDataSource;
import com.helloPerfect.com.editProfile.Model.EditProfileModel;
import com.helloPerfect.com.editProfile.Model.PicAdapterClickCallback;
import com.helloPerfect.com.editProfile.Model.ProfilePicture;
import com.helloPerfect.com.editProfile.ProfileAlert.ProfileAlertDialog;
import com.helloPerfect.com.networking.NetworkStateHolder;
import com.helloPerfect.com.photoVidPreview.PhotoVidActivity;
import com.helloPerfect.com.util.App_permission;
import com.helloPerfect.com.util.CloudManager.UploadManager;
import com.helloPerfect.com.util.CloudManager.UploaderCallback;
import com.helloPerfect.com.util.FileUtil.AppFileManger;
import com.helloPerfect.com.util.ImageChecker.ImageProcessor;
import com.helloPerfect.com.util.MediaBottomSelector;
import com.helloPerfect.com.util.Utility;
import com.videocompressor.com.CompressImage;
import com.videocompressor.com.DataModel.CompressedData;
import com.videocompressor.com.RxCompressObservable;
import com.videocompressor.com.VideoCompressor;
import java.io.File;
import java.util.ArrayList;
import javax.inject.Inject;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * <h>EditPrefPresenter class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

public class EditProfilePresenter implements EditProfileContract.Presenter, App_permission.Permission_Callback,MediaBottomSelector.Callback,PicAdapterClickCallback {

    private  final String GALLERY="gallery";
    private  final String CAMERA="camera";

    @Inject
    EditProfileContract.View view;
    @Inject
    UploadManager uploadManager;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    MediaBottomSelector mediaBottomSelector;
    @Inject
    App_permission app_permission;
    @Inject
    AppFileManger appFileManger;
    @Inject
    Utility utility;
    @Inject
    Activity activity;
    @Inject
    EditProfileModel model;
    @Inject
    ImageProcessor imageProcessor;
    @Inject
    VideoCompressor videoCompressor;
    @Inject
    CompressImage compressImage;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    ProfileAlertDialog profileAlertDialog;

    private File temp_file=null;
    private File currentVideo=null;
    private File currentThumb=null;
    private String  currentVideoUrl=null;
    private String currentThumbUrl=null;
    private int currentVideoHeight = 360;
    private int currentVideoWidth = 360;
    //0 means VIDEO
    private int mediaType = 0;
    private CompositeDisposable compositeDisposable;
    private int curAdapterPosition = -1;

    @Inject
    public EditProfilePresenter(){
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void init() {
        if(view != null) {
            view.applyFont();
            view.makeVideoThumbSquare();
            view.recyclerViewSetup();
        }
    }

    @Override
    public void openChooser()
    {
        temp_file=null;
        mediaBottomSelector.showBottomSheet(this);
    }

    @Override
    public void upDateToGallery()
    {
        if(temp_file==null)
            return;
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File file = new File(temp_file.getPath());
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        activity.sendBroadcast(mediaScanIntent);
    }

    @Override
    public boolean isValidMediaSize() {
        boolean valid = false;
        if(mediaType == 1)
            return true;
        if(temp_file != null){
            valid = utility.isValidVideoSize(temp_file.length());
        }
        if(!valid){
            if(view != null)
                view.showError(activity.getString(R.string.profile_size_limit_msg));
            model.deleteFile(temp_file);
        }
        return valid;
    }

    @Override
    public boolean isValidMediaSize(String filePath) {
        boolean valid = false;
        if(mediaType == 1)
            return true;
        try {
            File tempFile = new File(filePath);
            valid = utility.isValidVideoSize(tempFile.length());
            if (!valid) {
                if (view != null)
                    view.showError(activity.getString(R.string.profile_size_limit_msg));
                model.deleteFile(tempFile);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return valid;
    }

    @Override
    public String getRecentTempVideo()
    {
        return temp_file.getPath();
    }

    @Override
    public void loadUserVideo() {
        String videoUrl = dataSource.getUserVideoUrl();
        String videoThumb = dataSource.getUserVideoThumbnail();
        model.keepOldVideo(videoUrl, videoThumb);
        if (videoThumb != null && !videoThumb.isEmpty()) {
            //compress set the thumb
            currentVideoUrl = videoUrl;
            currentThumbUrl = videoThumb;
            if (view != null) {
                view.showAddVideoButton(false);
                view.showVideoThumb(videoThumb);
            }
        } else {
            if (view != null)
                view.showAddVideoButton(true);
        }
    }

    @Override
    public void onCamera() {
        ArrayList<App_permission.Permission> permissions =new ArrayList<>();
        permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.CAMERA);
        app_permission.getPermission(CAMERA,permissions,this);
    }

    @Override
    public void onGallery() {
        ArrayList<App_permission.Permission> permissions =new ArrayList<>();
        permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
        app_permission.getPermission(GALLERY,permissions,this);
    }

    @Override
    public void compressedMedia(String file_path, ImageView imageView) {
        if(networkStateHolder.isConnected()) {
            if (mediaType == 0)
                compressedVideo(file_path, imageView);
            else
                compressImage(file_path);
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void loadUserPicture() {
        String profilePic  = dataSource.getProfilePicture();
        ArrayList<String> othersPic = dataSource.getUserOtherImages();
        model.parsePictures(profilePic,othersPic);
    }

    @Override
    public void launchVideoChooser() {
        mediaType = 0; //video
        openChooser();
    }


    @Override
    public void compressImage(String filePath)
    {
        model.addToAdapter(ProfilePicture.PROFILE_PIC_LOADING,"",curAdapterPosition);
        Observer<CompressedData> observer = new Observer<CompressedData>()
        {
            @Override
            public void onSubscribe(Disposable d)
            {
                compositeDisposable.add(d);
            }

            @Override
            public void onNext(CompressedData value)
            {

                if(value != null){
                    if(view!=null) {
                        model.addMediaToAdapter(value.getPath(), curAdapterPosition);
                        uploadToAws(value.getPath());
                    }
                }
            }

            @Override
            public void onError(Throwable e)
            {
                e.printStackTrace();
                model.addToAdapter(ProfilePicture.PROFILE_PIC_EMPTY,"",curAdapterPosition);
                if(view!=null)
                    view.showError("Failed to collect video!");
            }

            @Override
            public void onComplete() {}
        };
        RxCompressObservable observable=compressImage.compressImage(activity,filePath);
        observable.subscribeOn(Schedulers.newThread());
        observable.observeOn(AndroidSchedulers.mainThread());
        observable.subscribe(observer);
    }

    /*
   * Uploading the image to aws.*/
    @Override
    public void uploadToAws(String filePath)
    {
        uploadManager.uploadFile(filePath, new UploaderCallback() {
            @Override
            public void onSuccess(String main_url, String thumb_nail,int height,int width)
            {
                model.addToAdapter(ProfilePicture.PROFILE_PIC,main_url,curAdapterPosition);
                if(view != null)
                    view.showTickMark(true);
            }

            @Override
            public void onError(String error)
            {
                model.deletePicWith(curAdapterPosition);
                model.addToAdapter(ProfilePicture.PROFILE_PIC_EMPTY,"",curAdapterPosition);
                if(view!=null)
                    view.showError(error);
            }
        });
    }

    @Override
    public boolean isProfileChanged() {
        return model.isProfileChanged();
    }

    @Override
    public void removeVideo() {
        currentVideoUrl = "";
        currentThumbUrl = "";
        //model.saveProfileVideo(currentVideoUrl,currentThumbUrl);
    }

    @Override
    public void saveEditedData() {
        model.saveProfilePicture();
        model.saveProfileVideo(currentVideoUrl,currentThumbUrl,currentVideoHeight,currentVideoWidth);
    }

    @Override
    public void loadVideoPreview(android.view.View v) {
        if(currentThumbUrl != null && !currentThumbUrl.isEmpty()
                && currentVideoUrl != null && !currentVideoUrl.isEmpty()){
            Intent intent = new Intent(activity,PhotoVidActivity.class);
            intent.putExtra("media_url",currentVideoUrl);
            intent.putExtra("media_thumb",currentThumbUrl);
            if(view != null){
                view.launchPreviewScreen(intent,v);
            }
        }
    }

    @Override
    public void compressedVideo(String file_path, ImageView imageView)
    {
        if(view != null) {
            view.showAddVideoButton(false);
            view.showVideoLoading(true);
        }
        Observer<CompressedData> observer = new Observer<CompressedData>()
        {
            @Override
            public void onSubscribe(Disposable d)
            {
                compositeDisposable.add(d);
            }

            @Override
            public void onNext(CompressedData value)
            {
                Log.e("video path: ", value.getPath());
                try {
                    model.deleteFile(temp_file);
                    currentVideo = new File(value.getPath());
                    currentThumb = appFileManger.getImageFile();
                    model.processThumbImage(currentVideo.getPath(), currentThumb.getPath(), imageView);
                    uploadVideo(currentVideo.getPath(),currentThumb.getPath());
                }catch (Exception e)
                {
                    e.printStackTrace();
                    if(view!=null) {
                        view.showVideoLoading(false);
                        view.showAddVideoButton(true);
                        view.showError(e.getMessage());
                    }
                }
            }
            @Override
            public void onError(Throwable e)
            {
                if(view != null) {
                    view.showError(e.getMessage());
                    view.showVideoLoading(false);
                    view.showAddVideoButton(true);
                }
                e.printStackTrace();
            }

            @Override
            public void onComplete() {}
        };
        RxCompressObservable observable=videoCompressor.compressVideo(file_path);;
        observable.subscribeOn(Schedulers.newThread());
        observable.observeOn(AndroidSchedulers.mainThread());
        observable.subscribe(observer);
    }

    @Override
    public void uploadVideo(String filePath,String thumbnail_path)
    {
        uploadManager.uploadVideoFile(filePath, new UploaderCallback() {
            @Override
            public void onSuccess(String main_url, String thumb_nail,int height,int width)
            {
                currentVideoUrl = main_url;
                currentThumbUrl = thumb_nail;
                currentVideoHeight = height;
                currentVideoWidth = width;

                if(view!=null) {
                    view.showVideoLoading(false);
                    view.showTickMark(true);
                }
            }

            @Override
            public void onError(String error) {
                if(view!=null)
                {
                    Log.w("EditProfilePresenter: ", error);
                    view.showError("Upload Error!!");
                    view.showVideoLoading(false);
                    view.showAddVideoButton(true);
                }
            }
        });
    }

    /*
     *delete the uploaded media */
    private void deleteMedia(String url_path)
    {
        //amazonS3.deleteMedia(Config.Type.IMAGE, url_path);
       // model.deletePicWith(curAdapterPosition);
    }


    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag) {
        if(isAllGranted&&tag.equals(CAMERA))
        {
            if(view!=null)
            {
                try {
                    if (mediaType == 0)
                        temp_file = appFileManger.getVideoFile();
                    else
                        temp_file = appFileManger.getImageFile();
                    view.openCamera(utility.getUri_Path(temp_file), mediaType);
                }catch (Exception e){
                    if(view != null)
                        view.showError(e.getMessage());
                }
            }
            //activity.startActivity(new Intent(activity, CameraActivity.class));
        }else if(isAllGranted&&tag.equals(GALLERY))
        {
            if(view!=null)
                view.openGallery(mediaType);
        }
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag) {
        String[] stringArray = deniedPermission.toArray(new String[0]);
        if(tag.equals(GALLERY))
        {
            app_permission.show_Alert_Permission(activity.getString(R.string.video_access_text),activity.getString(R.string.gallery_video_subtitle),
                    activity.getString(R.string.gallery_video_message),stringArray);
        }else if(tag.equals(CAMERA))
        {
            app_permission.show_Alert_Permission(activity.getString(R.string.camera_video_text),activity.getString(R.string.camera_video_subtitle),
                    activity.getString(R.string.camera_video_message),stringArray);
        }
    }

    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag) {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }

    @Override
    public void onPermissionPermanent_Denied(String tag, boolean parmanent) {
        if(parmanent)
        {
            if(tag.equals(GALLERY))
            {
                app_permission.showAlertDeniedPermission(activity.getString(R.string.video_denied_text),activity.getString(R.string.gallery_denied_video_subtitle),
                        activity.getString(R.string.gallery_denied_video_message));
            }else if(tag.equals(CAMERA))
            {
                app_permission.showAlertDeniedPermission(activity.getString(R.string.camera_denied_video_text),activity.getString(R.string.camera_denied_video_subtitle),
                        activity.getString(R.string.camera_denied_video_message));
            }
        }
    }

    @Override
    public void onPicRemove(int pos) {
        curAdapterPosition = pos;
        ProfilePicture profilePicture = model.deletePicWith(curAdapterPosition);
        //model.saveProfilePicture();
        if(profilePicture == null)
            return;
        if(view != null)
            view.showTickMark(true);
        deleteMedia(profilePicture.getProfilePic());
    }

    @Override
    public void onPicPreview(String profilePic,android.view.View v) {
        if(view != null) {
            //view.showMessage("Image Preview of position: ");
            Intent intent = new Intent(activity, PhotoVidActivity.class);
            intent.putExtra("media_url",profilePic);
            view.launchPreviewScreen(intent,v);
        }
    }

    @Override
    public void onEmptyPicClick(int pos) {
        mediaType = 1;  //picture
        curAdapterPosition = pos; //adapter position.
        openChooser();
    }

    @Override
    public void onSetAsProfilePic(int pos) {
        if(model.isProfilePicture(pos)){
            if(view != null)
                view.showMessage("This Picture is Your Profile picture!!");
            return;
        }
        if(model.makeProfilePic(pos)){
            if(view != null)
                view.showMessage("Profile picture updated Successfully!!");
            if(view != null)
                view.showTickMark(true);
        }
    }

//    @Override
//    public void onPicLongClick(int pos) {
//        profileAlertDialog.showDialog(pos,this);
//    }

//    @Override
//    public void onSetProfile(int pos) {
//        if(model.isProfilePicture(pos)){
//            if(view != null)
//                view.showMessage("This Picture is Your Profile picture!!");
//            return;
//        }
//        if(model.makeProfilePic(pos)){
//            if(view != null)
//                view.showMessage("Your Profile picture changed Successfully!!");
//            if(view != null)
//                view.showTickMark(true);
//        }
//    }

//    @Override
//    public void onCancel() {
//
//    }
}
