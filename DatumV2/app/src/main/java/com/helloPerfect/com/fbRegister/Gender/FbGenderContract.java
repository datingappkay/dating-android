package com.helloPerfect.com.fbRegister.Gender;
import com.helloPerfect.com.BasePresenter;
import com.helloPerfect.com.BaseView;

/**
 * <h2>{@link FbGenderContract}</h2>
 * @since  2/15/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface FbGenderContract
{
    interface View extends BaseView
    {
        void showMessage(String message);

        void showError(String message);

        void moverFragment(int gender);

        void upDateSelection(int position, boolean isSelected);
    }

    interface Presenter extends BasePresenter<View>
    {
        void handleSelection(int position, boolean isSelected);
    }
}
