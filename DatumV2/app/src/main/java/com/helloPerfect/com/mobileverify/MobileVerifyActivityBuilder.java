package com.helloPerfect.com.mobileverify;

import android.app.Activity;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import com.helloPerfect.com.dagger.ActivityScoped;
import com.helloPerfect.com.dagger.FragmentScoped;
import com.helloPerfect.com.mobileverify.main.PhnoDaggerModule;
import com.helloPerfect.com.mobileverify.main.PhnoFragment;
import com.helloPerfect.com.mobileverify.otp.OtpDaggerModule;
import com.helloPerfect.com.mobileverify.otp.Otp_Fragment;
import com.helloPerfect.com.mobileverify.otpreceive_error.ResentOtpFragment;
import com.helloPerfect.com.mobileverify.otpreceive_error.ResentOtpDaggerModule;
import com.helloPerfect.com.mobileverify.result.ResultDaggerModule;
import com.helloPerfect.com.mobileverify.result.ResultFragment;
import com.helloPerfect.com.util.Countrypicker.CountryPicker;
import javax.inject.Named;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
/**
 * <h1>PreferenceDaggerModule</h1>
 * <p>
 *
 * </p>
 * @author 3Embed
 * @since 13/08/17
 * @version 1.0.
 */
@Module
public abstract class MobileVerifyActivityBuilder
{

    public static final String ACTIVITY_FRAGMENT_MANAGER = "BaseActivity.FragmentManager";

    @ActivityScoped
    @Binds
    abstract Activity provideActivity(MobileVerifyActivity splashActivity);

    @ActivityScoped
    @Binds
    abstract MobileVerifyContract.View provideView(MobileVerifyActivity mobileVerifyActivity);

    @ActivityScoped
    @Binds
    abstract MobileVerifyContract.Presenter taskPresenter(MobileVerifyPresenter presenter);

    @Provides
    @Named(ACTIVITY_FRAGMENT_MANAGER)
    @ActivityScoped
    static FragmentManager activityFragmentManager(Activity activity)
    {
        return ((AppCompatActivity)activity).getSupportFragmentManager();
    }

    @FragmentScoped
    @ContributesAndroidInjector(modules = {PhnoDaggerModule.class})
    abstract PhnoFragment contributeFragmentPhno();

    @FragmentScoped
    @ContributesAndroidInjector
    abstract CountryPicker contributeFragmentCountryPicker ();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {ResultDaggerModule.class})
    abstract ResultFragment contributeResultFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {OtpDaggerModule.class})
    abstract Otp_Fragment contributeOtpFragment();


    @FragmentScoped
    @ContributesAndroidInjector(modules = {ResentOtpDaggerModule.class})
    abstract ResentOtpFragment contributeResentOtpFragment();
}