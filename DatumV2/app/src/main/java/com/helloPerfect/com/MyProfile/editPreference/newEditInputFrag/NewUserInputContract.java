package com.helloPerfect.com.MyProfile.editPreference.newEditInputFrag;
import com.helloPerfect.com.BasePresenter;
import com.helloPerfect.com.BaseView;

/**
 * @since  2/26/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface NewUserInputContract
{
    interface Presenter extends BasePresenter
    {
        void updatePreference(String pref_id, String value);
        void showError();
    }

    interface View extends BaseView
    {
        void showMessage(String message);
        void showError(String error);
        String getErrorTitle();
    }
}
