package com.helloPerfect.com.home.Chats;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.helloPerfect.com.dagger.ActivityScoped;
import com.helloPerfect.com.home.Chats.Model.ChatListAdapter;
import com.helloPerfect.com.home.Chats.Model.ChatListData;
import com.helloPerfect.com.home.Chats.Model.ChatListItem;
import com.helloPerfect.com.home.Chats.Model.MatchListAdapter;
import com.helloPerfect.com.util.ChatOptionDialog.ChatOptionDialog;
import com.helloPerfect.com.util.ReportUser.ReportUserDialog;
import com.helloPerfect.com.util.TypeFaceManager;
import com.helloPerfect.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
/**
 * @since  4/27/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public class ChatFragUtil
{
    public static final String HORIZONTAL_LIST="horizontal_list";
    public static final String VERTICAL_LIST="vertical_list";

    @Provides
    @ActivityScoped
    ArrayList<ChatListData> getMatchListItemPojo()
    {
        ArrayList<ChatListData> arrayList = new ArrayList<>();
        ChatListData firstBoostItem = new ChatListData();
        firstBoostItem.setForBoost(true);
        arrayList.add(firstBoostItem);
        return arrayList;
    }

    @Provides
    @ActivityScoped
    ArrayList<ChatListItem> getChatListItemPojo()
    {
        return new ArrayList<>();
    }

    @Provides
    @ActivityScoped
    MatchListAdapter getMatchListAdapter(Activity activity,ArrayList<ChatListData> list, TypeFaceManager typeFaceManager)
    {
        return new MatchListAdapter(activity,list,typeFaceManager);
    }

    @Provides
    @ActivityScoped
    ChatListAdapter getChatListAdapter(Activity activity,ArrayList<ChatListItem> chat_item,TypeFaceManager typeFaceManager)
    {
        return new ChatListAdapter(activity,chat_item,typeFaceManager);
    }

    @Named(HORIZONTAL_LIST)
    @Provides
    @ActivityScoped
    LinearLayoutManager getHorizontalLayoutManger(Activity activity)
    {
        return new LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL, false);
    }

    @Named(VERTICAL_LIST)
    @Provides
    @ActivityScoped
    LinearLayoutManager getVerticalLayoutManger(Activity activity)
    {
        return new LinearLayoutManager(activity);
    }

    @Provides
    @ActivityScoped
    ArrayList<String> reportReasonList(){
        return new ArrayList<>();
    }


    @Provides
    @ActivityScoped
    ReportUserDialog provideReportUserDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility){
        return new ReportUserDialog(activity,typeFaceManager,utility);
    }

    @Provides
    @ActivityScoped
    ChatOptionDialog provideChatOptionDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility){
        return new ChatOptionDialog(activity,typeFaceManager,utility);
    }

}
