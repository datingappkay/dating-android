package com.helloPerfect.com.home.Discover;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.helloPerfect.com.home.Discover.GridFrg.GridDataViewFrg;
import com.helloPerfect.com.home.Discover.ListFrg.ListDataViewFrg;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import androidx.core.app.ActivityOptionsCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.helloPerfect.com.MySearchPreference.MySearchPref;
import com.helloPerfect.com.R;

import com.helloPerfect.com.dagger.ActivityScoped;
import com.helloPerfect.com.data.model.DiscoverTabPositionHolder;
import com.helloPerfect.com.data.source.PreferenceTaskDataSource;
import com.helloPerfect.com.home.Discover.GridFrg.Model.CardDeckViewAdapter;
import com.helloPerfect.com.home.Discover.Model.DiscoveryPagerAdapter;
import com.helloPerfect.com.home.Discover.Model.UserItemPojo;
import com.helloPerfect.com.home.HomeActivity;
import com.helloPerfect.com.home.HomeContract;
import com.helloPerfect.com.util.AppConfig;
import com.helloPerfect.com.util.App_permission;
import com.helloPerfect.com.util.LocationProvider.Location_service;
import com.helloPerfect.com.util.TypeFaceManager;
import com.helloPerfect.com.util.WaveDrawable;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.ads.AdRequest;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
/**
 * <h2>DiscoveryFrag</h2>
 * <p>A simple {@link Fragment} subclass.</p>
 * @author 3Embed.
 * @version 1.0.
 * @since 28-02-2018.
 */
@ActivityScoped
public class DiscoveryFrag extends DaggerFragment implements DiscoveryFragContract.View
{
    private static final String GRID_FRAG_TAG = "grid_frag_tag";
    private static final String LIST_FRAG_TAG = "list_frag_tag";
    @Inject
    Location_service location_service;
    @Inject
    App_permission app_permission;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    Activity activity;

    private ArrayList<Fragment> fragmentArrayList = new ArrayList<>();
    private FragmentManager fragmentManager;
    private DiscoveryPagerAdapter adapter;

    @Inject
    HomeContract.Presenter homePresenter;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    DiscoveryFragContract.Presenter presenter;
    @Inject
    DiscoverTabPositionHolder tabPositionHolder;
    @Inject
    @Named(DiscoveryFragUtil.USER_LIST)
    ArrayList<UserItemPojo> userList;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.discoveryParent_layout)
    RelativeLayout parent_layout;
    private Unbinder unbinder;
    @BindView(R.id.body_view)
    RelativeLayout body_view;
    @BindView(R.id.user_profile_pic)
    SimpleDraweeView user_profile_pic;
    @BindView(R.id.network_error)
    TextView network_error;
    @BindView(R.id.btn_retry)
    Button btnRetry;
    @BindView(R.id.ll_network_error)
    LinearLayout llNetworkError;

    @BindView(R.id.btn_search_preference)
    Button btnSearchPref;
    @BindView(R.id.tv_user_search_message)
    TextView tvUserSearchMsg;
    @BindView(R.id.ll_search_layout)
    LinearLayout llSearchLayout;

    private  WaveDrawable waveDrawable;
    private int currentPage;
    public static int viewWidth = 500;
    public static int viewHeight = 600;
    private AdRequest adRequest;


    public DiscoveryFrag() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        presenter.initBoostViewCountObserver();
        presenter.initBoostEndObserver();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_discovery_frg, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        presenter.takeView(this);
        unbinder=ButterKnife.bind(this,view);
        initUI();
        applyFont();
        presenter.onUsersReceived();
        //first time need to update location
        presenter.updateCurrentLocation(true);
        viewWidth = viewPager.getWidth();
        viewHeight = viewPager.getHeight();
    }

    private void applyFont() {

    }

    /*
     * Initialization of the xml content.*/
    private void initUI()
    {
        tvUserSearchMsg.setTypeface(typeFaceManager.getCircularAirLight());
        btnSearchPref.setTypeface(typeFaceManager.getCircularAirBold());
        btnRetry.setTypeface(typeFaceManager.getCircularAirBold());
        network_error.setTypeface(typeFaceManager.getCircularAirBook());
        user_profile_pic.setImageURI(dataSource.getProfilePicture());

        fragmentManager = getChildFragmentManager();
        getFragmentList();
        adapter = new DiscoveryPagerAdapter(getContext(),fragmentManager,fragmentArrayList);

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);
        tabLayout.setupWithViewPager(viewPager);

        adapter.notifyDataSetChanged();
        for (int i = 0;i < tabLayout.getTabCount(); i++)
        {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            assert tab != null;
            tab.setCustomView(adapter.getTabView(i));
        }
        LinearInterpolator interpolator = new LinearInterpolator();
        waveDrawable = new WaveDrawable(ContextCompat.getColor(activity, R.color.datum), 450);
        waveDrawable.setWaveInterpolator(interpolator);
        body_view.setBackgroundDrawable(waveDrawable);
        waveDrawable.startAnimation();
        viewPager.setCurrentItem(0,true);
        tabPositionHolder.setCurrentPosition(tabLayout.getSelectedTabPosition());
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                tabPositionHolder.setCurrentPosition(tab.getPosition());
                currentPage =tab.getPosition();
                if(userList.isEmpty()) {
                    ((HomeActivity)activity).videoSurfaceView.onActivityStop();
                }
                else{
                    if(currentPage == 0){
                        //TODO is started need to maintain properly thats it.
                        //This ll work.
                        if(!CardDeckViewAdapter.playerStartedOnce){
                            presenter.startVideoPlayFirstItem();
                        }
                        else {
                            ((HomeActivity)activity).videoSurfaceView.playPlayer();
                        }
                    }
                    else{
                        ((HomeActivity)activity).videoSurfaceView.pausePlayer();
                    }
                }
                homePresenter.checkOnlyForProfileBoost();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab){}

            @Override
            public void onTabReselected(TabLayout.Tab tab){}

        });
    }

    private void getFragmentList() {
        GridDataViewFrg gridDataViewFrg = (GridDataViewFrg) fragmentManager.findFragmentByTag(GRID_FRAG_TAG);
        if(gridDataViewFrg == null)
            gridDataViewFrg = new GridDataViewFrg();

        ListDataViewFrg listDataViewFrg = (ListDataViewFrg) fragmentManager.findFragmentByTag(LIST_FRAG_TAG);
        if(listDataViewFrg == null)
            listDataViewFrg = new ListDataViewFrg();

        fragmentArrayList.clear();
        fragmentArrayList.add(gridDataViewFrg);
        fragmentArrayList.add(listDataViewFrg);
    }


    @Override
    public void onResume()
    {
        super.onResume();

    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        presenter.dropView();
        unbinder.unbind();
    }

    @OnClick(R.id.discoveryParent_layout)
    void onParentClicked(){}

    @OnClick(R.id.preference_button)
    void openPreference()
    {
        Intent intent = new Intent(activity,MySearchPref.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivityForResult(intent, AppConfig.SEARCH_REQUEST);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        if(!app_permission.onRequestPermissionsResult(requestCode, permissions, grantResults))
        {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(AppConfig.SEARCH_REQUEST==requestCode&&resultCode==Activity.RESULT_OK)
        {
            presenter.updateCurrentLocation(true);
        }else if(requestCode== Location_service.REQUEST_CHECK_SETTINGS)
        {
            if(resultCode== Activity.RESULT_OK)
            {
                location_service.checkLocationSettings();
            }else
            {
                presenter.getUserLocationFromApi();
            }
        }else
        {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void hideUserSearchMessage() {
        llSearchLayout.setVisibility(View.GONE);
        waveDrawable.startAnimation();
    }

    @Override
    public void showUserSearchMessage(String msg)
    {
        tvUserSearchMsg.setText(msg);
        tvUserSearchMsg.setVisibility(View.VISIBLE);
        btnSearchPref.setVisibility(View.GONE);
        llSearchLayout.setVisibility(View.VISIBLE);
        llNetworkError.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);
        waveDrawable.startAnimation();
    }

    @Override
    public void showError(int id)
    {
        String message=getString(id);
        Snackbar snackbar = Snackbar
                .make(parent_layout,""+message, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showError(String error)
    {
        Snackbar snackbar = Snackbar
                .make(parent_layout,""+error, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(String message)
    {
        Snackbar snackbar = Snackbar
                .make(parent_layout,""+message, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void user_found()
    {
        viewPager.setVisibility(View.VISIBLE);
        waveDrawable.stopAnimation();
        btnSearchPref.setVisibility(View.GONE);
        llSearchLayout.setVisibility(View.GONE);
        llNetworkError.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                homePresenter.checkOnlyForProfileBoost();
            }
        },1000);

    }

    @Override
    public void noUserFound() {
        viewPager.setVisibility(View.GONE);
        tvUserSearchMsg.setText(getString(R.string.failed_to_get_user_msg));
        tvUserSearchMsg.setVisibility(View.VISIBLE);
        btnSearchPref.setVisibility(View.VISIBLE);
        llSearchLayout.setVisibility(View.VISIBLE);
        llNetworkError.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_search_preference)
    public void searchPrefSetting(){
        openPreference();
    }

    @Override
    public void showNetworkError()
    {
        llNetworkError.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.GONE);
        waveDrawable.stopAnimation();
        llSearchLayout.setVisibility(View.GONE);
    }

    @Override
    public void onLike(String user_id)
    {
        homePresenter.doLiked(false,user_id);
    }

    @Override
    public void onDislike(String user_id)
    {
        homePresenter.doDislike(user_id);
    }

    @Override
    public void onSuperLike(String user_id)
    {
        homePresenter.doSuperLike(user_id);
    }



    @Override
    public int getPagePosition()
    {
        return currentPage;
    }


    @Override
    public void openBootsDailoag()
    {
        homePresenter.openBoostDialog();
    }

    @Override
    public void launchChatScreen(Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(activity).toBundle());
    }

    @Override
    public void updateListener(UserActionEventError callback)
    {
        homePresenter.setRevertCallback(callback);
    }

    @Override
    public void setNeedToUpdateChat(boolean yes) {
        homePresenter.setNeedToUpdateChat(yes);
    }



    @OnClick(R.id.btn_retry)
    public void onRetry(){
        presenter.updateCurrentLocation(true);

    }

    public void showBoostViewCounter(boolean show) {
        presenter.showBoostViewCounter(show);
    }

    public void startCoinAnimation() {
        presenter.startCoinAnimation();
    }



    @Override
    public void checkOnlyForBoost() {
        homePresenter.checkOnlyForProfileBoost();
    }

    @Override
    public void showLoadedProfileAds() {
        homePresenter.showLoadedProfileAds();
    }
}
