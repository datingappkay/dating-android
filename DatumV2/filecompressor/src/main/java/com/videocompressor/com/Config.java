package com.videocompressor.com;
/**
 * Static class to define general
 * configuration values of the application
 *@author Suresh */
class Config
{
    static final String COMPRESSED_DIR_NAME="Datum";
    /*
     * Application folder for video files*/
    static final String COMPRESSED_VIDEOS_DIR ="/CompressedVideos/";
    /*
     * Application folder for video files*/
    static final String COMPRESSED_IMAGE_DIR="/CompressedImage/";
    /*
    * Application folder for merged files*/
    static final String MERGED_DIR="/MergedVideos/";
    /*
     * Application folder for video files*/
    static final String COMPRESSOR_TEMP_DIR = "/Temp/";
}
