package com.helloPerfect.com.editProfile.Model;

public interface ViewHolderClickCallback {
    void onClick(android.view.View view, int position);
}
