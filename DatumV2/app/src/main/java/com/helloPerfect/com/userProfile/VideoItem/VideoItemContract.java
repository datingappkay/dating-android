package com.helloPerfect.com.userProfile.VideoItem;

import com.helloPerfect.com.BasePresenter;
import com.helloPerfect.com.BaseView;
/**
 * @since  4/4/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface VideoItemContract
{
    interface View extends BaseView
    {

    }

    interface Presenter extends BasePresenter<View>
    {

    }
}
