package com.helloPerfect.com.boostLikes.Model;

import com.helloPerfect.com.BaseModel;
import com.helloPerfect.com.data.model.CoinBalanceHolder;
import com.helloPerfect.com.data.source.PreferenceTaskDataSource;
import com.helloPerfect.com.home.Discover.Model.superLike.SuperLikeResponse;
import com.helloPerfect.com.home.HomeModel.LikeResponse;
import com.helloPerfect.com.util.ApiConfig;
import com.helloPerfect.com.util.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by ankit on 7/9/18.
 */

public class BoostLikeModel extends BaseModel{


    @Inject
    ArrayList<BoostLikeData> arrayList;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    Utility utility;
    @Inject
    BoostLikeAdapter adapter;
    @Inject
    CoinBalanceHolder coinBalanceHolder;



    @Inject
    public BoostLikeModel() {
    }

    public void parseLikeList(String response) {
        try{
            BoostLikeResponse boostLikeResponse = utility.getGson().fromJson(response,BoostLikeResponse.class);
            ArrayList<BoostLikeData> boostLikeData = boostLikeResponse.getDataList();
            if(boostLikeData != null && !boostLikeData.isEmpty()){
                arrayList.clear();
                arrayList.addAll(boostLikeData);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void notifyAdapter(){
        adapter.notifyDataSetChanged();
    }

    public boolean isListEmpty(){
        return  arrayList.isEmpty();
    }

    /*
     *Creating the form data for like service. */
    public Map<String, Object> setUserDetails(String user_id)
    {
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.DoLikeService.USER_ID, user_id);
        return map;
    }

    public void addDataInPosition(BoostLikeData item) {
        try
        {
            int actual_pos=item.item_actual_pos;
            if(actual_pos<1&&actual_pos>arrayList.size()-1)
            {
                arrayList.add(0,item);
            }else
            {
                arrayList.add(actual_pos,item);
            }
        }catch (Exception e){}
    }

    public void parseLikeResponse(String response) {
        try{
            LikeResponse likeResponse = utility.getGson().fromJson(response,LikeResponse.class);
            dataSource.setRemainsLinksCount(likeResponse.getRemainsLikesInString());
            dataSource.setNextLikeTime(likeResponse.getNextLikeTime());
        }catch (Exception e){

        }
    }

    public void parseSuperLike(String response)
    {
        try
        {
            SuperLikeResponse superLikeResponse = utility.getGson().fromJson(response, SuperLikeResponse.class);
            if(superLikeResponse.getCoinWallet() != null) {;
                coinBalanceHolder.setCoinBalance(String.valueOf(superLikeResponse.getCoinWallet().getCoin()));
                coinBalanceHolder.setUpdated(true);
            }
        }catch (Exception e){}

    }

    public boolean isDialogDontNeedToShow() {
        return dataSource.getShowSuperlikeCoinDialog();
    }

    public void parseCoinConfig(String response)
    {

    }

    public BoostLikeData getBoostLikeItem(int currentPosition) {
        try {
            return arrayList.get(currentPosition);
        }catch (Exception e){}
        return null;
    }
}

