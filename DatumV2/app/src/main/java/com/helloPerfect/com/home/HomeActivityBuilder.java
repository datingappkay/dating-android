package com.helloPerfect.com.home;

import android.app.Activity;

import com.helloPerfect.com.dagger.ActivityScoped;
import com.helloPerfect.com.dagger.FragmentScoped;
import com.helloPerfect.com.home.AppSetting.AppSettingContract;
import com.helloPerfect.com.home.AppSetting.AppSettingFrag;
import com.helloPerfect.com.home.AppSetting.AppSettingPresenter;
import com.helloPerfect.com.home.Chats.ChatsFragment;
import com.helloPerfect.com.home.Chats.ChatsFrgContract;
import com.helloPerfect.com.home.Chats.ChatsFrgPresenter;
import com.helloPerfect.com.home.Discover.DiscoveryFrag;
import com.helloPerfect.com.home.Discover.DiscoveryFragContract;
import com.helloPerfect.com.home.Discover.DiscoveryFragPresenter;
import com.helloPerfect.com.home.Discover.GridFrg.GridDataViewContract;
import com.helloPerfect.com.home.Discover.GridFrg.GridDataViewFrg;
import com.helloPerfect.com.home.Discover.GridFrg.GridDataViewFrgBuilder;
import com.helloPerfect.com.home.Discover.GridFrg.GridDataViewPresenter;
import com.helloPerfect.com.home.Discover.ListFrg.ListDataViewFrg;
import com.helloPerfect.com.home.Discover.ListFrg.ListDataViewFrgBuilder;
import com.helloPerfect.com.home.Discover.ListFrg.ListDataViewFrgContract;
import com.helloPerfect.com.home.Discover.ListFrg.ListDataViewFrgPresenter;
import com.helloPerfect.com.home.Prospects.LikesMe.LikeMeUtil;
import com.helloPerfect.com.home.Prospects.LikesMe.LikesMeBuilder;
import com.helloPerfect.com.home.Prospects.LikesMe.LikesMeFrg;
import com.helloPerfect.com.home.Prospects.MyLikes.MyLikesBuilder;
import com.helloPerfect.com.home.Prospects.MyLikes.MyLikesFrg;
import com.helloPerfect.com.home.Prospects.MyLikes.MyLikesUtil;
import com.helloPerfect.com.home.Prospects.MySuperlikes.MySuperlikesBuilder;
import com.helloPerfect.com.home.Prospects.MySuperlikes.MySuperlikesFrg;
import com.helloPerfect.com.home.Prospects.MySuperlikes.MySuperlikesUtil;
import com.helloPerfect.com.home.Prospects.Online.OnlineBuilder;
import com.helloPerfect.com.home.Prospects.Online.Online_frg;
import com.helloPerfect.com.home.Prospects.Online.Online_util;
import com.helloPerfect.com.home.Prospects.Passed.PassedBuilder;
import com.helloPerfect.com.home.Prospects.Passed.PassedFrg;
import com.helloPerfect.com.home.Prospects.Passed.PassedUtil;
import com.helloPerfect.com.home.Prospects.ProspectsContract;
import com.helloPerfect.com.home.Prospects.ProspectsFrg;
import com.helloPerfect.com.home.Prospects.ProspectsFrgPresenter;
import com.helloPerfect.com.home.Prospects.RecentVisitors.RecentVisitorsBuilder;
import com.helloPerfect.com.home.Prospects.RecentVisitors.RecentVisitorsFrg;
import com.helloPerfect.com.home.Prospects.RecentVisitors.RecentVisitorsUtil;
import com.helloPerfect.com.home.Prospects.SuperLikeMe.SuperLikeMeBuilder;
import com.helloPerfect.com.home.Prospects.SuperLikeMe.SuperLikeMeFrg;
import com.helloPerfect.com.home.Prospects.SuperLikeMe.SuperLikeMeUtil;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
/**
 * <h2>HomeActivityBuilder</h2>
 * <P>
 *
 * </P>
 * @since  2/21/2018.
 */
@Module
public abstract class HomeActivityBuilder
{
 @ActivityScoped
 @Binds
 abstract Activity provideActivity(HomeActivity homeActivity);

 @ActivityScoped
 @Binds
 abstract HomeContract.View provideView(HomeActivity homeActivity);

 @ActivityScoped
 @Binds
 abstract HomeContract.Presenter homePresenter(HomePresenter presenter);

 //tab1
 @FragmentScoped
 @ContributesAndroidInjector()
 abstract DiscoveryFrag getDiscoveryFrg();

 @ActivityScoped
 @Binds
 abstract DiscoveryFragContract.Presenter taskDiscoveryPrsenter(DiscoveryFragPresenter presenter);

 @FragmentScoped
 @ContributesAndroidInjector(modules={GridDataViewFrgBuilder.class})
 abstract GridDataViewFrg getGridDataViewFrg();

 @ActivityScoped
 @Binds
 abstract GridDataViewContract.Presenter taskGridPrsenter(GridDataViewPresenter presenter);

 @FragmentScoped
 @ContributesAndroidInjector(modules={ListDataViewFrgBuilder.class})
 abstract ListDataViewFrg getListDataViewFrg();

 @ActivityScoped
 @Binds
 abstract ListDataViewFrgContract.Presenter taskListPrsenter(ListDataViewFrgPresenter presenter);



 //tab2
 @FragmentScoped
 @ContributesAndroidInjector
 abstract ProspectsFrg getProspectsFrg();

 @ActivityScoped
 @Binds
 abstract ProspectsContract.Presenter taskPresenter(ProspectsFrgPresenter presenter);

 @FragmentScoped
 @ContributesAndroidInjector(modules = {OnlineBuilder.class,Online_util.class})
 abstract Online_frg getProspectItem_frg();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {LikesMeBuilder.class,LikeMeUtil.class})
 abstract LikesMeFrg getLikesMe_frg();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {MyLikesBuilder.class,MyLikesUtil.class})
 abstract MyLikesFrg getMyLikes_frg();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {MySuperlikesBuilder.class,MySuperlikesUtil.class})
 abstract MySuperlikesFrg getMySuperlikes_frg();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {PassedBuilder.class,PassedUtil.class})
 abstract PassedFrg getPassed_frg();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {RecentVisitorsBuilder.class,RecentVisitorsUtil.class})
 abstract RecentVisitorsFrg getRecentVisitors_frg();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {SuperLikeMeBuilder.class,SuperLikeMeUtil.class})
 abstract SuperLikeMeFrg getSuperLikeMeFrg();


//tab4
 @FragmentScoped
 @ContributesAndroidInjector()
 abstract ChatsFragment getChatsFrg();

 @FragmentScoped
 @Binds
 abstract ChatsFrgContract.Presenter bindChatFragPresenter(ChatsFrgPresenter presenter);

 //tab5
 @FragmentScoped
 @ContributesAndroidInjector()
 abstract AppSettingFrag appSettingFrg();

 @FragmentScoped
 @Binds
 abstract AppSettingContract.Presenter providePresenter(AppSettingPresenter presenter);
}
