package com.helloPerfect.com.splash.Model;

import android.text.TextUtils;

import com.helloPerfect.com.BaseModel;
import com.helloPerfect.com.data.model.CoinBalanceHolder;
import com.helloPerfect.com.util.Utility;

import javax.inject.Inject;

public class SplashModel extends BaseModel {

    @Inject
    CoinBalanceHolder coinBalanceHolder;
    @Inject
    Utility utility;


    @Inject
    public SplashModel() {
    }


    public boolean isCoinBalanceEmpty() {
        return TextUtils.isEmpty(coinBalanceHolder.getCoinBalance());
    }

    public void parseCoinBalance(String response)
    {
    }

    public void parseCoinConfig(String response) {

    }
}
