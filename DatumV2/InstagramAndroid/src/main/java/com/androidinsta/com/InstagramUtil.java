package com.androidinsta.com;
/**
 * <h2>InstagramUtil</h2>
 * <P>
 *     Instagram util for the app.
 * </P>
 * @since  8/16/2017.
 * @version 1.0.
 * @author Suresh.
 */
class InstagramUtil
{
    private static final int REQUEST_CODE=1711;
    private RequestToken requestToken = null;
    private Instagram instagram;

    private InstagramUtil()
    {
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setOAuthConsumerKey(InstagramConfig.INSTAGRAM_CONSUMER_KEY);
        configurationBuilder.setOAuthConsumerSecret(InstagramConfig.INSTAGRAM_CONSUMER_SECRET);
        Configuration configuration = configurationBuilder.build();
        instagram = new InstagramFactory(configuration);
    }

    Instagram getInstagram()
    {
        return instagram;
    }

    int getREQUEST_CODE()
    {
        return REQUEST_CODE;
    }

    RequestToken getRequestToken()
    {
        if (requestToken == null)
        {
            requestToken =instagram.getOAuthRequestToken(InstagramConfig.INSTAGRAM_CALLBACK_URL);
        }
        return requestToken;
    }

    static InstagramUtil instance = new InstagramUtil();
    public static InstagramUtil getInstance()
    {
        return instance;
    }
    public void logout()
    {
        instagram.setOAuthAccessToken(null);
        reset();
    }
    public void reset()
    {
        instance = new InstagramUtil();
    }
}
