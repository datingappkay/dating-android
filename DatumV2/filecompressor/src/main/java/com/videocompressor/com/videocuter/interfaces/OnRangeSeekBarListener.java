package com.videocompressor.com.videocuter.interfaces;
import com.videocompressor.com.videocuter.view.RangeSeekBarView;
/**
 * @since 1-01-2018
 * @author Suresh.
 * @version 1.0.*/
public interface OnRangeSeekBarListener
{
    void onCreate(RangeSeekBarView rangeSeekBarView, int index, float value);

    void onSeek(RangeSeekBarView rangeSeekBarView, int index, float value);

    void onSeekStart(RangeSeekBarView rangeSeekBarView, int index, float value);

    void onSeekStop(RangeSeekBarView rangeSeekBarView, int index, float value);
}
