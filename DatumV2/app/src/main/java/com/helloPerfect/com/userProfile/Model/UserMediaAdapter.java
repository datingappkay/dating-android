package com.helloPerfect.com.userProfile.Model;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import com.helloPerfect.com.userProfile.ImageItem.ImageItemFrg;
import com.helloPerfect.com.userProfile.VideoItem.VideoItemFrg;
import java.util.ArrayList;
/**
 *<h2>UserMediaAdapter</h2>
 * <P>
 *      Pager adapter for the user list details.
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 15-03-2018.
 * */
public class UserMediaAdapter extends FragmentStatePagerAdapter
{
    private VideoItemFrg videoItemFrg=null;

    private ArrayList<MediaPojo> item_list;

    public UserMediaAdapter(FragmentManager fm,ArrayList<MediaPojo> item_list)
    {
        super(fm);
        this.item_list=item_list;
    }

    @Override
    public int getCount()
    {
        return item_list.size();
    }

    @Override
    public Fragment getItem(int position)
    {
        MediaPojo temp=item_list.get(position);
        if(temp.isVideo())
        {
            videoItemFrg=new VideoItemFrg();
            String uri = temp.getVideo_url();
            String thumb = temp.getVideo_thumbnail();
            if(uri != null)
                thumb = uri.replace(".mp4",".jpg").replace(".mov",".jpg");
            videoItemFrg.setMediaFile(thumb, temp.getVideo_url());
            return videoItemFrg;
        }else
        {
            ImageItemFrg frg=new ImageItemFrg();
            frg.setMediaFile(temp.getImage_url());
            return frg;
        }
    }
}
