package com.helloPerfect.com.selectLanguage.model;

public interface LanguageAdapterCallback {
    void onLanguageClick(int position);
}
