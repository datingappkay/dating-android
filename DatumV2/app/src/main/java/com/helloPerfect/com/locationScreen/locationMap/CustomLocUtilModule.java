package com.helloPerfect.com.locationScreen.locationMap;

import android.app.Activity;

import com.helloPerfect.com.dagger.ActivityScoped;
import com.helloPerfect.com.util.LocationUtil;

import dagger.Module;
import dagger.Provides;

/**
 * <h>CustomLocUtilModule class</h>
 * @author 3Embed.
 * @since 11/5/18.
 * @version 1.0.
 */

@Module
public class CustomLocUtilModule {

    @ActivityScoped
    @Provides
    LocationUtil provideLocationUtil(Activity activity){
        return new LocationUtil(activity);
    }

}
