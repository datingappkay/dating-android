package com.helloPerfect.com.dagger;

import com.helloPerfect.com.ChatMessageScreen.ChatMessageActivity;
import com.helloPerfect.com.ChatMessageScreen.ChatMessageModule;
import com.helloPerfect.com.ChatMessageScreen.ChatMessageUtil;
import com.helloPerfect.com.ImageCropper.CropImageActivity;
import com.helloPerfect.com.MqttChat.ForwardMessage.ActivityForwardMessage;
import com.helloPerfect.com.MqttChat.Giphy.SelectGIF;
import com.helloPerfect.com.MqttChat.Wallpapers.Activities.DrawActivity;
import com.helloPerfect.com.MqttChat.Wallpapers.Activities.SolidColorActivity;
import com.helloPerfect.com.MyProfile.MyProfileBuilder;
import com.helloPerfect.com.MyProfile.MyProfilePage;
import com.helloPerfect.com.MyProfile.ProfileUtil;
import com.helloPerfect.com.MyProfile.editAge.EditDobActivity;
import com.helloPerfect.com.MyProfile.editAge.EditDobModule;
import com.helloPerfect.com.MyProfile.editEmail.EditEmailActivity;
import com.helloPerfect.com.MyProfile.editEmail.EditEmailModule;
import com.helloPerfect.com.MyProfile.editEmail.EditEmailUtilModule;
import com.helloPerfect.com.MyProfile.editGender.EditGenderActivity;
import com.helloPerfect.com.MyProfile.editGender.EditGenderModule;
import com.helloPerfect.com.MyProfile.editGender.EditGenderUtilModule;
import com.helloPerfect.com.MyProfile.editName.EditNameActivity;
import com.helloPerfect.com.MyProfile.editName.EditNameModule;
import com.helloPerfect.com.MyProfile.editPreference.EditPrefActivity;
import com.helloPerfect.com.MyProfile.editPreference.EditPrefModule;
import com.helloPerfect.com.MyProfile.editPreference.EditPrefUtilModule;
import com.helloPerfect.com.MySearchPreference.MySearchPref;
import com.helloPerfect.com.MySearchPreference.MySearchPrefBuilder;
import com.helloPerfect.com.MySearchPreference.MySearchPrefUtil;
import com.helloPerfect.com.UserPreference.MyPreferencePage;
import com.helloPerfect.com.UserPreference.MyPreferencePageBuilder;
import com.helloPerfect.com.UserPreference.UserPrefUtil;
import com.helloPerfect.com.boostLikes.BoostLikeActivity;
import com.helloPerfect.com.boostLikes.BoostLikeModule;
import com.helloPerfect.com.boostLikes.BoostLikeUtil;
import com.helloPerfect.com.campaignScreen.CampaignActivity;
import com.helloPerfect.com.campaignScreen.CampaignModule;
import com.helloPerfect.com.editProfile.EditProfileActivity;
import com.helloPerfect.com.editProfile.EditProfileModule;
import com.helloPerfect.com.editProfile.EditProfileUtilModule;
import com.helloPerfect.com.fbRegister.FbRegisterActivityBuilder;
import com.helloPerfect.com.fbRegister.FbRegisterPage;
import com.helloPerfect.com.fbRegister.FbRegisterUtil;
import com.helloPerfect.com.googleLocationSearch.GoogleLocSearchActivity;
import com.helloPerfect.com.googleLocationSearch.GoogleLocSearchModule;
import com.helloPerfect.com.googleLocationSearch.GoogleLocSearchUtilModule;
import com.helloPerfect.com.home.AppSetting.AppSettingFragUtil;
import com.helloPerfect.com.home.Chats.ChatFragUtil;
import com.helloPerfect.com.home.Discover.DiscoveryFragUtil;
import com.helloPerfect.com.home.HomeActivity;
import com.helloPerfect.com.home.HomeActivityBuilder;
import com.helloPerfect.com.home.HomeUtil;
import com.helloPerfect.com.home.Prospects.ProspectsFragUtil;
import com.helloPerfect.com.locationScreen.LocSearchModule;
import com.helloPerfect.com.locationScreen.LocSearchUtilModule;
import com.helloPerfect.com.locationScreen.LocationSearchActivity;
import com.helloPerfect.com.locationScreen.locationMap.CustomLocActivity;
import com.helloPerfect.com.locationScreen.locationMap.CustomLocModule;
import com.helloPerfect.com.locationScreen.locationMap.CustomLocUtilModule;
import com.helloPerfect.com.login.LoginActivity;
import com.helloPerfect.com.login.LoginDaggerModule;
import com.helloPerfect.com.login.LoginUtil;
import com.helloPerfect.com.messageInfo.MessageInfoActivity;
import com.helloPerfect.com.messageInfo.MessageInfoModule;
import com.helloPerfect.com.messageInfo.MessageinfoUtil;
import com.helloPerfect.com.mobileverify.MobileVerifyActivity;
import com.helloPerfect.com.mobileverify.MobileVerifyActivityBuilder;
import com.helloPerfect.com.mobileverify.MobileVerifyUtil;
import com.helloPerfect.com.passportLocation.PassportActivity;
import com.helloPerfect.com.passportLocation.PassportModule;
import com.helloPerfect.com.passportLocation.PassportUtilModule;
import com.helloPerfect.com.register.RegisterActivityBuilder;
import com.helloPerfect.com.register.RegisterPage;
import com.helloPerfect.com.register.RegisterUtil;
import com.helloPerfect.com.selectLanguage.SelectLanguageActivity;
import com.helloPerfect.com.selectLanguage.SelectLanguageModule;
import com.helloPerfect.com.selectLanguage.SelectLanguageUtil;
import com.helloPerfect.com.settings.SettingsActivity;
import com.helloPerfect.com.settings.SettingsModule;
import com.helloPerfect.com.settings.SettingsUtilModule;
import com.helloPerfect.com.splash.SplashActivity;
import com.helloPerfect.com.splash.SplashDaggerModule;
import com.helloPerfect.com.splash.SplashUtil;
import com.helloPerfect.com.userProfile.Profile_util;
import com.helloPerfect.com.userProfile.UserProfileBuilder;
import com.helloPerfect.com.userProfile.UserProfilePage;
import com.helloPerfect.com.webPage.WebActivity;
import com.helloPerfect.com.webPage.WebModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
/**
 * We want Dagger.Android to create a Subcomponent which has a parent Component of whichever module ActivityBindingModule is on,
 * in our case that will be AppComponent.
 * The beautiful part about this setup is that you never need to tell AppComponent that it is going to have all these subcomponents
 * nor do you need to tell these subcomponents that AppComponent exists.
 * We are also telling Dagger.Android that this generated SubComponent needs to include the specified modules and be aware of a scope annotation @ActivityScoped
 * When Dagger.Android annotation processor runs it will create 4 subcomponents for us.
 */

@Module
public abstract class ActivityBindingModule
{
    @ActivityScoped
    @ContributesAndroidInjector(modules = {SelectLanguageUtil.class, SelectLanguageModule.class})
    abstract SelectLanguageActivity selectLanguageActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {SplashDaggerModule.class, SplashUtil.class})
    abstract SplashActivity splashActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules ={LoginDaggerModule.class, LoginUtil.class})
    abstract LoginActivity loginActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {MobileVerifyActivityBuilder.class,MobileVerifyUtil.class})
    abstract MobileVerifyActivity mobileVerifyActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules ={RegisterActivityBuilder.class, RegisterUtil.class})
    abstract RegisterPage getRegisterActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules ={FbRegisterActivityBuilder.class, FbRegisterUtil.class})
    abstract FbRegisterPage getFbRegisterActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules ={MyPreferencePageBuilder.class,UserPrefUtil.class})
    abstract MyPreferencePage myPreferencePage();


    @ActivityScoped
    @ContributesAndroidInjector(modules ={HomeActivityBuilder.class,HomeUtil.class
            , DiscoveryFragUtil.class,ProspectsFragUtil.class,ChatFragUtil.class, AppSettingFragUtil.class})
    abstract HomeActivity homeActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules ={MySearchPrefBuilder.class,MySearchPrefUtil.class})
    abstract MySearchPref homeMySearchPref();

    @ActivityScoped
    @ContributesAndroidInjector(modules ={UserProfileBuilder.class,Profile_util.class})
    abstract UserProfilePage homeUserProfileAct();

    @ActivityScoped
    @ContributesAndroidInjector(modules ={MyProfileBuilder.class,ProfileUtil.class})
    abstract MyProfilePage MyProfilePageAct();

    @ActivityScoped
    @ContributesAndroidInjector
    abstract CropImageActivity getCropImageActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {SettingsModule.class, SettingsUtilModule.class})
    abstract SettingsActivity settingsActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {EditProfileModule.class, EditProfileUtilModule.class})
    abstract EditProfileActivity editProfileActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {EditPrefModule.class, EditPrefUtilModule.class})
    abstract EditPrefActivity editPrefActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {EditGenderModule.class,EditGenderUtilModule.class})
    abstract EditGenderActivity editGenderActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {EditDobModule.class})
    abstract EditDobActivity editDobActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {EditEmailModule.class, EditEmailUtilModule.class})
    abstract EditEmailActivity editEmailActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {EditNameModule.class})
    abstract EditNameActivity editNameActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {LocSearchModule.class, LocSearchUtilModule.class})
    abstract LocationSearchActivity location_search_activity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {CustomLocModule.class, CustomLocUtilModule.class})
    abstract CustomLocActivity customLocActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = WebModule.class)
    abstract WebActivity webActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {PassportModule.class, PassportUtilModule.class})
    abstract PassportActivity passportActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {GoogleLocSearchModule.class, GoogleLocSearchUtilModule.class})
    abstract GoogleLocSearchActivity googleLocSearchActivity();



    @ActivityScoped
    @ContributesAndroidInjector()
    abstract ActivityForwardMessage activityForwardMessage();

    @ActivityScoped
    @ContributesAndroidInjector()
    abstract SelectGIF selectGIF();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {ChatMessageModule.class, ChatMessageUtil.class})
    abstract ChatMessageActivity chatMessageScreen();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {BoostLikeModule.class, BoostLikeUtil.class})
    abstract BoostLikeActivity boostLikeActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {CampaignModule.class})
    abstract CampaignActivity campaignActivity();

    @ActivityScoped
    @ContributesAndroidInjector()
    abstract DrawActivity drawActivity();

    @ActivityScoped
    @ContributesAndroidInjector()
    abstract SolidColorActivity solidColorActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {MessageInfoModule.class, MessageinfoUtil.class})
    abstract MessageInfoActivity messageInfoActivity();
}
