package com.helloPerfect.com.fbRegister;

import android.app.Activity;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;

import com.helloPerfect.com.dagger.ActivityScoped;
import com.helloPerfect.com.dagger.FragmentScoped;
import com.helloPerfect.com.fbRegister.Email.FbEmailFragment;
import com.helloPerfect.com.fbRegister.Email.FbEmailFrgBuilder;
import com.helloPerfect.com.fbRegister.Gender.FbGenderFragment;
import com.helloPerfect.com.fbRegister.Gender.FbGenderFrgBuilder;
import com.helloPerfect.com.fbRegister.Name.FbNameFragBuilder;
import com.helloPerfect.com.fbRegister.Name.FbNameFragment;
import com.helloPerfect.com.fbRegister.ProfilePic.FbProfilePicBuilder;
import com.helloPerfect.com.fbRegister.ProfilePic.FbProfilePicFrg;
import com.helloPerfect.com.fbRegister.ProfileVideo.FbProfileVideoBuilder;
import com.helloPerfect.com.fbRegister.ProfileVideo.FbProfileVideoFrg;
import com.helloPerfect.com.fbRegister.Userdob.FbDobFragment;
import com.helloPerfect.com.fbRegister.Userdob.FbDobFrgBuilder;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

/**
 * <h1>FbRegisterActivityBuilder</h1>
 * <p>
 *
 * </p>
 * @author 3Embed
 * @since 13/08/17
 * @version 1.0.
 */
@Module
public abstract class FbRegisterActivityBuilder
{
 static final String ACTIVITY_FRAGMENT_MANAGER = "Register.FragmentManager";

 @ActivityScoped
 @Binds
 abstract Activity provideActivity(FbRegisterPage register_page);

 @ActivityScoped
 @Binds
 abstract FbRegisterContact.View provideView(FbRegisterPage register_page);

 @ActivityScoped
 @Binds
 abstract FbRegisterContact.Presenter taskPresenter(FbRegisterPagePresenter presenter);

 @Provides
 @Named(ACTIVITY_FRAGMENT_MANAGER)
 @ActivityScoped
 static FragmentManager activityFragmentManager(Activity activity)
 {
  return ((AppCompatActivity)activity).getSupportFragmentManager();
 }

 @FragmentScoped
 @ContributesAndroidInjector(modules = {FbEmailFrgBuilder.class})
 abstract FbEmailFragment getFbEmailFragment();

 @FragmentScoped
 @ContributesAndroidInjector(modules ={FbNameFragBuilder.class})
 abstract FbNameFragment getFbNameFragment();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {FbDobFrgBuilder.class})
 abstract FbDobFragment getFbDobFragment();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {FbGenderFrgBuilder.class})
 abstract FbGenderFragment getFbGenderFragment();


 @FragmentScoped
 @ContributesAndroidInjector(modules = {FbProfilePicBuilder.class})
 abstract FbProfilePicFrg getFbProfilePicFragment();


 @FragmentScoped
 @ContributesAndroidInjector(modules = {FbProfileVideoBuilder.class})
 abstract FbProfileVideoFrg getFbProfileVideoFragment();


}
