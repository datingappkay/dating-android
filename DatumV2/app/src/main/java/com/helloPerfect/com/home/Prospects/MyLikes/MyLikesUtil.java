package com.helloPerfect.com.home.Prospects.MyLikes;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.helloPerfect.com.dagger.FragmentScoped;
import com.helloPerfect.com.home.Prospects.MyLikes.Model.MyLikesItemPojo;
import com.helloPerfect.com.home.Prospects.MyLikes.Model.MyLikesUserAdapter;
import com.helloPerfect.com.util.TypeFaceManager;
import com.helloPerfect.com.util.Utility;

import java.util.ArrayList;
import dagger.Module;
import dagger.Provides;
/**
 * @since   3/23/2018.
 * @author 3Embed.
 * @version 1.0
 */
@Module
public class MyLikesUtil
{
    @Provides
    @FragmentScoped
    ArrayList<MyLikesItemPojo> getList()
    {
        return new ArrayList<>();
    }
    @Provides
    @FragmentScoped
    MyLikesUserAdapter getUserListAdapter(ArrayList<MyLikesItemPojo> list, TypeFaceManager typeFaceManager, Utility utility)
    {
        return new MyLikesUserAdapter(list,typeFaceManager,utility);
    }

    @Provides
    @FragmentScoped
    LinearLayoutManager getLinearLayoutManager(Activity activity)
    {
        return new LinearLayoutManager(activity);
    }
}
