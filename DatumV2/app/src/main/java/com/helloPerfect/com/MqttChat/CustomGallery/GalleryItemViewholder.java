package com.helloPerfect.com.MqttChat.CustomGallery;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.helloPerfect.com.R;


/**
 * Handler for the Gallery image view .
 *
 * @since 09/05/17.
 */
public class GalleryItemViewholder extends RecyclerView.ViewHolder {
    GrideSquareImageView thumb_nail;

    GalleryItemViewholder(View itemView) {
        super(itemView);
        thumb_nail = (GrideSquareImageView) itemView.findViewById(R.id.thumb_nail_image);
    }
}