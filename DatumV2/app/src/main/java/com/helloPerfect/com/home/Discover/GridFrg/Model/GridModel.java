package com.helloPerfect.com.home.Discover.GridFrg.Model;

import com.helloPerfect.com.BaseModel;
import com.helloPerfect.com.data.model.CoinBalanceHolder;

import com.helloPerfect.com.data.source.PreferenceTaskDataSource;

import javax.inject.Inject;

public class GridModel extends BaseModel{

    @Inject
    CoinBalanceHolder coinBalanceHolder;
    @Inject
    PreferenceTaskDataSource dataSource;

    @Inject
    public GridModel() {
    }


    public boolean isEnoughWalletBalanceToSuperLike() {

            return true;

    }


    public boolean isSuperlikeSpendDialogNeedToShow() {
        return dataSource.getShowSuperlikeCoinDialog();
    }
}
