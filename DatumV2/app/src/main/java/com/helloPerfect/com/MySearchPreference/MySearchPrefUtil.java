package com.helloPerfect.com.MySearchPreference;

import android.app.Activity;
import com.helloPerfect.com.dagger.ActivityScoped;
import com.helloPerfect.com.data.source.PreferenceTaskDataSource;
import com.helloPerfect.com.util.TypeFaceManager;
import com.helloPerfect.com.util.Utility;
import com.helloPerfect.com.util.boostDialog.BoostDialog;
import com.helloPerfect.com.util.progressbar.LoadingProgress;
import dagger.Module;
import dagger.Provides;
/**
 * @since  3/8/2018.
 */
@Module
public class MySearchPrefUtil
{
    @Provides
    @ActivityScoped
    AnimatorHandler animatorHandler(Activity activity)
    {
        return new AnimatorHandler(activity);
    }

    @ActivityScoped
    @Provides
    LoadingProgress getLoadingProgress(Activity activity)
    {
        return new LoadingProgress(activity);
    }

    @Provides
    @ActivityScoped
    BoostDialog getBootsDialog(Activity activity, TypeFaceManager typeFaceManager, PreferenceTaskDataSource dataSource, Utility utility)
    {
        return new BoostDialog(activity,typeFaceManager,dataSource,utility);
    }

}
