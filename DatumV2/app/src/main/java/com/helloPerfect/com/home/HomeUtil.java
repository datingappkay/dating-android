package com.helloPerfect.com.home;

import android.app.Activity;
import android.content.Context;
import android.location.Geocoder;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.helloPerfect.com.dagger.ActivityScoped;
import com.helloPerfect.com.data.model.DiscoverTabPositionHolder;
import com.helloPerfect.com.data.source.PreferenceTaskDataSource;
import com.helloPerfect.com.home.AppSetting.AppSettingFrag;
import com.helloPerfect.com.home.Chats.ChatsFragment;
import com.helloPerfect.com.home.Discover.DiscoveryFrag;
import com.helloPerfect.com.home.HomeModel.LoadMoreStatus;
import com.helloPerfect.com.home.Prospects.ProspectsFrg;
import com.helloPerfect.com.util.App_permission;
import com.helloPerfect.com.util.LocationProvider.Location_service;
import com.helloPerfect.com.util.SuggestionAleret.SuggestionDialog;
import com.helloPerfect.com.util.TypeFaceManager;
import com.helloPerfect.com.util.Utility;
import com.helloPerfect.com.util.boostDialog.BoostDialog;
import com.helloPerfect.com.util.progressbar.LoadingProgress;
import com.helloPerfect.com.util.timerDialog.TimerDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
/**
 * <h2>HomeUtil</h2>
 * <P>
 *     Home page util for the home page data and the details .
 * </P>
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public class HomeUtil
{
    public static final String HOME_FRAGMENT_MANAGER = "HOME_FM";
    public static final String LOAD_MORE_STATUS ="load_more";
    public static final String LOAD_MORE_DATE_STATUS ="load_date_more";
    public static final String LOAD_MORE_PAST_DATE_STATUS ="load_past_date_more";

    public static final String DISCOVER_FRAG_TAG = "discoverFragTag";
    public static final String PROSPECT_FRAG_TAG = "prospectFragTag";
    public static final String DATE_FRAG_TAG = "dateFragTag";
    public static final String CHAT_FRAG_TAG = "chatFragTag";
    public static final String SETTINGS_FRAG_TAG = "settingsFragTag";

    @ActivityScoped
    @Provides
    DiscoveryFrag provideDiscoveryFrg(@Named(HOME_FRAGMENT_MANAGER) FragmentManager fragmentManager){
        DiscoveryFrag discoveryFrag = (DiscoveryFrag) fragmentManager.findFragmentByTag(DISCOVER_FRAG_TAG);
        if(discoveryFrag == null)
            discoveryFrag = new DiscoveryFrag();
        return discoveryFrag;
    }

    @ActivityScoped
    @Provides
    ProspectsFrg provideProspectsFrg(@Named(HOME_FRAGMENT_MANAGER)FragmentManager fragmentManager){
        ProspectsFrg prospectsFrg = (ProspectsFrg) fragmentManager.findFragmentByTag(PROSPECT_FRAG_TAG);
        if(prospectsFrg == null)
            prospectsFrg = new ProspectsFrg();
        return prospectsFrg;
    }


    @ActivityScoped
    @Provides
    ChatsFragment provideChatsFragment(@Named(HOME_FRAGMENT_MANAGER)FragmentManager fragmentManager){
        ChatsFragment chatsFragment = (ChatsFragment) fragmentManager.findFragmentByTag(CHAT_FRAG_TAG);
        if(chatsFragment == null)
            chatsFragment = new ChatsFragment();
        return chatsFragment;
    }

    @ActivityScoped
    @Provides
    AppSettingFrag provideAppSettingsFrag(@Named(HOME_FRAGMENT_MANAGER)FragmentManager fragmentManager){
        AppSettingFrag appSettingFrag = (AppSettingFrag) fragmentManager.findFragmentByTag(SETTINGS_FRAG_TAG);
        if(appSettingFrag == null)
            appSettingFrag = new AppSettingFrag();
        return appSettingFrag;
    }

    @Named(HOME_FRAGMENT_MANAGER)
    @Provides
    @ActivityScoped
    FragmentManager activityFragmentManager(Activity activity)
    {
        return ((AppCompatActivity)activity).getSupportFragmentManager();
    }

    @Provides
    @ActivityScoped
    SuggestionDialog getSuggestionAlert(Activity activity,TypeFaceManager typeFaceManager,Utility utility,PreferenceTaskDataSource dataSource)
    {
        return new SuggestionDialog(activity,typeFaceManager,utility,dataSource);
    }

    @ActivityScoped
    @Provides
    Location_service getLocation_service(Activity activity)
    {
        return new Location_service(activity);
    }

    @Provides
    @ActivityScoped
    App_permission getApp_permission(Activity activity, TypeFaceManager typeFaceManager)
    {
        return new App_permission(activity,typeFaceManager);
    }

    @Provides
    @ActivityScoped
    AnimationHandler getAnimationHandler(Context context)
    {
        return new AnimationHandler(context);
    }

    @Named(LOAD_MORE_STATUS)
    @Provides
    @ActivityScoped
    LoadMoreStatus dataPresentStatus()
    {
        return new LoadMoreStatus();
    }

    @Named(LOAD_MORE_DATE_STATUS)
    @Provides
    @ActivityScoped
    LoadMoreStatus dateLoadMoreStatus()
    {
        return new LoadMoreStatus();
    }

    @Provides
    @ActivityScoped
    BoostDialog getBootsDialog(Activity activity, TypeFaceManager typeFaceManager, PreferenceTaskDataSource dataSource,Utility utility)
    {
       return new BoostDialog(activity,typeFaceManager,dataSource,utility);
    }

    @Provides
    @ActivityScoped
    LoadingProgress provideLoadingProgress(Activity activity){
        return new LoadingProgress(activity);
    }


    @ActivityScoped
    @Provides
    Geocoder provideGeocoder(Activity activity)
    {
        return new Geocoder(activity, Locale.getDefault());
    }
    @ActivityScoped
    @Provides
    DiscoverTabPositionHolder ProvideDiscoverTabPostionHolder(){
        return new DiscoverTabPositionHolder();
    }


    @ActivityScoped
    @Provides
    List<UnifiedNativeAd> provideAdList(){
        return new ArrayList<>();
    }


    @ActivityScoped
    @Provides
    TimerDialog provideTimerDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility,PreferenceTaskDataSource dataSource){
        return new TimerDialog(activity,typeFaceManager,utility,dataSource);
    }

}
