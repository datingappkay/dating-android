package com.helloPerfect.com.dagger;

import android.content.Context;

import com.androidinsta.com.InstagramManger;
import com.couchbase.lite.android.AndroidContext;
import com.facebookmanager.com.FacebookManager;
import com.helloPerfect.com.MatchedView.DateAlertDialog;
import com.helloPerfect.com.MatchedView.Its_Match_alert;
import com.helloPerfect.com.MatchedView.MatchAlertObserver;
import com.helloPerfect.com.MatchedView.MatchAnimation;
import com.helloPerfect.com.MatchedView.MatchedModel;
import com.helloPerfect.com.MqttChat.Database.CouchDbController;
import com.helloPerfect.com.MqttManager.MqttRxObserver;
import com.helloPerfect.com.R;
import com.helloPerfect.com.boostDetail.model.CoinPlan;
import com.helloPerfect.com.boostDetail.model.SubsPlan;
import com.helloPerfect.com.data.local.PreferencesHelper;
import com.helloPerfect.com.data.model.CoinBalanceHolder;
import com.helloPerfect.com.data.model.ProUserModel;
import com.helloPerfect.com.data.model.ProfileDataChangeHolder;
import com.helloPerfect.com.data.model.UserActionHolder;
import com.helloPerfect.com.data.source.PassportLocationDataSource;
import com.helloPerfect.com.data.source.PreferenceTaskDataSource;
import com.helloPerfect.com.home.HomeModel.HomeAnimation;
import com.helloPerfect.com.networking.NetworkStateHolder;
import com.helloPerfect.com.networking.RxNetworkObserver;
import com.helloPerfect.com.util.AppConfig;
import com.helloPerfect.com.util.CalendarEventHelper;
import com.helloPerfect.com.util.CloudManager.UploadManager;
import com.helloPerfect.com.util.CustomObserver.AdminCoinObserver;
import com.helloPerfect.com.util.CustomObserver.BoostEndObserver;
import com.helloPerfect.com.util.CustomObserver.BoostViewCountObserver;
import com.helloPerfect.com.util.CustomObserver.CoinBalanceObserver;
import com.helloPerfect.com.util.CustomObserver.DateObserver;
import com.helloPerfect.com.util.CustomObserver.DateRefreshObserver;
import com.helloPerfect.com.util.CustomObserver.LocationObserver;
import com.helloPerfect.com.util.CustomObserver.SettingsDataChangeObserver;
import com.helloPerfect.com.util.CustomObserver.dataChangeObserver.DatumDataChangeObserver;
import com.helloPerfect.com.util.DatumActivateLifeListener;
import com.helloPerfect.com.util.DeviceUuidFactory;
import com.helloPerfect.com.util.FileUtil.AppFileManger;
import com.helloPerfect.com.util.ImageChecker.ImageProcessor;
import com.helloPerfect.com.util.LocationProvider.LocationApiManager;
import com.helloPerfect.com.util.TypeFaceManager;
import com.helloPerfect.com.util.Utility;
import com.helloPerfect.com.util.accountKit.AccountKitManager;
import com.helloPerfect.com.util.accountKit.AccountKitManagerImpl;
import com.helloPerfect.com.util.boostDialog.Offer;
import com.helloPerfect.com.util.boostDialog.Slide;
import com.helloPerfect.com.util.notificationHelper.NotificationHelper;
import com.helloPerfect.com.util.notificationHelper.NotificationHelperImpl;

import java.util.ArrayList;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppUtilModule
{
    @Singleton
    @Provides
    PreferenceTaskDataSource preferencesHelper(Context context)
    {
        return new PreferencesHelper(context);
    }

    @Singleton
    @Provides
    TypeFaceManager provideTypefaceManager(Context context){
        return new TypeFaceManager(context);
    }

    @Singleton
    @Provides
    FacebookManager provideFacebookManager(Context context)
    {
        return new FacebookManager(context, context.getString(R.string.facebook_app_id));
    }

    @Singleton
    @Provides
    InstagramManger getInstagramManger(Context context)
    {
        return  new InstagramManger(context,AppConfig.INSTA_CLINT_KEY,AppConfig.INSTA_SECRET_KEY);
    }

    @Singleton
    @Provides
    DeviceUuidFactory getDeviceUuidFactory(Context context)
    {
        return new DeviceUuidFactory(context);
    }

    @Singleton
    @Provides
    Utility getUtility(Context context)
    {
        return new Utility(context);
    }

    @Singleton
    @Provides
    UploadManager uploadManager()
    {
        return new UploadManager();
    }


    @Singleton
    @Provides
    AppFileManger appFileManger(Context context)
    {
        return new AppFileManger(context);
    }



    @Singleton
    @Provides
    LocationApiManager getLocationApiManager()
    {
        return new LocationApiManager(AppConfig.GOOGLE_LOCATION_KEY);
    }

    @Singleton
    @Provides
    NetworkStateHolder getNetworkStateHolder()
    {
        return new NetworkStateHolder();
    }

    @Singleton
    @Provides
    MatchAlertObserver getMatchAlertObserver()
    {
        return new MatchAlertObserver();
    }

    @Singleton
    @Provides
    RxNetworkObserver getRxNetworkObserver()
    {
        return new RxNetworkObserver();
    }


    @Singleton
    @Provides
    ImageProcessor getImageProcessor()
    {
        return new ImageProcessor(AppConfig.IMAGE_PROCESSOR_KEY,AppConfig.IMAGE_PROCESSOR_SECRET_KEY);
    }

    @Singleton
    @Provides
    MatchAnimation getAnimationHandler(Context context)
    {
        return new MatchAnimation(context);
    }

    @Singleton
    @Provides
    HomeAnimation provideHomeAnimation(Context context)
    {
        return new HomeAnimation(context);
    }

    @Singleton
    @Provides
    DateAlertDialog getDateAlertDialog(Context context,TypeFaceManager typeFaceManager)
    {
        return new DateAlertDialog(context,typeFaceManager);
    }

    @Singleton
    @Provides
    Its_Match_alert getIts_Match_alert(Context context, PreferenceTaskDataSource dataSource, TypeFaceManager typeFaceManager,MatchAnimation animationHandler,DateAlertDialog dateAlertDialog)
    {
        return new Its_Match_alert(context,dataSource,typeFaceManager,animationHandler,dateAlertDialog);
    }

    @Singleton
    @Provides
    MatchedModel getMatchedModel(Context context,PreferenceTaskDataSource dataSource,Utility utility)
    {
        return new MatchedModel(context,dataSource,utility);
    }

    @Singleton
    @Provides
    DatumActivateLifeListener getDatumActivateLifeListener()
    {
        return new DatumActivateLifeListener();
    }

    @Singleton
    @Provides
    CouchDbController getCouchDbController(Context context)
    {
        return new CouchDbController(new AndroidContext(context));
    }

    @Singleton
    @Provides
    DateObserver provideDateObserver(){
        return new DateObserver();
    }

    @Singleton
    @Provides
    LocationObserver provideLoationObserver(){
        return new LocationObserver();
    }

    @Singleton
    @Provides
    ProfileDataChangeHolder provideProfileDataChangeHolder(){
        return new ProfileDataChangeHolder();
    }

    @Singleton
    @Provides
    ArrayList<Slide> provideSlideList(Context context)
    {
        ArrayList<Slide> slideArrayList = new ArrayList<>();
        CharSequence[] boostSubTitles = context.getResources().getTextArray(R.array.boost_dialog_subtitle);
        CharSequence[] boostMsgs = context.getResources().getTextArray(R.array.boost_dialog_msg);
        int[] colors = context.getResources().getIntArray(R.array.boost_dialog_color);
        int[] drawable = {R.drawable.unlimited_likes ,R.drawable.dialog_boost ,R.drawable.who_sees_you,
                R.drawable.swipe_around_the_world, R.drawable.control_your_profile,
                R.drawable.super_likes, R.drawable.unlimited_rewinds, R.drawable.turn_off_adverts,
                0};
        for(int i = 0; i< boostSubTitles.length;i++){
            slideArrayList.add(new Slide(colors[i],drawable[i],(String)boostSubTitles[i], (String) boostMsgs[i]));
        }
        return slideArrayList;
    }

    @Singleton
    @Provides
    ArrayList<Offer> provideOfferList(){
        ArrayList<Offer> offerList = new ArrayList<>();
        offerList.add(new Offer("",12,"$ 350.00/mth",false));
        offerList.add(new Offer("MOST POPULAR",6,"$ 450.00/mth",true));
        offerList.add(new Offer("",1,"$ 150.00/mth",false));
        return offerList;
    }

    @Singleton
    @Provides
    CoinBalanceHolder provideCoinBalanceHolder(){
        return new CoinBalanceHolder();
    }

    @Singleton
    @Provides
    CoinBalanceObserver provideCoinBalanceObserver(){
        return new CoinBalanceObserver();
    }

    @Singleton
    @Provides
    ArrayList<CoinPlan> provideCoinPlanList(){
        return new ArrayList<>();
    }

    @Singleton
    @Provides
    ArrayList<SubsPlan> provideSubsPlanList(){
        return new ArrayList<>();
    }

    @Singleton
    @Provides
    PassportLocationDataSource passportLocationHelper(Context context)
    {
        return new PreferencesHelper(context);
    }


    @Singleton
    @Provides
    CalendarEventHelper provideCalendarEventHelper(Context context){
        return new CalendarEventHelper(context);
    }

    @Singleton
    @Provides
    MqttRxObserver provideMqttRxObserver(){
        return new MqttRxObserver();
    }

    @Singleton
    @Provides
    DateRefreshObserver provideDateRefreshObserver(){
        return new DateRefreshObserver();
    }


    @Singleton
    @Provides
    ProUserModel provideProUserModel(Context context, PreferenceTaskDataSource dataSource, Utility utility,BoostViewCountObserver countObserver,NotificationHelper notificationHelper)
    {
        return new ProUserModel(context,dataSource,utility,countObserver,notificationHelper);
    }

    @Singleton
    @Provides
    BoostViewCountObserver boostViewCountObserver()
    {
        return new BoostViewCountObserver();
    }

    @Singleton
    @Provides
    BoostEndObserver boostEndObserver()
    {
        return new BoostEndObserver();
    }

    @Singleton
    @Provides
    AdminCoinObserver adminCoinObserver()
    {
        return new AdminCoinObserver();
    }

    @Singleton
    @Provides
    SettingsDataChangeObserver settingsDataChangeObserver(){
        return new SettingsDataChangeObserver();
    }

    @Singleton
    @Provides
    NotificationHelper provideNotificationHelper(Context context,DatumActivateLifeListener datumActivateLifeListener){
        return new NotificationHelperImpl(context,datumActivateLifeListener);
    }

    @Singleton
    @Provides
    DatumDataChangeObserver datumDataChangeObserver(){
        return new DatumDataChangeObserver();
    }

    @Singleton
    @Provides
    UserActionHolder provideUserActionHolder(PreferenceTaskDataSource dataSource){
        return new UserActionHolder(dataSource);
    }

    @Singleton
    @Provides
    AccountKitManager provideAccountKitManager(){
        return new AccountKitManagerImpl();

    }
}
