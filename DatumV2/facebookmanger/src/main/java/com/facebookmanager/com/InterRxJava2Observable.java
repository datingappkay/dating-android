package com.facebookmanager.com;

import org.json.JSONObject;

import io.reactivex.Observable;
import io.reactivex.Observer;

/**
 * @author Suresh.
 * @version 1.0.
 * @since 12/14/2017.
 */
class InterRxJava2Observable extends Observable<JSONObject> {
    private Observer<? super JSONObject> observer;

    static InterRxJava2Observable getObservable() {
        return new InterRxJava2Observable();
    }

    @Override
    protected void subscribeActual(Observer<? super JSONObject> observer) {
        this.observer = observer;
    }

    void publishData(JSONObject data) {
        if (observer != null && data != null) {
            try {
                boolean isError = data.getBoolean("ERROR");
                if (isError) {
                    observer.onError(new Throwable(data.getString("MESSAGE")));
                } else {
                    observer.onNext(data);
                }
            } catch (Exception e) {
                observer.onError(new Throwable(e.getMessage()));
            }
            observer.onComplete();
        }
    }
}